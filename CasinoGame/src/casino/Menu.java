package casino;

public class Menu {

	public static final int principal = 0, joueur = 1, convertir = 2, objet = 3, option = 4, tuto = 5, credit = 6;
	public static final int tutoPoker = 0, tutoBJ = 1, tutoMachine = 2, tutoRoulette = 3, tutoPari = 4;
	private int inTuto;
	private int loc;
	private int select;
	
	public Menu() {
		inTuto = -1;
		loc = principal;
		select = 1;
	}
	
	public int getInTuto() {
		return inTuto;
	}
	
	public void setInTuto(int tuto) {
		inTuto = tuto;
	}

	public int getLoc() {
		return loc;
	}

	public void setLoc(int loc) {
		select = 1;
		this.loc = loc;
	}

	public int getSelect() {
		return select;
	}
	
	public void setSelectP(int select) {
		this.select = select;
	}

	public void setSelect(int select) {
		if(loc == principal) {
			if(select == 1) {
				if(this.select >= 6) {
					this.select = 1;
				}else {
					this.select++;
				}
			}
			if(select == -1) {
				if(this.select <= 1) {
					this.select = 6;
				}else {
					this.select--;
				}
			}
		}
		if(loc == joueur || loc == convertir || loc == objet) {
			if(select == 1) {
				if(this.select >= 4) {
					this.select = 1;
				}else {
					this.select++;
				}
			}
			if(select == -1) {
				if(this.select <= 1) {
					this.select = 4;
				}else {
					this.select--;
				}
			}
		}
		if(loc == option) {
			if(select == 1) {
				if(this.select >= 3) {
					this.select = 1;
				}else {
					this.select++;
				}
			}
			if(select == -1) {
				if(this.select <= 1) {
					this.select = 3;
				}else {
					this.select--;
				}
			}
		}
		if(loc == tuto) {
			if(select == 1) {
				if(this.select >= 5) {
					this.select = 1;
				}else {
					this.select++;
				}
			}
			if(select == -1) {
				if(this.select <= 1) {
					this.select = 5;
				}else {
					this.select--;
				}
			}
		}
	}
	
	
	
}
