package casino;

import jeu2carte.BlackJack;
import jeu2carte.Poker;
import jeu2chance.MachineSou;
import jeu2chance.Pari;
import jeu2chance.Roulette;
import joueur.JoueurCourant;
import utilisateur.ThreadJeu;

public class Casino {

	//diff�rent jeu
	private Roulette roulette;
	private MachineSou machineSou;
	private Pari pari;
	private Menu menu;
	
	private ThreadJeu jeu2carte;
	
	//diff�rent joueurs jouable
	private JoueurCourant Jfacile;
	private JoueurCourant Jmoyen;
	private JoueurCourant Jdifficile;
	private JoueurCourant Jlibre;
	
	//joueur s�l�ctionn�
	private JoueurCourant Jcourant;
	
	//liste des trophees
	public static int trophee1= 0, trophee2 = 1, trophee3 = 2;
	private boolean[] trophee;
	
	public Casino() {
		//initialise les joueurs jouable
		Jfacile = new JoueurCourant("patrick", 1000, 10000, 50000, "images/icon1.png", 1);
		Jmoyen = new JoueurCourant("xavier", 100, 1000, 100000, "images/icon2.png", 2);
		Jdifficile = new JoueurCourant("hubert", 0, 100, 1000000, "images/icon3.png", 3);
		Jlibre = new JoueurCourant("frederic", -1, -1, -1, "images/icon4.png", 2);
		
		Jcourant = Jlibre;
		
		jeu2carte = new ThreadJeu();
		
		trophee = new boolean[] {false,false,false};
	}
	
	/**
	 * 
	 * @return la partie de poker du casino
	 */
	public Poker getPoker() {
		return jeu2carte.getJeuPoker();
	}


	/**
	 * pour cr�er une partie de poker
	 * et ajoute le joueur courant
	 * 
	 * @param poker une partie de poker
	 */
	public void setPoker(Poker poker) {
		poker.addJoueur(getJcourant());
		jeu2carte.setJeuPoker(poker);
	}


	/**
	 * 
	 * @return la partie de blackjack du casino
	 */
	public BlackJack getBlackJack() {
		return jeu2carte.getJeuBJ();
	}


	/**
	 * pour cr�er une partie de backjack
	 * et ajoute le joueur courant
	 * 
	 * @param backjack une partie de backjack
	 */
	public void setBlackJack(BlackJack blackJack) {
		blackJack.addJoueur(getJcourant());
		jeu2carte.setJeuBJ(blackJack);
	}


	/**
	 * 
	 * @return la partie de roulette du casino
	 */
	public Roulette getRoulette() {
		return roulette;
	}


	/**
	 * pour cr�er une partie de roulette
	 * et ajoute le joueur courant
	 * 
	 * @param roulette une partie de roulette
	 */
	public void setRoulette(Roulette roulette) {
		this.roulette = roulette;
		this.roulette.setJcourant(Jcourant);
	}


	/**
	 * 
	 * @return la partie de machine � sou du casino
	 */
	public MachineSou getMachineSou() {
		return machineSou;
	}


	/**
	 * pour cr�er une partie de machine � sou
	 * et ajoute le joueur courant
	 * 
	 * @param machineSou une partie de machine � sou
	 */
	public void setMachineSou(MachineSou machineSou) {
		this.machineSou = machineSou;
		this.machineSou.setJcourant(Jcourant);
	}


	/**
	 * 
	 * @return la partie de pari du casino
	 */
	public Pari getPari() {
		return pari;
	}


	/**
	 * pour cr�er une partie de pari
	 * et ajoute le joueur courant
	 * 
	 * @param pari une partie de pari
	 */
	public void setPari(Pari pari) {
		this.pari = pari;
		this.pari.setJcourant(Jcourant);
	}
	
	/**
	 * 
	 * @return le menu du casino
	 */
	public Menu getMenu() {
		return menu;
	}


	/**
	 * pour cr�er le menu
	 * 
	 * @param menu un menu
	 */
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	

	public ThreadJeu getJeu2carte() {
		return jeu2carte;
	}

	public JoueurCourant getJfacile() {
		return Jfacile;
	}

	public JoueurCourant getJmoyen() {
		return Jmoyen;
	}

	public JoueurCourant getJdifficile() {
		return Jdifficile;
	}

	public JoueurCourant getJlibre() {
		return Jlibre;
	}


	public JoueurCourant getJcourant() {
		return Jcourant;
	}


	public void setJcourant(JoueurCourant jcourant) {
		Jcourant = jcourant;
	}
	
	public boolean hasTrophee(int tr) {
		return trophee[tr];
	}
	
	public void addTrophee(int tr) {
		trophee[tr] = true;
	}
	
	
}
