package casino;

import utilisateur.Timer;

abstract public class Jeu {

	//la mise pour acc�der au jeu
	protected int mise;
	protected Timer time; //le temps d'un jeu
	protected boolean play; //savoir si le jeu est lanc�

	public Jeu(long time) {
		this.time = new Timer(time);
		play = false;
	}
	
	
	public int getMise() {
		return mise;
	}


	/**
	 * entre 0 et 500
	 * @param mise la mise � ajouter ou diminuer
	 */
	public void setMise(int mise) {
		if(this.mise+mise<1) {
			this.mise = 500;
		}else if(this.mise+mise>500){
			this.mise = 1;
		}else {
			this.mise += mise;
		}
	}



	public boolean isTimeFinished() {
		return time.hasFinished();
	}


	public void resetTime() {
		this.time.restart();
	}
	
	
	public void finirTimer() {
		this.time.finTimer();
	}
	
	public float getTime() {
		return this.time.getTime()/1000;
	}

	public boolean isPlay() {
		return play;
	}


	public void setPlay(boolean play) {
		this.play = play;
	}


	/**
	 * lance une partie et s'occupe de modifier 
	 * la nouvelle somme de jeton du joueur en d�but et fin de parti
	 * 
	 * precondition : le joueur a assez de jeton pour joueur
	 * @param joueur le joueur qui va jouer
	 * @return -1 si il y a un probl�me 0 si la partie est perdu et 1 si gagn�
	 * 
	 */
	abstract public int play();
	
	protected void attends(int sec) {
		try {
			Thread.sleep(sec*1000);
		} catch (InterruptedException e) {
			Thread.interrupted();
			e.printStackTrace();
		}
	}
}
