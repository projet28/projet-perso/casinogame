package utilisateur;

public class Action{

	private char key; // la touche � r�cuperer
	
	public static final int casino = 0, poker = 1, blackjack = 2, roulette = 3,
			machine = 4, pari = 5, menu = 6;
	private boolean cas, pok, bj, roul, mach, par, men;
	
	public Action() {
		key = ' ';
		cas = false;
		pok = false;
		bj = false;
		roul = false;
		mach = false;
		par = false;
		men = false;
	}
	
	public char getKey() {
		return key;
	}

	public void setKey(char key) {
		if(key != 'e') this.key = key;
	}
	
	private void allfalse() {
		cas = false;
		pok = false;
		bj = false;
		roul = false;
		mach = false;
		par = false;
		men = false;
	}
	
	/**
	 * 
	 * @param lieu le lieu pour les actions
	 * @return -1 si erreur 0 sinon
	 */
	public int setLieu(int lieu) {
		switch (lieu) {
			case casino : allfalse(); cas = true; break;
			case poker : allfalse(); pok = true; break;
			case blackjack : allfalse(); bj = true; break;
			case roulette : allfalse(); roul = true; break;
			case machine : allfalse(); mach = true; break;
			case pari : allfalse(); par = true; break;
			case menu : allfalse(); men = true; break;
			default : return -1;
		}
		return 0;
	}
	
	/**
	 * 
	 * @return le lieu de l'action ou -1 si erreur
	 */
	public int getLieu() {
		if(cas) return casino;
		if(pok) return poker;
		if(bj) return blackjack;
		if(roul) return roulette;
		if(mach) return machine;
		if(par) return pari;
		if(men) return menu;
		return -1;
	}

	/**
	 * utilise le clavier: r: relance
	 * 					   s: suivre
	 * 					   c: coucher
	 * 					   t: tapis
	 * 
	 * @param mise la mise courante si 0 alors encore aucune mise
	 * @param blind la blind mise de d�part (mise minimum)
	 * @return c si se couche 
	 * 		   s si il suit
	 * 		   r si il relance
	 * 		   t si il tapis
	 * 		   e si il quitte
	 */
	public char actionPoker() {
		if(key == 'c') return 'c';
		if(key == 's') return 's';
		if(key == 't') return 't';
		if(key == 'r') return 'r';
		if(key == (char) 27) return 'e';
		return ' ';
	}
	
	
	/**
	 * pour savoir quelle mise le joueur va mettre
	 * 
	 * @return la mise du joueur ou -1 si erreur
	 */
	public char actionMiserBJ() {
		if(key == 'u') return 'u';
		if(key == 'd') return 'd';
		if(key == 'a') return 'a';
		if(key == (char) 27) return 'e';
		return ' ';
	}
	
	/**
	 * @return -1 si il reste :r
	 * 			1 si il veut une autre carte :c
	 * 			2 si il split :s
	 * 			3 si il double :d
	 * 			-11 si erreur
	 * 
	 */
	public char actionBJ() {
		if(key == 'c') return 'c';
		if(key == 's') return 's';
		if(key == 'r') return 'r';
		if(key == 'd') return 'd';
		if(key == (char) 27) return 'e';
		return ' ';
	}
}
