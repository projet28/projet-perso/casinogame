package utilisateur;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class Timer {

	private Instant debut;
	private Instant fin;
	private long duree; // duree du timer en millisecondes
	private boolean fini;
	
	public Timer(long duree) {
		this.duree = duree;
		restart();
	}
	
	/**
	 * met le timer � la fin
	 */
	public void finTimer() {
		fini = true;
	}
	
	/**
	 * redemarre le timer � 0
	 */
	public void restart() {
		fini = false;
		debut = Instant.now();
		fin = Instant.now().plus(duree, ChronoUnit.MILLIS);
	}
	
	/**
	 * 
	 * @return true si le timer est fini false sinon
	 */
	public boolean hasFinished() {
		if(fini) {
			return true;
		}else {
			if(debut.compareTo((Instant.now().minus(duree, ChronoUnit.MILLIS))) < 0) {
				fini = false;
				return true;
			}else {
				return false;
			}
		}
	}
	
	/**
	 * 
	 * @return le temps restant
	 */
	public long getTime() {
		return Instant.now().until(fin, ChronoUnit.MILLIS);
	}

}
