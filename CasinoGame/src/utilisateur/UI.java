package utilisateur;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import org.newdawn.slick.Animation;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

import casino.Casino;
import casino.Menu;
import jeu2carte.BlackJack;
import jeu2carte.MainBJ;
import jeu2carte.Poker;
import jeu2chance.MachineSou;
import jeu2chance.Pari;
import jeu2chance.Roulette;
import joueur.IA;
import joueur.JoueurCourant;

public class UI extends BasicGame{
	private Casino casino; //le casino
	private Action act;	//attraper les evenements clavier
	
	private Input input;
	
	private boolean close;
	
	private Timer touche;//timer de touche
	private Timer toucheDown;
	private String erreur;//pour afficher les erreurs
	private Timer timerErr;//timer afficher erreur
	private String message;//pour afficher des messages
	private Timer timerMess;//timer afficher messages
	
	private boolean aJouer;//savoir si il y a une partie de carte de lancer
	private int tmpJ;//recuperer les jetons gagn�
	
	private int cheval;//pour savoir quelle cheval est selectionn� dans pari
	
	private boolean fulls;//grand ecran ou non
	
	//tous les sons
	private boolean son;//activer ou non le son
	private Music ambiance;
	private Music ambianceCarte;
	private Music ambianceChance;
	private SoundEffect bruit;
	
	//toutes les images
	private Image retour;
	//credit
	private Image credit;
	//tuto
	private Image tutoPoker;
	private Image tutoBJ;
	private Image tutoMachine;
	private Image tutoRoulette;
	private Image tutoPari;
	//perso
	private Image icon;
	private Image verre;
	private Image patteDeLapin;
	private Image abonnement;
	private Image gardeDuCorps;
	private Image trophee1;
	private Image trophee2;
	private Image trophee3;
	//fond
	private Image fondTexte;
	private Image fondTexteSelect;
	private Image fondTexte2;
	private Image fondTexteSelect2;
	private Image fondPerso;
	private Image fondMess;
	private Image fondErr;
	private Image fondInfo;
	private Image fondInfo2;
	private Image fondInfo3;
	private Image fondInfo4;
	private Image fondInfo5;
	private Image fondInfo6;
	private Image fondInfo7;
	private Image fondCasino;
	private Image fondMenu;
	private Image fondPoker;
	private Image fondBJ;
	private Image fondMachine;
	private Image fondRoulette;
	private Image fondPari;
	private Image contourPoker;
	private Image contourBJ;
	private Image contourRoulette;
	private Image contourMachine;
	private Image contourPari;
	private Image contourMenu;
	//poker BJ
	private Image carteBack;
	private Image[][] cartes;
	private Image dealer;
	private Image pBlind;
	private Image gBlind;
	private Image jeton1;
	private Image jeton5;
	private Image jeton10;
	private Image jeton25;
	private Image jeton100;
	private Image jeton500;
	private Image jeton1000;
	private Image jeton5000;
	//machine
	private Map<String,Image> symbole;
	private Map<String,Image> chevauxFixe;
	
	//map des positions
	private Map<String,Position> positions;
	private List<Position> billes;
	private int posBille;
	
	//sprite pour anim
	private Timer timeBille;
	private Image bille;
	
	private Timer timeCourse;
	private Map<String, Integer> posCheval;
	private Map<String, Integer> vitesseCheval;

	//les animations
	private Animation animR;
	
	private Animation animM1;
	private Animation animM2;
	private Animation animM3;
	private Animation animM4;
	private Animation animM5;
	private Animation animM6;
	private Animation animM7;
	private Animation animM8;
	private Animation animM9;
	
	private Map<String,Animation> chevaux;
	
	private class Position {
		private int posX;
		private int posY;
		
		private Position bouton;
		private Position paquet;
		private Position jetons;
		private Position info;
		
		public Position(int x, int y) {
			this.posX = x;
			this.posY = y;
		}
		
		public Position(int x, int y, int jx, int jy, int ix, int iy) {
			this.posX = x;
			this.posY = y;
			jetons = new Position(posX+jx,posY+jy);
			info = new Position(posX+ix,posY+iy);
		}
		
		public Position(int x, int y, int bx, int by, int px, int py, int jx, int jy, int ix, int iy) {
			this.posX = x;
			this.posY = y;
			bouton = new Position(posX+bx,posY+by);
			paquet = new Position(posX+px,posY+py);
			jetons = new Position(posX+jx,posY+jy);
			info = new Position(posX+ix,posY+iy);
		}
	}
	
	public UI(Casino casino, Action act)
    {
		super("casino virtuelle");
		this.casino = casino;
		this.act = act;
		act.setLieu(Action.casino);
		
		new Thread(casino.getJeu2carte(), "jeu").start();
		
		close = false;
		
		aJouer = false;
		
		cheval = 0;
		
		touche = new Timer(200);
		toucheDown = new Timer(50);
		erreur = "";
		timerErr = new Timer(4000);
		timerErr.finTimer();
		message = "";
		timerMess = new Timer(4000);
		timerMess.finTimer();
		
		timeBille = new Timer(100);
		posBille = 0;
		
		timeCourse = new Timer(80);
		
		son = true;
		bruit = new SoundEffect();
		fulls = true;
		charger();
    }
	
	
    public boolean isClose() {
		return close;
	}


	@Override
        public void init(GameContainer gc) throws SlickException
    {
		input = gc.getInput();
    	//initialise les sons
    	ambiance = new Music("son/ambiance.wav");
    	ambianceChance = new Music("son/ambianceChance.wav");
    	ambianceCarte = new Music("son/ambianceCarte.wav");
    	if(son) ambiance.loop();
    	
    	//initialise les images
    	retour = new Image("images/retour.png");
    	//credit
    	credit = new Image("images/credit.png");
    	
    	//tuto
    	tutoPoker = new Image("images/tuto/tutoPoker.png");
    	tutoBJ = new Image("images/tuto/tutoBJ.png");
    	tutoMachine = new Image("images/tuto/tutoMachine.png");
    	tutoRoulette = new Image("images/tuto/tutoRoulette.png");
    	tutoPari = new Image("images/tuto/tutoPari.png");
    	
    	//perso
    	icon = new Image(casino.getJcourant().getIcon());
    	verre = new Image("images/verre.png");
    	patteDeLapin = new Image("images/patte_de_lapin.png");
    	abonnement = new Image("images/abonnement.png");
    	gardeDuCorps = new Image("images/garde_du_corps.png");
    	trophee1 = new Image("images/trophee1.png");
    	trophee2 = new Image("images/trophee2.png");
    	trophee3 = new Image("images/trophee3.png");
    	
    	//fond
    	fondTexte = new Image("images/fondTexte.png");
    	fondTexteSelect = new Image("images/fondTexteSelect.png");
    	fondTexte2 = new Image("images/fondTexte2.png");
    	fondTexteSelect2 = new Image("images/fondTexteSelect2.png");
    	fondPerso = new Image("images/fondPerso.png");
    	fondMess = new Image("images/fondMess.png");
    	fondErr = new Image("images/fondErr.png");
    	fondInfo = new Image("images/fondInfo.png");
    	fondInfo2 = new Image("images/fondInfo2.png");
    	fondInfo3 = new Image("images/fondInfo3.png");
    	fondInfo4 = new Image("images/fondInfo4.png");
    	fondInfo5 = new Image("images/fondInfo5.png");
    	fondInfo6 = new Image("images/fondInfo6.png");
    	fondInfo7 = new Image("images/fondInfo7.png");
    	fondCasino = new Image("images/fondCasino.png");
    	fondPoker = new Image("images/fondPoker.png");
    	fondBJ = new Image("images/fondBJ.png");
    	fondMachine = new Image("images/fondMachine.png");
    	fondRoulette = new Image("images/fondRoulette.png");
    	fondPari = new Image("images/fondPari.png");
    	fondMenu = new Image("images/fondMenu.png");
    	contourPoker = new Image("images/contourPoker.png");
    	contourBJ = new Image("images/contourBJ.png");
    	contourRoulette = new Image("images/contourRoulette.png");
    	contourMachine = new Image("images/contourMachine.png");
    	contourPari = new Image("images/contourPari.png");
    	contourMenu = new Image("images/contourMenu.png");
    	
    	//poker BJ
    	carteBack = new Image("images/cartes/back.png");
    	cartes = new Image[4][13];
    	for(int i=0;i<4;i++) {
    		for(int j=1;j<14;j++) {
    			cartes[i][j-1] = new Image("images/cartes/"+i+j+".png");
    		}
    	}
    	dealer = new Image("images/jetons/dealer.png");
    	pBlind = new Image("images/jetons/pBlind.png");
    	gBlind = new Image("images/jetons/gBlind.png");
    	jeton1 = new Image("images/jetons/jeton1.png");
    	jeton5 = new Image("images/jetons/jeton5.png");
    	jeton10 = new Image("images/jetons/jeton10.png");
    	jeton25 = new Image("images/jetons/jeton25.png");
    	jeton100 = new Image("images/jetons/jeton100.png");
    	jeton500 = new Image("images/jetons/jeton500.png");
    	jeton1000 = new Image("images/jetons/jeton1000.png");
    	jeton5000 = new Image("images/jetons/jeton5000.png");
    	//machine
    	symbole  = new TreeMap<String,Image>();
    	symbole.put("M1", new Image("images/machine/s1.png"));
    	symbole.put("M2", new Image("images/machine/s2.png"));
    	symbole.put("M3", new Image("images/machine/s3.png"));
    	symbole.put("M4", new Image("images/machine/s4.png"));
    	symbole.put("M5", new Image("images/machine/s5.png"));
    	symbole.put("M6", new Image("images/machine/s6.png"));
    	symbole.put("M7", new Image("images/machine/s7.png"));
    	symbole.put("M8", new Image("images/machine/s8.png"));
    	symbole.put("M9", new Image("images/machine/s9.png"));
    	
    	chevauxFixe = new TreeMap<String,Image>();
    	chevauxFixe.put("P0", new Image("images/cheval/c0.png"));
    	chevauxFixe.put("P1", new Image("images/cheval/c1.png"));
    	chevauxFixe.put("P2", new Image("images/cheval/c2.png"));
    	chevauxFixe.put("P3", new Image("images/cheval/c3.png"));
    	chevauxFixe.put("P4", new Image("images/cheval/c4.png"));
    	chevauxFixe.put("P5", new Image("images/cheval/c5.png"));
    	chevauxFixe.put("P6", new Image("images/cheval/c6.png"));
    	chevauxFixe.put("P7", new Image("images/cheval/c7.png"));
    	chevauxFixe.put("P8", new Image("images/cheval/c8.png"));
    	chevauxFixe.put("P9", new Image("images/cheval/c9.png"));
    	
    	//la map des positions
    	positions = new TreeMap<String,Position>();
    	//position poker
    	positions.put("P0", new Position(950,600,-55, -10, -130, 0, 125, 26, 0, 0));
    	positions.put("P1", new Position(520,450,30, -50, 50, 120, 110, 18, -270, 0));
    	positions.put("P2", new Position(650,330,150, 30, 200, -5, 150, 125, 0, -100));
    	positions.put("P3", new Position(1200,340,150, 20, -75, -10, 100, 125, 0, -100));
    	positions.put("P4", new Position(1400,450,-30, 150, -50, -55, -50, 150, 150, 0));
    	//position BJ
    	positions.put("B0", new Position(950, 550, 0, 400, 0, 0));
    	positions.put("B1", new Position(410, 420, 20, 400, -130, 450));
    	positions.put("B2", new Position(620, 500, 60, 400, -130, 450));
    	positions.put("B3", new Position(1200, 500, 0, 400, 100, 450));
    	positions.put("B4", new Position(1400, 420, 60, 400, 100, 450));
    	positions.put("B5", new Position(840,50,0,0,0,0));
    	//position jeton roulette
    	positions.put("R10", new Position(780,520));
    	positions.put("R100", new Position(780,400));
    	positions.put("R11", new Position(840,570));
    	positions.put("R12", new Position(840,470));
    	positions.put("R13", new Position(840,370));
    	positions.put("R14", new Position(915,570));
    	positions.put("R15", new Position(915,470));
    	positions.put("R16", new Position(915,370));
    	positions.put("R17", new Position(1000,570));
    	positions.put("R18", new Position(1000,470));
    	positions.put("R19", new Position(1000,370));
    	positions.put("R110", new Position(1075,570));
    	positions.put("R111", new Position(1075,470));
    	positions.put("R112", new Position(1075,370));
    	positions.put("R113", new Position(1150,570));
    	positions.put("R114", new Position(1150,470));
    	positions.put("R115", new Position(1150,370));
    	positions.put("R116", new Position(1225,570));
    	positions.put("R117", new Position(1225,470));
    	positions.put("R118", new Position(1225,370));
    	positions.put("R119", new Position(1300,570));
    	positions.put("R120", new Position(1300,470));
    	positions.put("R121", new Position(1300,370));
    	positions.put("R122", new Position(1375,570));
    	positions.put("R123", new Position(1375,470));
    	positions.put("R124", new Position(1375,370));
    	positions.put("R125", new Position(1450,570));
    	positions.put("R126", new Position(1450,470));
    	positions.put("R127", new Position(1450,370));
    	positions.put("R128", new Position(1525,570));
    	positions.put("R129", new Position(1525,470));
    	positions.put("R130", new Position(1525,370));
    	positions.put("R131", new Position(1600,570));
    	positions.put("R132", new Position(1600,470));
    	positions.put("R133", new Position(1600,370));
    	positions.put("R134", new Position(1675,570));
    	positions.put("R135", new Position(1675,470));
    	positions.put("R136", new Position(1675,370));
    	
    	positions.put("R200", new Position(785,470));
    	positions.put("R201", new Position(810,570));
    	positions.put("R202", new Position(810,490));
    	positions.put("R2002", new Position(810,440));
    	positions.put("R203", new Position(810,370));
    	positions.put("R214", new Position(885,570));
    	positions.put("R225", new Position(885,470));
    	positions.put("R236", new Position(885,370));
    	positions.put("R247", new Position(960,570));
    	positions.put("R258", new Position(960,470));
    	positions.put("R269", new Position(960,370));
    	positions.put("R2710", new Position(1035,570));
    	positions.put("R2811", new Position(1035,470));
    	positions.put("R2912", new Position(1035,370));
    	positions.put("R21013", new Position(1110,570));
    	positions.put("R21114", new Position(1110,470));
    	positions.put("R21215", new Position(1110,370));
    	positions.put("R21316", new Position(1185,570));
    	positions.put("R21417", new Position(1185,470));
    	positions.put("R21518", new Position(1185,370));
    	positions.put("R21619", new Position(1260,570));
    	positions.put("R21720", new Position(1260,470));
    	positions.put("R21821", new Position(1260,370));
    	positions.put("R21922", new Position(1335,570));
    	positions.put("R22023", new Position(1335,470));
    	positions.put("R22124", new Position(1335,370));
    	positions.put("R22225", new Position(1410,570));
    	positions.put("R22326", new Position(1410,470));
    	positions.put("R22427", new Position(1410,370));
    	positions.put("R22528", new Position(1485,570));
    	positions.put("R22629", new Position(1485,470));
    	positions.put("R22730", new Position(1485,370));
    	positions.put("R22831", new Position(1560,570));
    	positions.put("R22932", new Position(1560,470));
    	positions.put("R23033", new Position(1560,370));
    	positions.put("R23134", new Position(1635,570));
    	positions.put("R23235", new Position(1635,470));
    	positions.put("R23336", new Position(1635,370));
    	positions.put("R212", new Position(840,510));
    	positions.put("R223", new Position(840,410));
    	positions.put("R245", new Position(915,510));
    	positions.put("R256", new Position(915,410));
    	positions.put("R278", new Position(990,510));
    	positions.put("R289", new Position(990,410));
    	positions.put("R21011", new Position(1065,510));
    	positions.put("R21112", new Position(1065,410));
    	positions.put("R21314", new Position(1140,510));
    	positions.put("R21415", new Position(1140,410));
    	positions.put("R21617", new Position(1215,510));
    	positions.put("R21718", new Position(1215,410));
    	positions.put("R21920", new Position(1290,510));
    	positions.put("R22021", new Position(1290,410));
    	positions.put("R22223", new Position(1365,510));
    	positions.put("R22324", new Position(1365,410));
    	positions.put("R22526", new Position(1440,510));
    	positions.put("R22627", new Position(1440,410));
    	positions.put("R22829", new Position(1515,510));
    	positions.put("R22930", new Position(1515,410));
    	positions.put("R23132", new Position(1590,510));
    	positions.put("R23233", new Position(1590,410));
    	positions.put("R23435", new Position(1665,510));
    	positions.put("R23536", new Position(1665,410));
    	
    	positions.put("R301", new Position(810,520));
    	positions.put("R300", new Position(810,460));
    	positions.put("R302", new Position(810,420));
    	positions.put("R312", new Position(840,610));
    	positions.put("R345", new Position(920,610));
    	positions.put("R378", new Position(995,610));
    	positions.put("R31011", new Position(1070,610));
    	positions.put("R31314", new Position(1150,610));
    	positions.put("R31617", new Position(1222,610));
    	positions.put("R31920", new Position(1300,610));
    	positions.put("R32223", new Position(1375,610));
    	positions.put("R32526", new Position(1450,610));
    	positions.put("R32829", new Position(1525,610));
    	positions.put("R33132", new Position(1600,610));
    	positions.put("R33435", new Position(1675,610));
    	
    	positions.put("R41", new Position(880,520));
    	positions.put("R42", new Position(880,420));
    	positions.put("R44", new Position(960,520));
    	positions.put("R45", new Position(960,420));
    	positions.put("R47", new Position(1035,520));
    	positions.put("R48", new Position(1035,420));
    	positions.put("R410", new Position(1110,520));
    	positions.put("R411", new Position(1110,420));
    	positions.put("R413", new Position(1190,520));
    	positions.put("R414", new Position(1190,420));
    	positions.put("R416", new Position(1262,520));
    	positions.put("R417", new Position(1262,420));
    	positions.put("R419", new Position(1340,520));
    	positions.put("R420", new Position(1340,420));
    	positions.put("R422", new Position(1415,520));
    	positions.put("R423", new Position(1415,420));
    	positions.put("R425", new Position(1490,520));
    	positions.put("R426", new Position(1490,420));
    	positions.put("R428", new Position(1565,520));
    	positions.put("R429", new Position(1565,420));
    	positions.put("R431", new Position(1640,520));
    	positions.put("R432", new Position(1640,420));
    	
    	positions.put("R5", new Position(810,610));
    	
    	positions.put("R61", new Position(880,610));
    	positions.put("R64", new Position(960,610));
    	positions.put("R67", new Position(1035,610));
    	positions.put("R610", new Position(1110,610));
    	positions.put("R613", new Position(1190,610));
    	positions.put("R616", new Position(1262,610));
    	positions.put("R619", new Position(1340,610));
    	positions.put("R622", new Position(1415,610));
    	positions.put("R625", new Position(1490,610));
    	positions.put("R628", new Position(1565,610));
    	positions.put("R631", new Position(1640,610));
    	
    	positions.put("R712", new Position(850,669));
    	positions.put("R71314", new Position(1200,681));
    	positions.put("R72526", new Position(1480,660));
    	positions.put("R714", new Position(1741,572));
    	positions.put("R725", new Position(1738,466));
    	positions.put("R736", new Position(1744,380));
    	
    	positions.put("R81", new Position(850,769));
    	positions.put("R82", new Position(1000,781));
    	positions.put("R83", new Position(1180,760));
    	positions.put("R84", new Position(1340,772));
    	positions.put("R85", new Position(1520,766));
    	positions.put("R86", new Position(1650,780));
    	
    	//position bille
    	billes = new ArrayList<Position>();
    	billes.add(new Position(350, 390));
    	billes.add(new Position(380, 393));
    	billes.add(new Position(410, 396));
    	billes.add(new Position(440, 410));
    	billes.add(new Position(464, 425));
    	billes.add(new Position(485, 450));
    	billes.add(new Position(505, 470));
    	billes.add(new Position(525, 500));
    	billes.add(new Position(535, 530));
    	billes.add(new Position(540, 560));
    	billes.add(new Position(540, 590));
    	billes.add(new Position(530, 620));
    	billes.add(new Position(520, 650));
    	billes.add(new Position(510, 680));
    	billes.add(new Position(495, 705));
    	billes.add(new Position(465, 715));
    	billes.add(new Position(440, 740));
    	billes.add(new Position(410, 750));
    	billes.add(new Position(380, 755));
    	billes.add(new Position(350, 755));
    	billes.add(new Position(320, 750));
    	billes.add(new Position(290, 745));
    	billes.add(new Position(265, 740));
    	billes.add(new Position(239, 715));
    	billes.add(new Position(215, 700));
    	billes.add(new Position(195, 670));
    	billes.add(new Position(185, 650));
    	billes.add(new Position(175, 615));
    	billes.add(new Position(175, 590));
    	billes.add(new Position(170, 560));
    	billes.add(new Position(170, 530));
    	billes.add(new Position(175, 500));
    	billes.add(new Position(185, 470));
    	billes.add(new Position(210, 450));
    	billes.add(new Position(235, 425));
    	billes.add(new Position(256, 410));
    	billes.add(new Position(290, 393));
    	billes.add(new Position(320, 390));
    	
    	//sprite
    	bille = new Image("images/bille.png");
    	
    	//initialise les anim
    	animR = new Animation();
    	animR.addFrame(new Image("images/anim/r1.png"), 145);
    	animR.addFrame(new Image("images/anim/r2.png"), 145);
    	animR.addFrame(new Image("images/anim/r3.png"), 145);
    	animR.addFrame(new Image("images/anim/r4.png"), 145);
    	animR.addFrame(new Image("images/anim/r5.png"), 145);
    	animR.addFrame(new Image("images/anim/r6.png"), 145);
    	animR.addFrame(new Image("images/anim/r7.png"), 145);
    	animR.addFrame(new Image("images/anim/r8.png"), 145);
    	animR.addFrame(new Image("images/anim/r9.png"), 145);
    	animR.addFrame(new Image("images/anim/r10.png"), 145);
    	animR.addFrame(new Image("images/anim/r11.png"), 145);
    	animR.addFrame(new Image("images/anim/r12.png"), 145);
    	animR.addFrame(new Image("images/anim/r13.png"), 145);
    	animR.addFrame(new Image("images/anim/r14.png"), 145);
    	animR.addFrame(new Image("images/anim/r15.png"), 145);
    	animR.addFrame(new Image("images/anim/r16.png"), 145);
    	animR.addFrame(new Image("images/anim/r17.png"), 145);
    	animR.addFrame(new Image("images/anim/r18.png"), 145);
    	animR.addFrame(new Image("images/anim/r19.png"), 145);
    	animR.addFrame(new Image("images/anim/r20.png"), 145);
    	animR.addFrame(new Image("images/anim/r21.png"), 145);
    	animR.addFrame(new Image("images/anim/r22.png"), 145);
    	animR.addFrame(new Image("images/anim/r23.png"), 145);
    	animR.addFrame(new Image("images/anim/r24.png"), 145);
    	animR.addFrame(new Image("images/anim/r25.png"), 145);
    	animR.addFrame(new Image("images/anim/r26.png"), 145);
    	animR.addFrame(new Image("images/anim/r27.png"), 145);
    	animR.addFrame(new Image("images/anim/r28.png"), 145);
    	animR.addFrame(new Image("images/anim/r29.png"), 145);
    	animR.addFrame(new Image("images/anim/r30.png"), 145);
    	animR.addFrame(new Image("images/anim/r31.png"), 145);
    	animR.addFrame(new Image("images/anim/r32.png"), 145);
    	animR.addFrame(new Image("images/anim/r33.png"), 145);
    	animR.addFrame(new Image("images/anim/r34.png"), 145);
    	animR.addFrame(new Image("images/anim/r35.png"), 145);
    	animR.addFrame(new Image("images/anim/r36.png"), 145);
    	animR.addFrame(new Image("images/anim/r37.png"), 145);
    	animR.addFrame(new Image("images/anim/r38.png"), 145);
    	
    	animM1 = new Animation();
    	animM1.addFrame(new Image("images/machine/s1.png"), 100);
    	animM1.addFrame(new Image("images/machine/s2.png"), 100);
    	animM1.addFrame(new Image("images/machine/s3.png"), 100);
    	animM1.addFrame(new Image("images/machine/s4.png"), 100);
    	animM1.addFrame(new Image("images/machine/s5.png"), 100);
    	animM1.addFrame(new Image("images/machine/s6.png"), 100);
    	animM1.addFrame(new Image("images/machine/s7.png"), 100);
    	animM1.addFrame(new Image("images/machine/s8.png"), 100);
    	animM1.addFrame(new Image("images/machine/s9.png"), 100);
    	
    	animM2 = animM1.copy();
    	animM3 = animM1.copy();
    	animM4 = animM1.copy();
    	animM5 = animM1.copy();
    	animM6 = animM1.copy();
    	animM7 = animM1.copy();
    	animM8 = animM1.copy();
    	animM9 = animM1.copy();
    	
    	animM1.setCurrentFrame(2);
    	animM2.setCurrentFrame(5);
    	animM3.setCurrentFrame(6);
    	animM4.setCurrentFrame(9);
    	animM5.setCurrentFrame(1);
    	animM6.setCurrentFrame(3);
    	animM7.setCurrentFrame(7);
    	animM8.setCurrentFrame(4);
    	animM9.setCurrentFrame(8);
    	
    	//pari
    	chevaux  = new TreeMap<String,Animation>();
    	SpriteSheet ch1 = new SpriteSheet("images/anim/cheval0.png", 128, 128);
    	Animation animC1 = new Animation(ch1, 150);
    	chevaux.put("P0", animC1);
    	SpriteSheet ch2 = new SpriteSheet("images/anim/cheval1.png", 128, 128);
    	Animation animC2 = new Animation(ch2, 150);
    	chevaux.put("P1", animC2);
    	SpriteSheet ch3 = new SpriteSheet("images/anim/cheval2.png", 128, 128);
    	Animation animC3 = new Animation(ch3, 150);
    	chevaux.put("P2", animC3);
    	SpriteSheet ch4 = new SpriteSheet("images/anim/cheval3.png", 128, 128);
    	Animation animC4 = new Animation(ch4, 150);
    	chevaux.put("P3", animC4);
    	SpriteSheet ch5 = new SpriteSheet("images/anim/cheval4.png", 128, 128);
    	Animation animC5 = new Animation(ch5, 150);
    	chevaux.put("P4", animC5);
    	SpriteSheet ch6 = new SpriteSheet("images/anim/cheval5.png", 128, 128);
    	Animation animC6 = new Animation(ch6, 150);
    	chevaux.put("P5", animC6);
    	SpriteSheet ch7 = new SpriteSheet("images/anim/cheval6.png", 128, 128);
    	Animation animC7 = new Animation(ch7, 150);
    	chevaux.put("P6", animC7);
    	SpriteSheet ch8 = new SpriteSheet("images/anim/cheval7.png", 128, 128);
    	Animation animC8 = new Animation(ch8, 150);
    	chevaux.put("P7", animC8);
    	SpriteSheet ch9 = new SpriteSheet("images/anim/cheval8.png", 128, 128);
    	Animation animC9 = new Animation(ch9, 150);
    	chevaux.put("P8", animC9);
    	SpriteSheet ch10 = new SpriteSheet("images/anim/cheval9.png", 128, 128);
    	Animation animC10 = new Animation(ch10, 150);
    	chevaux.put("P9", animC10);
    	
    	posCheval = new TreeMap<String, Integer>();
    	posCheval.put("P0", 0);
    	posCheval.put("P1", 0);
    	posCheval.put("P2", 0);
    	posCheval.put("P3", 0);
    	posCheval.put("P4", 0);
    	posCheval.put("P5", 0);
    	posCheval.put("P6", 0);
    	posCheval.put("P7", 0);
    	posCheval.put("P8", 0);
    	posCheval.put("P9", 0);
    	
    	vitesseCheval = new TreeMap<String, Integer>();
    }

    @Override
        public void update(GameContainer gc, int delta) throws SlickException
    {
    	//fermer le jeu
    	if(close) {
    		gc.exit();
    	}
    	
    	//actualiser fenetre et icon
    	gc.setFullscreen(fulls);
    	icon = new Image(casino.getJcourant().getIcon());
    	
    	//anim
    	animR.update(delta);
    	animM1.update(delta);
    	animM2.update(delta);
    	animM3.update(delta);
    	animM4.update(delta);
    	animM5.update(delta);
    	animM6.update(delta);
    	animM7.update(delta);
    	animM8.update(delta);
    	animM9.update(delta);
    	chevaux.values().forEach(a -> a.update(delta));
    	
    	//mise BJ, roulette et pari et relance poker
    	if(act.getLieu() == Action.poker && !casino.getPoker().isTimeFinished() &&
    			casino.getPoker().isRelance() && casino.getPoker().isPlay()) {
			if(input.isKeyDown(Input.KEY_UP) && touche.hasFinished() && toucheDown.hasFinished()) {
				toucheDown.restart();
				casino.getPoker().setNbRelance(1);
			}
			if(input.isKeyDown(Input.KEY_DOWN) && touche.hasFinished() && toucheDown.hasFinished()) {
				toucheDown.restart();
				casino.getPoker().setNbRelance(-1);
			}
		}
    	
    	if(act.getLieu() == Action.blackjack && !casino.getBlackJack().isTimeFinished() &&
    			casino.getBlackJack().isMiseAct() && casino.getBlackJack().isPlay()) {
			if(input.isKeyDown(Input.KEY_UP) && touche.hasFinished() && toucheDown.hasFinished()) {
				toucheDown.restart();
				casino.getBlackJack().setMise(1);
			}
			if(input.isKeyDown(Input.KEY_DOWN) && touche.hasFinished() && toucheDown.hasFinished()) {
				toucheDown.restart();
				casino.getBlackJack().setMise(-1);
			}
		}
    	
    	if(act.getLieu() == Action.roulette && casino.getRoulette().isTimeFinished()) {
			if(input.isKeyDown(Input.KEY_RIGHT) && touche.hasFinished() && toucheDown.hasFinished()) {
				toucheDown.restart();
				casino.getRoulette().setMise(1);
			}
			if(input.isKeyDown(Input.KEY_LEFT) && touche.hasFinished() && toucheDown.hasFinished()) {
				toucheDown.restart();
				casino.getRoulette().setMise(-1);
			}
		}
    	
    	if(act.getLieu() == Action.pari && casino.getPari().isTimeFinished()) {
			if(input.isKeyDown(Input.KEY_RIGHT) && touche.hasFinished() && toucheDown.hasFinished()) {
				toucheDown.restart();
				casino.getPari().setMise(1);
			}
			if(input.isKeyDown(Input.KEY_LEFT) && touche.hasFinished() && toucheDown.hasFinished()) {
				toucheDown.restart();
				casino.getPari().setMise(-1);
			}
		}
    	
    	//gerer fin de partie carte
    	if(act.getLieu() == Action.poker && aJouer && !casino.getPoker().isPlay()) {
    		if(casino.getJeu2carte().getResultPoker()==1) {
    			messJeu("BRAVO");
    		}else {
    			messJeu("PERDU");
    		}
    		aJouer = false;
    	}
    	if(act.getLieu() == Action.blackjack && aJouer && !casino.getBlackJack().isPlay()) {
    		if(casino.getJeu2carte().getResultBJ()==1) {
    			messJeu("On vous a\nexclu");
    		}else {
    			messJeu("plus de jeton");
    		}
    		aJouer = false;
    	}
    	
    	//gerer fin de partie chance
    	if(act.getLieu() == Action.roulette && casino.getRoulette().isPlay()) {
    		if(!casino.getRoulette().isTimeFinished()) {
	    		if(timeBille.hasFinished()) {
	    			timeBille.restart();
	        		posBille--;
	    			if(posBille<0) posBille = 37;
	    		}
    		}else {
	    		casino.getRoulette().setPlay(false);
	    		if(tmpJ>0) {
	    			if(casino.getRoulette().getNumero() == 0.5) {
	    				messJeu("BRAVO\nle numero : 00");
	    			}else {
	    				messJeu("BRAVO\nle numero : "+(int) casino.getRoulette().getNumero());
	    			}
	    		}else {
	    			if(casino.getRoulette().getNumero() == 0.5) {
	    				messJeu("le numero : 00");
	    			}else {
	    				messJeu("le numero : "+(int) casino.getRoulette().getNumero());
	    			}
	    		}
	    		casino.getJcourant().setJeton(tmpJ);
    		}
    	}
    	if(act.getLieu() == Action.machine && casino.getMachineSou().isPlay() && casino.getMachineSou().isTimeFinished()) {
    		casino.getMachineSou().setPlay(false);
    		if(tmpJ>10000) {
    			messJeu("BRAVO\nJACKPOT!");
    			if(bruit.isBruit()) bruit.getSonMachineWin().play(); 
    		}else if(tmpJ>0){
    			messJeu("BRAVO!");
    			if(bruit.isBruit()) bruit.getSonMachineWin().play(); 
    		}else{
    			messJeu("PERDU");
    		}
    		casino.getJcourant().setJeton(tmpJ);
    	}
    	if(act.getLieu() == Action.pari && casino.getPari().isPlay()) {
    		if(!casino.getPari().isTimeFinished()) {
    			if(timeCourse.hasFinished()) {
    				timeCourse.restart();
    				Random r = new Random();
    				for(int i=0; i<casino.getPari().getChevaux().length; i++) {
    					posCheval.replace("P"+i, (posCheval.get("P"+i)+vitesseCheval.get("P"+i)+r.nextInt(15)));
    				}
    			}
    		}else {
	    		casino.getPari().setPlay(false);
	    		if(tmpJ>0) {
	    			messJeu("BRAVO");
	    		}else {
	    			messJeu("PERDU");
	    		}
	    		casino.getJcourant().setJeton(tmpJ);
    		}
    	}
    	
    	//verifier si defaite
    	if(casino.getJcourant().getObjectif() > 0 && casino.getJcourant().getArgent() <= 0 &&
    			casino.getJcourant().getJeton() <= 0) {
    		messJeu("GAME OVER\nplus d'argent");
    		casino.getJcourant().restart();
    	}
    	
    	//verifier si victoire
    	if(casino.getJcourant().getObjectif() > 0 && 
    			casino.getJcourant().getArgent() >= casino.getJcourant().getObjectif()) {
    		switch(casino.getJcourant().getObjectif()) {
    			case 50000: if(!casino.hasTrophee(0)) {
    							messJeu("OBJECTIF ATTEINT\ntroph�e 1 gagn�");
	    						casino.addTrophee(0);
	    						if(casino.hasTrophee(0) && casino.hasTrophee(1) && casino.hasTrophee(2)) {
	    							messJeu("BRAVO\nTous les troph�es\ngagn�s");
	    				    	}
			    			}
			    			break;
    			case 100000: if(!casino.hasTrophee(1)) {
    				messJeu("OBJECTIF ATTEINT\ntroph�e 2 gagn�");
					casino.addTrophee(1);
					if(casino.hasTrophee(0) && casino.hasTrophee(1) && casino.hasTrophee(2)) {
						messJeu("BRAVO\nTous les troph�es\ngagn�s");
			    	}
    			}
    			break;
    			case 1000000: if(!casino.hasTrophee(2)) {
    				messJeu("OBJECTIF ATTEINT\ntroph�e 3 gagn�");
					casino.addTrophee(2);
					if(casino.hasTrophee(0) && casino.hasTrophee(1) && casino.hasTrophee(2)) {
						messJeu("BRAVO\nTous les troph�es\ngagn�s");
			    	}
    			}
    			break;
    		}
    	}
    }

    @Override
        public void render(GameContainer gc, Graphics g) throws SlickException
    {
    	g.setBackground(Color.white);
    	
    	/***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /*************************************CASINO****************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
    	
        if(act.getLieu() == Action.casino) {
        	g.drawImage(fondCasino, 0, 0);
        	int x = input.getMouseX();
        	int y = input.getMouseY();
        	if(x > 800 && x < 1125 && y > 400 && y < 750) {
        		contourPoker.draw(0, 0);
        	}
        	if(x > 125 && x < 475 && y > 650 && y < 1050) {
        		contourBJ.draw(0, 0);
        	}
        	if(x > 1450 && x < 1800 && y > 710 && y < 1000) {
        		contourRoulette.draw(0, 0);
        	}
        	if(x > 1425 && x < 1700 && y > 300 && y < 600) {
        		contourMachine.draw(0, 0);
        	}
        	if(x > 740 && x < 1080 && y > 25 && y < 225) {
        		contourPari.draw(0, 0);
        	}
        	if(x > 150 && x < 775 && y > 100 && y < 600) {
        		contourMenu.draw(0, 0);
        	}
        	
        	//info touche
        	g.drawImage(fondInfo7, 900, 900);
            g.setColor(Color.white);
            g.drawString("p : poker\nb : blackJack\nr : roulette\nm : machine � sous\nc : course de chevaux\n"
            		+ "tab : menu", 910, 910);
        }
        
        /***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /****************************************POKER**************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        if(act.getLieu() == Action.poker) {
        	g.drawImage(fondPoker, 0, 0);
        	if(!casino.getPoker().isPlay()) {
        		g.drawImage(fondInfo5, 1700, 1000);
	            g.setColor(Color.white);
        		g.drawString("espace pour entrer\nco�t d'entr�e :\n" + casino.getPoker().getMise(), 1710, 1010);
        	}
        	if(casino.getPoker().isPlay()) {
        		//barre de temps
        		if(!casino.getPoker().isTimeFinished()) {
        			g.setColor(Color.black);
    	            g.fillRoundRect(10, 1050, 1900, 10, 50);
    	            g.setColor(Color.green);
    	            if(76*(casino.getPoker().getTime()+1) > 0 ) {
    	            	g.fillRoundRect(10, 1050, 76*(casino.getPoker().getTime()+1), 10, 50);
    	            }
        		}else {
        			g.setColor(Color.green);
    	            g.fillRoundRect(10, 1050, 1900, 10, 50);
        		}
        		//carte
        		//afficher jeu sur la table
        		for(int i = 0; i<casino.getPoker().getJeu().size(); i++) {
        			cartes[casino.getPoker().getJeu().get(i).getCouleur()]
        					[casino.getPoker().getJeu().get(i).getNumero()-1].
        					draw(i*60 + 850, 420, (float) 0.5);
        		}
        		
        		//afficher main du joueur sur la table
        		for(int i = 0; i<casino.getPoker().getJoueur().getMainP().getMainPoss().size(); i++) {
        			cartes[casino.getPoker().getJoueur().getMainP().getMainPoss().get(i).getCouleur()]
        				[casino.getPoker().getJoueur().getMainP().getMainPoss().get(i).getNumero()-1].rotate(2);
        			
        			cartes[casino.getPoker().getJoueur().getMainP().getMainPoss().get(i).getCouleur()]
        					[casino.getPoker().getJoueur().getMainP().getMainPoss().get(i).getNumero()-1].
        			draw(i*60 + positions.get("P0").posX,
        					positions.get("P0").posY, (float) 0.5);
        			
        			cartes[casino.getPoker().getJoueur().getMainP().getMainPoss().get(i).getCouleur()]
            				[casino.getPoker().getJoueur().getMainP().getMainPoss().get(i).getNumero()-1].rotate(-2);
        		}
        		
        		int nbJoueur = casino.getPoker().getJoueurs().size();
        		//afficher les cartes cach�es des joueurs
        		for(int i=1; i<nbJoueur; i++) {
        			if(casino.getPoker().getJoueurs().get(i).getMainP().getSize() > 0) {
        				
        				switch(i) {
	        				case 1: carteBack.rotate(91);
	        						g.drawImage(carteBack, positions.get("P1").posX, positions.get("P1").posY);
	        						carteBack.rotate(1);
	        						g.drawImage(carteBack, positions.get("P1").posX, positions.get("P1").posY+60);
	        						carteBack.rotate(-92);
	        				break;
	        				case 2: carteBack.rotate(-178);
	        						g.drawImage(carteBack, positions.get("P2").posX, positions.get("P2").posY);
	        						carteBack.rotate(-2);
	        						g.drawImage(carteBack, positions.get("P2").posX+60, positions.get("P2").posY);
									carteBack.rotate(180);
	        				break;
	        				case 3: carteBack.rotate(-181);
	        						g.drawImage(carteBack, positions.get("P3").posX, positions.get("P3").posY);
	        						g.drawImage(carteBack, positions.get("P3").posX+60, positions.get("P3").posY);
									carteBack.rotate(181);
	        				break;
	        				case 4: carteBack.rotate(-93);
	        						g.drawImage(carteBack, positions.get("P4").posX, positions.get("P4").posY);
	        						carteBack.rotate(2);
	        						g.drawImage(carteBack, positions.get("P4").posX, positions.get("P4").posY+60);
									carteBack.rotate(91);
	        				break;
        				}
        			}
        		}
        		
        		//afficher main du joueur
        		for(int i=0; i<casino.getPoker().getJoueur().getMainP().getMain().size(); i++) {
        			g.drawImage(cartes[casino.getPoker().getJoueur().getMainP().getMain().get(i).getCouleur()]
        					[casino.getPoker().getJoueur().getMainP().getMain().get(i).getNumero()-1], 
	        				i*150+50, 850);
        		}
        		
        		//bouton et paquet
	        	switch(casino.getPoker().getDealer()) {
	        		case 0: g.drawImage(dealer, positions.get("P0").bouton.posX+1, positions.get("P0").bouton.posY-2);
	        				carteBack.rotate(15);
	        				g.drawImage(carteBack, positions.get("P0").paquet.posX+1, positions.get("P0").paquet.posY);
	        				carteBack.rotate(-15);
	        				g.drawImage(pBlind, positions.get("P1").bouton.posX-1, positions.get("P1").bouton.posY-1);
	        				g.drawImage(gBlind, positions.get("P2").bouton.posX, positions.get("P2").bouton.posY-2);
	        		break;
	        		case 1: g.drawImage(dealer, positions.get("P1").bouton.posX+2, positions.get("P1").bouton.posY+1);
	        				carteBack.rotate(55);
	        				g.drawImage(carteBack, positions.get("P1").paquet.posX, positions.get("P1").paquet.posY-1);
	        				carteBack.rotate(-55);
	        				g.drawImage(pBlind, positions.get("P2").bouton.posX-1, positions.get("P2").bouton.posY-2);
	        				g.drawImage(gBlind, positions.get("P"+(3%nbJoueur)).bouton.posX+1, positions.get("P"+(3%nbJoueur)).bouton.posY);
	        		break;
	        		case 2: g.drawImage(dealer, positions.get("P2").bouton.posX+2, positions.get("P2").bouton.posY+1);
		    				carteBack.rotate(160);
		    				g.drawImage(carteBack, positions.get("P2").paquet.posX+2, positions.get("P2").paquet.posY);
		    				carteBack.rotate(-160);
		    				g.drawImage(pBlind, positions.get("P"+(3%nbJoueur)).bouton.posX-1, positions.get("P"+(3%nbJoueur)).bouton.posY+1);
		    				g.drawImage(gBlind, positions.get("P"+(4%nbJoueur)).bouton.posX, positions.get("P"+(4%nbJoueur)).bouton.posY+2);
		    		break;
	        		case 3: g.drawImage(dealer, positions.get("P3").bouton.posX, positions.get("P3").bouton.posY+3);
		    				carteBack.rotate(220);
		    				g.drawImage(carteBack, positions.get("P3").paquet.posX-1, positions.get("P3").paquet.posY-1);
		    				carteBack.rotate(-220);
		    				g.drawImage(pBlind, positions.get("P"+(4%nbJoueur)).bouton.posX-2, positions.get("P"+(4%nbJoueur)).bouton.posY);
		    				g.drawImage(gBlind, positions.get("P"+(5%nbJoueur)).bouton.posX, positions.get("P"+(5%nbJoueur)).bouton.posY+2);
		    		break;
	        		case 4: g.drawImage(dealer, positions.get("P4").bouton.posX+1, positions.get("P4").bouton.posY-1);
		    				carteBack.rotate(55);
		    				g.drawImage(carteBack, positions.get("P4").paquet.posX-1, positions.get("P4").paquet.posY+1);
		    				carteBack.rotate(-55);
		    				g.drawImage(pBlind, positions.get("P0").bouton.posX-2, positions.get("P0").bouton.posY);
		    				g.drawImage(gBlind, positions.get("P1").bouton.posX, positions.get("P1").bouton.posY+2);
		    		break;
	        	}
	        	
	        	//jetons du joueur
	        	int[] t = Poker.getRepartJeton(casino.getPoker().getJetonJoueurCourant());
	        	for(int i=0; i<t.length; i++) {
	        		if(t[i] > 0) {
		        		switch(i) {
		        			case 0: g.drawImage(jeton1, positions.get("P0").jetons.posX, positions.get("P0").jetons.posY);
		        			break;
		        			case 1: g.drawImage(jeton5, positions.get("P0").jetons.posX+26, positions.get("P0").jetons.posY);
		        			break;
		        			case 2: g.drawImage(jeton10, positions.get("P0").jetons.posX+52, positions.get("P0").jetons.posY);
		        			break;
		        			case 3: g.drawImage(jeton25, positions.get("P0").jetons.posX+77, positions.get("P0").jetons.posY);
		        			break;
		        			case 4: g.drawImage(jeton100, positions.get("P0").jetons.posX, positions.get("P0").jetons.posY-26);
		        			break;
		        			case 5: g.drawImage(jeton500, positions.get("P0").jetons.posX+26, positions.get("P0").jetons.posY-26);
		        			break;
		        			case 6: g.drawImage(jeton1000, positions.get("P0").jetons.posX+52, positions.get("P0").jetons.posY-26);
		        			break;
		        			case 7: g.drawImage(jeton5000, positions.get("P0").jetons.posX+77, positions.get("P0").jetons.posY-26);
		        			break;
		        		}
	        		}
	        	}
	        	
	        	//jetons des autres joueurs
	        	for(int i=1; i<nbJoueur; i++) {
	        		int[] t2 = Poker.getRepartJeton(casino.getPoker().getJoueurs().get(i).getJeton());
		        	for(int j=0; j<t2.length; j++) {
		        		if(t2[j] > 0) {
		        			int tmp = i;
		        			if(i==3 || i==4) {
		        				tmp = i-1;
		        			}
		        			g.rotate(positions.get("P"+i).jetons.posX, positions.get("P"+i).jetons.posY, tmp*90);
			        		
			        		switch(j) {
			        			case 0: g.drawImage(jeton1, positions.get("P"+i).jetons.posX, positions.get("P"+i).jetons.posY);
			        			break;
			        			case 1: g.drawImage(jeton5, positions.get("P"+i).jetons.posX+26, positions.get("P"+i).jetons.posY);
			        			break;
			        			case 2: g.drawImage(jeton10, positions.get("P"+i).jetons.posX+52, positions.get("P"+i).jetons.posY);
			        			break;
			        			case 3: g.drawImage(jeton25, positions.get("P"+i).jetons.posX+77, positions.get("P"+i).jetons.posY);
			        			break;
			        			case 4: g.drawImage(jeton100, positions.get("P"+i).jetons.posX, positions.get("P"+i).jetons.posY-26);
			        			break;
			        			case 5: g.drawImage(jeton500, positions.get("P"+i).jetons.posX+26, positions.get("P"+i).jetons.posY-26);
			        			break;
			        			case 6: g.drawImage(jeton1000, positions.get("P"+i).jetons.posX+52, positions.get("P"+i).jetons.posY-26);
			        			break;
			        			case 7: g.drawImage(jeton5000, positions.get("P"+i).jetons.posX+77, positions.get("P"+i).jetons.posY-26);
			        			break;
			        		}
			        		
			        		g.rotate(positions.get("P"+i).jetons.posX, positions.get("P"+i).jetons.posY, -tmp*90);
		        		}
		        	}
	        	}
	        	
	        	//jetons du pot
	        	t = Poker.getRepartJeton(casino.getPoker().getMiseTotal());
	        	for(int i=0; i<t.length; i++) {
	        		if(t[i] > 0) {
		        		switch(i) {
		        			case 0: g.drawImage(jeton1, 935, 545);
		        			break;
		        			case 1: g.drawImage(jeton5, 961, 545);
		        			break;
		        			case 2: g.drawImage(jeton10, 987, 545);
		        			break;
		        			case 3: g.drawImage(jeton25, 1013, 545);
		        			break;
		        			case 4: g.drawImage(jeton100, 935, 519);
		        			break;
		        			case 5: g.drawImage(jeton500, 961, 519);
		        			break;
		        			case 6: g.drawImage(jeton1000, 987, 519);
		        			break;
		        			case 7: g.drawImage(jeton5000, 1013, 519);
		        			break;
		        		}
	        		}
	        	}
	        	
	        	//info mise
	            g.drawImage(fondInfo, 900, 920);
	            g.setColor(Color.white);
	            g.drawString("mise courante :\n"+casino.getPoker().getMiseCourante()+
	            		"\nvaleur du pot :\n"+casino.getPoker().getMiseTotal(), 910, 930);
	            
	            //info jetons restant
	            g.drawImage(fondInfo2, 900, 820);
	            g.setColor(Color.white);
	            g.drawString("jetons restant :\n"+casino.getPoker().getJetonJoueurCourant(), 910, 830);
	            
	        	//info adversaire
	            for(int i=1; i<nbJoueur; i++) {
	            	g.drawImage(fondInfo2, positions.get("P"+i).info.posX, positions.get("P"+i).info.posY);
		            g.setColor(Color.white);
		            g.drawString(casino.getPoker().getJoueurs().get(i).getNom()+
		            		"\njetons restant :\n"+casino.getPoker().getJoueurs().get(i).getJeton(),
		            		positions.get("P"+i).info.posX+10, positions.get("P"+i).info.posY+10);
	            }
	            
	            //info relance
	            if(casino.getPoker().isRelance()) {
	            	g.drawImage(fondInfo3, 1700, 840);
		            g.setColor(Color.white);
		            g.drawString("relance de :\n" + casino.getPoker().getNbRelance(), 1710, 850);
	            }
	            
	            //info touche
	            g.drawImage(fondInfo4, 1700, 900);
	            g.setColor(Color.white);
	            g.drawString("s : suivre\nc : se coucher\nt : tapis\nr : relancer\np : miser", 1710, 910);
        	}
        }
        
        /***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /**************************************BLACKJACK************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        
        if(act.getLieu() == Action.blackjack) {
        	g.drawImage(fondBJ, 0, 0);
        	if(!casino.getBlackJack().isPlay()) {
        		g.drawImage(fondInfo5, 1700, 1000);
	            g.setColor(Color.white);
        		g.drawString("espace pour jouer\nmise : "+casino.getBlackJack().getMise(), 1710, 1010);
        	}
        	if(casino.getBlackJack().isPlay()) {
        		//barre de temps
        		if(!casino.getBlackJack().isTimeFinished()) {
        			g.setColor(Color.black);
    	            g.fillRoundRect(10, 1050, 1900, 10, 50);
    	            g.setColor(Color.green);
    	            if(76*(casino.getBlackJack().getTime()+1) > 0 ) {
    	            	g.fillRoundRect(10, 1050, 76*(casino.getBlackJack().getTime()+1), 10, 50);
    	            }
        		}else {
        			g.setColor(Color.green);
    	            g.fillRoundRect(10, 1050, 1900, 10, 50);
        		}
        		
        		//afficher le paquet
        		carteBack.rotate(-30);
        		carteBack.draw(positions.get("B5").posX + 170, positions.get("B5").posY, (float) 1.5);
        		carteBack.rotate(30);
        		
        		int nbJoueur = casino.getBlackJack().getJoueurs().size();
        		//afficher les cartes sur la table
        		for(int i=0; i<nbJoueur; i++) {
        			MainBJ m = casino.getBlackJack().getJoueurs().get(i).getMainB();
        			for(int j=0; j<m.getSize(); j++) {
        				if(i==nbJoueur-1) {
        					if(j==0 && !casino.getBlackJack().isCroupier()) {
        						carteBack.draw(positions.get("B"+i).posX, positions.get("B"+i).posY, (float) 1.5);
        					}else {
        						cartes[m.getMain().get(j).getCouleur()][m.getMain().get(j).getNumero()-1]
                						.draw(positions.get("B"+i).posX, positions.get("B"+i).posY + j*80, (float) 0.75);
        					}
        				}else {
        					cartes[m.getMain().get(j).getCouleur()][m.getMain().get(j).getNumero()-1]
            						.draw(positions.get("B"+i).posX, positions.get("B"+i).posY - j*80, (float) 0.75);
        				}
        			}
        			if(i!=nbJoueur-1) {
	        			MainBJ m2 = null;
	        			if(m.getSplit() != null) {
	        				m2 = m.getSplit();
	        				for(int j=0; j<m2.getSize(); j++) {
	        					cartes[m2.getMain().get(j).getCouleur()][m2.getMain().get(j).getNumero()-1]
	            						.draw(positions.get("B"+i).posX+100, positions.get("B"+i).posY - j*80, (float) 0.75);
	        				}
	        			}
        			}
        		}
        		
        		//afficher main du joueur
        		for(int i=0; i<casino.getBlackJack().getJoueur().getMainB().getMain().size(); i++) {
        			g.drawImage(cartes[casino.getBlackJack().getJoueur().getMainB().getMain().get(i).getCouleur()]
        					[casino.getBlackJack().getJoueur().getMainB().getMain().get(i).getNumero()-1], 
	        				20, 880-i*130);
        		}
        		if(casino.getBlackJack().getJoueur().getMainB().getSplit() != null) {
        			for(int i=0; i<casino.getBlackJack().getJoueur().getMainB().getSplit().getMain().size(); i++) {
            			g.drawImage(cartes
            					[casino.getBlackJack().getJoueur().getMainB().getSplit().getMain().get(i).getCouleur()]
            					[casino.getBlackJack().getJoueur().getMainB().getSplit().getMain().get(i).getNumero()-1], 
    	        				150, 880-i*130);
            		}
        		}
        		
        		//jetons du joueur
	        	int[] t = BlackJack.getRepartJeton(casino.getBlackJack().getJetonJoueurCourant());
	        	int[] t4 = BlackJack.getRepartJeton(casino.getBlackJack().getMise());
	        	for(int i=0; i<t4.length; i++) {
	        		if(t4[i] > 0) {
		        		switch(i) {
		        			case 0: g.drawImage(jeton1, positions.get("B0").jetons.posX-10, positions.get("B0").jetons.posY-200);
		        			break;
		        			case 1: g.drawImage(jeton5, positions.get("B0").jetons.posX+16, positions.get("B0").jetons.posY-200);
		        			break;
		        			case 2: g.drawImage(jeton10, positions.get("B0").jetons.posX+42, positions.get("B0").jetons.posY-200);
		        			break;
		        			case 3: g.drawImage(jeton25, positions.get("B0").jetons.posX+67, positions.get("B0").jetons.posY-200);
		        			break;
		        			case 4: g.drawImage(jeton100, positions.get("B0").jetons.posX-10, positions.get("B0").jetons.posY-226);
		        			break;
		        			case 5: g.drawImage(jeton500, positions.get("B0").jetons.posX+16, positions.get("B0").jetons.posY-226);
		        			break;
		        			case 6: g.drawImage(jeton1000, positions.get("B0").jetons.posX+42, positions.get("B0").jetons.posY-226);
		        			break;
		        			case 7: g.drawImage(jeton5000, positions.get("B0").jetons.posX+67, positions.get("B0").jetons.posY-226);
		        			break;
		        		}
	        		}
	        	}
	        	for(int i=0; i<t.length; i++) {
	        		if(t[i] > 0) {
		        		switch(i) {
		        			case 0: g.drawImage(jeton1, positions.get("B0").jetons.posX, positions.get("B0").jetons.posY-20);
		        			break;
		        			case 1: g.drawImage(jeton5, positions.get("B0").jetons.posX+26, positions.get("B0").jetons.posY-20);
		        			break;
		        			case 2: g.drawImage(jeton10, positions.get("B0").jetons.posX+52, positions.get("B0").jetons.posY-20);
		        			break;
		        			case 3: g.drawImage(jeton25, positions.get("B0").jetons.posX+77, positions.get("B0").jetons.posY-20);
		        			break;
		        			case 4: g.drawImage(jeton100, positions.get("B0").jetons.posX, positions.get("B0").jetons.posY-46);
		        			break;
		        			case 5: g.drawImage(jeton500, positions.get("B0").jetons.posX+26, positions.get("B0").jetons.posY-46);
		        			break;
		        			case 6: g.drawImage(jeton1000, positions.get("B0").jetons.posX+52, positions.get("B0").jetons.posY-46);
		        			break;
		        			case 7: g.drawImage(jeton5000, positions.get("B0").jetons.posX+77, positions.get("B0").jetons.posY-46);
		        			break;
		        		}
	        		}
	        	}
        		
        		//jetons des autres joueurs
	        	for(int i=1; i<nbJoueur-1; i++) {
	        		int[] t2 = BlackJack.getRepartJeton(casino.getBlackJack().getJoueurs().get(i).getJeton());
	        		if(casino.getBlackJack().getJoueurs().get(i) instanceof IA) {
	        			int[] t3 = BlackJack.getRepartJeton(((IA) casino.getBlackJack().getJoueurs().get(i)).getMiseBJ());
	        			for(int j=0; j<t3.length; j++) {
			        		if(t3[j] > 0) {
				        		switch(j) {
				        			case 0: g.drawImage(jeton1, positions.get("B"+i).jetons.posX, positions.get("B"+i).jetons.posY-200);
				        			break;
				        			case 1: g.drawImage(jeton5, positions.get("B"+i).jetons.posX+26, positions.get("B"+i).jetons.posY-200);
				        			break;
				        			case 2: g.drawImage(jeton10, positions.get("B"+i).jetons.posX+52, positions.get("B"+i).jetons.posY-200);
				        			break;
				        			case 3: g.drawImage(jeton25, positions.get("B"+i).jetons.posX+77, positions.get("B"+i).jetons.posY-200);
				        			break;
				        			case 4: g.drawImage(jeton100, positions.get("B"+i).jetons.posX, positions.get("B"+i).jetons.posY-226);
				        			break;
				        			case 5: g.drawImage(jeton500, positions.get("B"+i).jetons.posX+26, positions.get("B"+i).jetons.posY-226);
				        			break;
				        			case 6: g.drawImage(jeton1000, positions.get("B"+i).jetons.posX+52, positions.get("B"+i).jetons.posY-226);
				        			break;
				        			case 7: g.drawImage(jeton5000, positions.get("B"+i).jetons.posX+77, positions.get("B"+i).jetons.posY-226);
				        			break;
				        		}
			        		}
			        	}
	        		}
		        	for(int j=0; j<t2.length; j++) {
		        		if(t2[j] > 0) {
			        		switch(j) {
			        			case 0: g.drawImage(jeton1, positions.get("B"+i).jetons.posX, positions.get("B"+i).jetons.posY);
			        			break;
			        			case 1: g.drawImage(jeton5, positions.get("B"+i).jetons.posX+26, positions.get("B"+i).jetons.posY);
			        			break;
			        			case 2: g.drawImage(jeton10, positions.get("B"+i).jetons.posX+52, positions.get("B"+i).jetons.posY);
			        			break;
			        			case 3: g.drawImage(jeton25, positions.get("B"+i).jetons.posX+77, positions.get("B"+i).jetons.posY);
			        			break;
			        			case 4: g.drawImage(jeton100, positions.get("B"+i).jetons.posX, positions.get("B"+i).jetons.posY-26);
			        			break;
			        			case 5: g.drawImage(jeton500, positions.get("B"+i).jetons.posX+26, positions.get("B"+i).jetons.posY-26);
			        			break;
			        			case 6: g.drawImage(jeton1000, positions.get("B"+i).jetons.posX+52, positions.get("B"+i).jetons.posY-26);
			        			break;
			        			case 7: g.drawImage(jeton5000, positions.get("B"+i).jetons.posX+77, positions.get("B"+i).jetons.posY-26);
			        			break;
			        		}
		        		}
		        	}
	        	}
        		
        		//info jetons restant
	        	g.drawImage(fondInfo2, 900, 960);
	            g.setColor(Color.white);
	            g.drawString("jetons restant :\n"+casino.getBlackJack().getJetonJoueurCourant(), 910, 980);
	            
	          //info autres joueurs
	           for(int i=1; i<nbJoueur-1; i++) {
	        	    g.drawImage(fondInfo2, positions.get("B"+i).info.posX, positions.get("B"+i).info.posY);
		            g.setColor(Color.white);
		            g.drawString(casino.getBlackJack().getJoueurs().get(i).getNom()+
		            		"\njetons restant :\n"+casino.getBlackJack().getJoueurs().get(i).getJeton(),
		            		positions.get("B"+i).info.posX+10, positions.get("B"+i).info.posY+10);
	            }
        		
        		//info touche
        		if(!casino.getBlackJack().isMiseAct()) {
        			g.drawImage(fondInfo4, 1700, 900);
	                g.setColor(Color.white);
	                g.drawString("mise : "+ casino.getBlackJack().getMise() +
	                		"\nr : rester\nc : tirer\ns : split\nd : double", 1710, 910);
        		}else {
        			g.drawImage(fondInfo4, 1700, 900);
	                g.setColor(Color.white);
	                g.drawString("mise : "+ casino.getBlackJack().getMise() +
	                		"\nup : augmenter\ndown : diminuer\nentree : accepter", 1710, 910);
        		}
        	}
        }
        
        /***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /**************************************ROULETTE*************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        
        if(act.getLieu() == Action.roulette) {
        	g.drawImage(fondRoulette, 0, 80);
        	if(casino.getRoulette().isTimeFinished()) {
        		animR.setCurrentFrame(0);
        		animR.restart();
        		bruit.getSonRoulette().stop();
        		g.drawImage(bille, billes.get(posBille).posX, billes.get(posBille).posY);
        			
	        	if(!casino.getRoulette().isPretPari()) {
	        		g.drawImage(fondMess, 1700, 970);
		            g.setColor(Color.white);
	        		g.drawString("entrer pour miser\nnombre de jeton :\n" + casino.getRoulette().getTypePari() +
		            "\nmise : " + casino.getRoulette().getMise(), 1710, 980);
	        	}
	        	if(casino.getRoulette().isPretPari()) {
	        		Roulette roulette = casino.getRoulette();
	        		switch(roulette.getTypePari()) {
	        			case 1: if(roulette.getPari()[0] == 0.5) {
	        						jeton1.draw(positions.get("R100").posX,
		      					    positions.get("R100").posY, 2);
	        					}else {
	        						jeton1.draw(positions.get("R1"+(int)roulette.getPari()[0]).posX,
	        	      				positions.get("R1"+(int)roulette.getPari()[0]).posY, 2);
	        					}
	        			break;
	        			case 2: if(roulette.getPari()[0] == 0.5 && roulette.getPari()[1] == 2.0) {
	        						jeton1.draw(positions.get("R2002").posX,
		      					    positions.get("R2002").posY, 2);
	        					}else {
		        					jeton1.draw(positions.get("R2"+(int)roulette.getPari()[0]+(int)roulette.getPari()[1]).posX,
		      					    positions.get("R2"+(int)roulette.getPari()[0]+(int)roulette.getPari()[1]).posY, 2);
	        					}
	        			break;
	        			case 3: jeton1.draw(positions.get("R3"+(int)roulette.getPari()[0]+(int)roulette.getPari()[1]).posX,
	      					    positions.get("R3"+(int)roulette.getPari()[0]+(int)roulette.getPari()[1]).posY, 2);
	        			break;
	        			case 4: jeton1.draw(positions.get("R4"+(int)roulette.getPari()[0]).posX,
	      					    positions.get("R4"+(int)roulette.getPari()[0]).posY, 2);
	        			break;
	        			case 5: jeton1.draw(positions.get("R5").posX, positions.get("R5").posY, 2);
	        			break;
	        			case 6: jeton1.draw(positions.get("R6"+(int)roulette.getPari()[0]).posX,
	      					    positions.get("R6"+(int)roulette.getPari()[0]).posY, 2);
	        			break;
	        			case 12: jeton1.draw(positions.get("R7"+(int)roulette.getPari()[0]+(int)roulette.getPari()[1]).posX,
	       					     positions.get("R7"+(int)roulette.getPari()[0]+(int)roulette.getPari()[1]).posY, 2);
	        			break;
	        			case 24: jeton1.draw(positions.get("R8"+roulette.getChoixExt()).posX,
	        					 positions.get("R8"+roulette.getChoixExt()).posY, 2);
	        			break;
	        		}
	        		
	        		g.drawImage(fondInfo4, 1700, 950);
		            g.setColor(Color.white);
	        		g.drawString("espace pour jouer\nnumero :\n" + casino.getRoulette().getStringPari() +
		            "\nmise : " + casino.getRoulette().getMise(), 1710, 960);
	        	}
        	}else{
        		animR.draw(103, 327);
        		animR.setSpeed(casino.getRoulette().getTime());
        		g.drawImage(bille, billes.get(posBille).posX, billes.get(posBille).posY);
        		if(bruit.isBruit() && !bruit.getSonRoulette().playing()) bruit.getSonRoulette().play();
        	}
        }
        
        /***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /**************************************MACHINE**************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        
        if(act.getLieu() == Action.machine) {
        	g.drawImage(fondMachine, 0, 0);
        	if(casino.getMachineSou().isTimeFinished()) {
        		bruit.getSonMachine().stop(); 
        		for(int i=0; i<3; i++) {
        			for(int j=0; j<3; j++) {
        				g.drawImage(symbole.get(casino.getMachineSou().getImage()[i][j]), 580+i*290, 80+j*305);
        			}
        		}
        		g.setColor(Color.red);
        		switch(casino.getMachineSou().getChoix()) {
        			case "2 diagonale" : g.rotate(525, 185, 46);
        								 g.fillRoundRect(425, 75, 1265, 10, 100);
        								 g.rotate(525, 185, -46);
        			case "1 diagonale" : g.rotate(525, 795, -46); 
        								 g.fillRoundRect(420, 905, 1260, 10, 100);
        								 g.rotate(525, 795, 46);
        			case "triple" : g.fillRoundRect(525, 795, 895, 10, 100);
        			case "double" : g.fillRoundRect(525, 185, 895, 10, 100);
        			case "simple" : g.fillRoundRect(525, 495, 895, 10, 100);
        		}
        		
        		g.drawImage(fondInfo4, 1700, 950);
	            g.setColor(Color.white);
        		g.drawString("espace pour jouer\nchoix :\n" + casino.getMachineSou().getChoix() +
	            "\nmise : " + casino.getMachineSou().getMise(), 1710, 960);
        	}else {
        		animM1.draw(580, 80);
        		animM2.draw(580+1*290, 80);
        		animM3.draw(580+2*290, 80);
        		animM4.draw(580, 80+1*305);
        		animM5.draw(580+1*290, 80+1*305);
        		animM6.draw(580+2*290, 80+1*305);
        		animM7.draw(580, 80+2*305);
        		animM8.draw(580+1*290, 80+2*305);
        		animM9.draw(580+2*290, 80+2*305);
        		if(bruit.isBruit() && !bruit.getSonMachine().playing()) bruit.getSonMachine().play(); 
        	}
        }
        
        /***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /*****************************************PARI**************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        
        if(act.getLieu() == Action.pari) {
        	g.drawImage(fondPari, 0, 0);
        	bruit.getSonPari().stop();
        	if(casino.getPari().isTimeFinished()) {
        		
        		//afficher les chevaux
        		for(int i=0; i<casino.getPari().getChevaux().length; i++) {
        			chevaux.get("P"+casino.getPari().getChevaux()[i]).restart();
        			chevauxFixe.get("P"+casino.getPari().getChevaux()[i]).draw(80, 380+i*40);
        		}
        		
        		if(!casino.getPari().isPretPari()) {
        			g.drawImage(fondMess, 1700, 970);
		            g.setColor(Color.white);
	        		g.drawString("entrer pour miser\nnombre de chevaux :\n" + (casino.getPari().getTypePari()+1) +
		            "\nmise : " + casino.getPari().getMise(), 1710, 980);
	        	}
	        	if(casino.getPari().isPretPari()) {
	        		//afficher la selection
	        		g.setColor(Color.red);
	        		switch(cheval) {
	        			case 0 : g.fillRoundRect(80, 380+75, 100, 10, 100); break;
	        			case 1 : g.fillRoundRect(80, 380+115, 100, 10, 100); break;
	        			case 2 : g.fillRoundRect(80, 380+155, 100, 10, 100); break;
	        			case 3 : g.fillRoundRect(80, 380+195, 100, 10, 100); break;
	        			case 4 : g.fillRoundRect(80, 380+235, 100, 10, 100); break;
	        			case 5 : g.fillRoundRect(80, 380+275, 100, 10, 100); break;
	        			case 6 : g.fillRoundRect(80, 380+315, 100, 10, 100); break;
	        			case 7 : g.fillRoundRect(80, 380+355, 100, 10, 100); break;
	        			case 8 : g.fillRoundRect(80, 380+395, 100, 10, 100); break;
	        			case 9 : g.fillRoundRect(80, 380+435, 100, 10, 100); break;
	        		}
	        		
	        		g.drawImage(fondInfo4, 1700, 950);
		            g.setColor(Color.white);
	        		g.drawString("espace pour jouer\nentrer pour parier\nchoix : " + casino.getPari().getPari() +
	        		"\nc�te : "	+ casino.getPari().getCote(cheval) +	
		            "\nmise : " + casino.getPari().getMise(), 1710, 960);
	        	}
        	}else {
        		//afficher resultat
        		g.drawImage(fondInfo6, 900, 200);
	            g.setColor(Color.white);
        		g.drawString("resultat :", 910, 210);
        		for(int i=0; i<casino.getPari().getResultat().length; i++) {
        			if(80 + posCheval.get("P"+casino.getPari().getResultat()[i]) >= 1800) {
        				g.drawString(" " + casino.getPari().getResultat()[i], 1000+i*15, 210);
        			}
        		}
        		
        		//afficher chevaux
        		for(int i=0; i<casino.getPari().getChevaux().length; i++) {
        			if(80 + posCheval.get("P"+casino.getPari().getChevaux()[i]) >= 1800) {
        				chevauxFixe.get("P"+casino.getPari().getChevaux()[i]).draw(1800, 380+i*40);
        			}else {
	        			chevaux.get("P"+casino.getPari().getChevaux()[i]).
	        			draw(80 + posCheval.get("P"+casino.getPari().getChevaux()[i]), 380+i*40);
        			}
        		}
        		if(bruit.isBruit() && !bruit.getSonPari().playing()) bruit.getSonPari().loop(); 
        	}
        }
        
        /***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /****************************************MENU***************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        
        if(act.getLieu() == Action.menu) {
        	g.drawImage(fondMenu, 0, 0);
        	if(casino.getMenu().getLoc() == Menu.principal) {
	        	g.drawImage(fondTexte, 550, 50);
	            g.drawImage(fondTexte, 550, 200);
	            g.drawImage(fondTexte, 550, 350);
	            g.drawImage(fondTexte, 550, 500);
	            g.drawImage(fondTexte, 550, 650);
	            g.drawImage(fondTexte, 550, 800);
	            g.drawImage(fondTexte, 550, 950);
	            
	            
	            if(casino.getMenu().getSelect() == 1) g.drawImage(fondTexteSelect, 550, 200);
	            if(casino.getMenu().getSelect() == 2) g.drawImage(fondTexteSelect, 550, 350);
	            if(casino.getMenu().getSelect() == 3) g.drawImage(fondTexteSelect, 550, 500);
	            if(casino.getMenu().getSelect() == 4) g.drawImage(fondTexteSelect, 550, 650);
	            if(casino.getMenu().getSelect() == 5) g.drawImage(fondTexteSelect, 550, 800);
	            if(casino.getMenu().getSelect() == 6) g.drawImage(fondTexteSelect, 550, 950);
	            
	            g.setColor(Color.white);
                g.drawString("MENU", 900, 100);
                g.drawString("JOUEUR", 900, 250);
                g.drawString("CONVERTIR", 900, 400);
                g.drawString("OBJET", 900, 550);
                g.drawString("OPTION", 900, 700);
                g.drawString("TUTO", 900, 850);
                g.drawString("CREDIT", 900, 1000);
        	}
        	if(casino.getMenu().getLoc() == Menu.joueur) {
	            g.drawImage(fondTexte2, 550, 50);
	            g.drawImage(fondTexte2, 550, 250);
	            g.drawImage(fondTexte2, 550, 450);
	            g.drawImage(fondTexte2, 550, 650);
	            g.drawImage(fondTexte2, 550, 850);
	            
	            if(casino.getMenu().getSelect() == 1) g.drawImage(fondTexteSelect2, 550, 250);
	            if(casino.getMenu().getSelect() == 2) g.drawImage(fondTexteSelect2, 550, 450);
	            if(casino.getMenu().getSelect() == 3) g.drawImage(fondTexteSelect2, 550, 650);
	            if(casino.getMenu().getSelect() == 4) g.drawImage(fondTexteSelect2, 550, 850);
	            
	            g.setColor(Color.white);
                g.drawString("JOUEUR", 900, 125);
                g.drawString("FACILE", 900, 325);
                g.drawString("MOYEN", 900, 525);
                g.drawString("DIFFICILE", 900, 725);
                g.drawString("LIBRE", 900, 925);
        	}
        	if(casino.getMenu().getLoc() == Menu.convertir) {
        		g.drawImage(fondTexte2, 550, 50);
	            g.drawImage(fondTexte2, 550, 250);
	            g.drawImage(fondTexte2, 550, 450);
	            g.drawImage(fondTexte2, 550, 650);
	            g.drawImage(fondTexte2, 550, 850);
	            
	            if(casino.getMenu().getSelect() == 1) g.drawImage(fondTexteSelect2, 550, 250);
	            if(casino.getMenu().getSelect() == 2) g.drawImage(fondTexteSelect2, 550, 450);
	            if(casino.getMenu().getSelect() == 3) g.drawImage(fondTexteSelect2, 550, 650);
	            if(casino.getMenu().getSelect() == 4) g.drawImage(fondTexteSelect2, 550, 850);
	            
	            g.setColor(Color.white);
                g.drawString("CONVERTIR", 900, 125);
                g.drawString("VENDRE 10 JETONS", 900, 325);
                g.drawString("VENDRE 1000 JETONS", 900, 525);
                g.drawString("ACHETER 100 JETONS", 900, 725);
                g.drawString("ACHETER 10000 JETONS", 900, 925);
        	}
        	if(casino.getMenu().getLoc() == Menu.objet) {
        		g.drawImage(fondTexte2, 550, 50);
	            g.drawImage(fondTexte2, 550, 250);
	            g.drawImage(fondTexte2, 550, 450);
	            g.drawImage(fondTexte2, 550, 650);
	            g.drawImage(fondTexte2, 550, 850);
	            
	            if(casino.getMenu().getSelect() == 1) g.drawImage(fondTexteSelect2, 550, 250);
	            if(casino.getMenu().getSelect() == 2) g.drawImage(fondTexteSelect2, 550, 450);
	            if(casino.getMenu().getSelect() == 3) g.drawImage(fondTexteSelect2, 550, 650);
	            if(casino.getMenu().getSelect() == 4) g.drawImage(fondTexteSelect2, 550, 850);
	            
	            g.setColor(Color.white);
                g.drawString("OBJET", 900, 125);
                g.drawString("BOISSON", 900, 325);
                g.drawString("PATTE DE LAPIN", 900, 525);
                g.drawString("ABONNEMENT", 900, 725);
                g.drawString("GARDE DU CORPS", 900, 925);
        	}
        	if(casino.getMenu().getLoc() == Menu.option) {
        		g.drawImage(fondTexte2, 550, 50);
	            g.drawImage(fondTexte2, 550, 250);
	            g.drawImage(fondTexte2, 550, 450);
	            g.drawImage(fondTexte2, 550, 650);
	            
	            if(casino.getMenu().getSelect() == 1) g.drawImage(fondTexteSelect2, 550, 250);
	            if(casino.getMenu().getSelect() == 2) g.drawImage(fondTexteSelect2, 550, 450);
	            if(casino.getMenu().getSelect() == 3) g.drawImage(fondTexteSelect2, 550, 650);
	            
	            g.setColor(Color.white);
                g.drawString("OPTION", 900, 125);
                if(son) {
                	g.drawString("DESACTIVER LES MUSIQUES", 900, 325);
                }else {
                	g.drawString("ACTIVER LES MUSIQUES", 900, 325);
                }
                if(bruit.isBruit()) {
                	g.drawString("DESACTIVER LE SON", 900, 525);
                }else {
                	g.drawString("ACTIVER LE SON", 900, 525);
                }
                if(fulls) {
                	g.drawString("FENETRE", 900, 725);
                }else {
                	g.drawString("PLEIN ECRAN", 900, 725);
                }
        	}
        	if(casino.getMenu().getLoc() == Menu.tuto && casino.getMenu().getInTuto() == -1) {
        		g.drawImage(fondTexte, 550, 50);
	            g.drawImage(fondTexte, 550, 200);
	            g.drawImage(fondTexte, 550, 350);
	            g.drawImage(fondTexte, 550, 500);
	            g.drawImage(fondTexte, 550, 650);
	            g.drawImage(fondTexte, 550, 800);
	            
	            if(casino.getMenu().getSelect() == 1) g.drawImage(fondTexteSelect, 550, 200);
	            if(casino.getMenu().getSelect() == 2) g.drawImage(fondTexteSelect, 550, 350);
	            if(casino.getMenu().getSelect() == 3) g.drawImage(fondTexteSelect, 550, 500);
	            if(casino.getMenu().getSelect() == 4) g.drawImage(fondTexteSelect, 550, 650);
	            if(casino.getMenu().getSelect() == 5) g.drawImage(fondTexteSelect, 550, 800);
	            	
	            g.setColor(Color.white);
	            g.drawString("TUTO", 900, 100);
	            g.drawString("POKER", 900, 250);
                g.drawString("BLACK JACK", 900, 400);
                g.drawString("MACHINE A SOUS", 900, 550);
                g.drawString("ROULETTE", 900, 700);
                g.drawString("PARI", 900, 850);
        	}
        	if(casino.getMenu().getLoc() == Menu.tuto && casino.getMenu().getInTuto() == Menu.tutoPoker) {
        		g.drawImage(tutoPoker, 0, 0);
        	}
			if(casino.getMenu().getLoc() == Menu.tuto && casino.getMenu().getInTuto() == Menu.tutoBJ) {
				g.drawImage(tutoBJ, 0, 0);
			}
			if(casino.getMenu().getLoc() == Menu.tuto && casino.getMenu().getInTuto() == Menu.tutoMachine) {
				g.drawImage(tutoMachine, 0, 0);
			}
			if(casino.getMenu().getLoc() == Menu.tuto && casino.getMenu().getInTuto() == Menu.tutoRoulette) {
				g.drawImage(tutoRoulette, 0, 0);
			}
			if(casino.getMenu().getLoc() == Menu.tuto && casino.getMenu().getInTuto() == Menu.tutoPari) {
				g.drawImage(tutoPari, 0, 0);
			}
        	if(casino.getMenu().getLoc() == Menu.credit) {
        		g.drawImage(credit, 0, 0);
        	}
        }
        
        //etat du perso
        g.setColor(Color.black);
        g.drawImage(fondPerso, 0, 0);
        g.setColor(Color.white);
        g.drawImage(icon, 20, 20);
        g.drawString(casino.getJcourant().getNom(), 180, 10);
        if(casino.getJcourant().getObjectif() == -1) {
        	g.drawString("argent : infini", 180, 30);
            g.drawString("jeton : infini", 180, 50);
        }else {
        	g.drawString("argent : " + casino.getJcourant().getArgent(), 180, 30);
            g.drawString("jeton : " + casino.getJcourant().getJeton(), 180, 50);
        }
        if(casino.getJcourant().isObjet(JoueurCourant.verre)) {
        	g.drawImage(verre, 175, 70);
        }
        if(casino.getJcourant().isObjet(JoueurCourant.patte_de_lapin)) {
        	g.drawImage(patteDeLapin, 230, 70);
        }
        if(casino.getJcourant().isObjet(JoueurCourant.abonnement)) {
        	g.drawImage(abonnement, 175, 130);
		}
		if(casino.getJcourant().isObjet(JoueurCourant.garde_du_corps)) {
			g.drawImage(gardeDuCorps, 230, 130);
		}
		if(casino.hasTrophee(0)) {
			g.drawImage(trophee1, 285, 70);
		}
		if(casino.hasTrophee(1)) {
			g.drawImage(trophee2, 340, 70);
		}
		if(casino.hasTrophee(2)) {
			g.drawImage(trophee3, 285, 130);
		}
		
		//afficher message
		if(!timerMess.hasFinished()) {
        	g.setColor(Color.black);
        	g.drawImage(fondMess, 1000, 400);
        	g.setColor(Color.red);
        	g.drawString(message, 1010, 410);
        }
        
        //afficher erreur
        if(!timerErr.hasFinished()) {
        	g.setColor(Color.black);
        	g.drawImage(fondErr, 1700, 10);
        	g.setColor(Color.red);
        	g.drawString(erreur, 1710, 20);
        }
        
        //afficher retour
        if(act.getLieu() == Action.menu) g.drawImage(retour, 1850, 1000);
    }
    
    /***********************************************************************************/
    /***********************************************************************************/
    /***********************************************************************************/
    /***********************************GESTION CLAVIER*********************************/
    /***********************************************************************************/
    /***********************************************************************************/
    /***********************************************************************************/
    
    @Override
    public void keyPressed(int key, char c) {
    	if(touche.hasFinished()) {
    		touche.restart();
    		//TODO � enlever 
    		if(c=='d') casino.getJcourant().setArgent(100000);
	    	/***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /*************************************CASINO****************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	    	if(act.getLieu() == Action.casino) {
	    		if(key == Input.KEY_ESCAPE) {
	        		fermer();
	        	}
	    		if(key == Input.KEY_P) {
	    			act.setLieu(Action.poker);
	    			Poker poker = new Poker(25000, act, bruit);
	    			casino.setPoker(poker);
	    			if(son) ambiance.stop();
	    			if(son) ambianceCarte.loop();
	    		}
	    		if(key == Input.KEY_B) {
	    			act.setLieu(Action.blackjack);
	    			BlackJack bj = new BlackJack(25000, act, bruit);
	    			casino.setBlackJack(bj);
	    			if(son) ambiance.stop();
	    			if(son) ambianceCarte.loop();
	    		}
	    		if(key == Input.KEY_R) {
	    			act.setLieu(Action.roulette);
	    			Roulette roulette = new Roulette(5000, casino.getJcourant());
	    			casino.setRoulette(roulette);
	    			if(son) ambiance.stop();
	    			if(son) ambianceChance.loop();
	    		}
	    		if(key == Input.KEY_M) {
	    			act.setLieu(Action.machine);
	    			MachineSou machine = new MachineSou(5000, casino.getJcourant());
	    			casino.setMachineSou(machine);
	    			if(son) ambiance.stop();
	    			if(son) ambianceChance.loop();
				}
				if(key == Input.KEY_C) {
					act.setLieu(Action.pari);
					Pari pari = new Pari(18000, casino.getJcourant());
					casino.setPari(pari);
					if(son) ambiance.stop();
					if(son) ambianceChance.loop();
				}
				if(key == Input.KEY_TAB) {
					act.setLieu(Action.menu);
					Menu menu = new Menu();
					casino.setMenu(menu);
				}
	    	}
	    	/***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /****************************************POKER**************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	    	if(act.getLieu() == Action.poker) {
	    		if(key == Input.KEY_ESCAPE && !casino.getPoker().isPlay()) {
	    			act.setLieu(Action.casino);
	    			if(son) ambianceCarte.stop();
	    			if(son) ambiance.loop();
	    			sauvegarde();
	        	}
	    		if(key == Input.KEY_SPACE && !casino.getPoker().isPlay()) {
	    			if(casino.getPoker().getMise() <= casino.getPoker().getJoueur().getJeton()
	    					|| casino.getJcourant().getJeton() == -1) {
	    				casino.getJeu2carte().setPoker();
	    				aJouer = true;
	    			}else {
	    				messErr("Pas assez de jetons\npour jouer.");
	    			}
	    		}
	    		if(casino.getPoker().isPlay() && !casino.getPoker().isTimeFinished()) {
	    			if(key == Input.KEY_ESCAPE) {
	    				act.setKey((char) 27);
	    				aJouer = false;
	    				act.setLieu(Action.casino);
		    			if(son) ambianceCarte.stop();
		    			if(son) ambiance.loop();
		    			sauvegarde();
	    			}else {
	    				act.setKey(c);
	    			}
	    		}
	    		if(key == Input.KEY_UP && casino.getPoker().isPlay()  && !casino.getPoker().isTimeFinished()
	    				&& casino.getPoker().isRelance()) {
	    			casino.getPoker().setNbRelance(1);
	    		}
	    		if(key == Input.KEY_DOWN && casino.getPoker().isPlay()  && !casino.getPoker().isTimeFinished()
	    				&& casino.getPoker().isRelance()) {
	    			casino.getPoker().setNbRelance(-1);
	    		}
	    		if(key == Input.KEY_UP && !casino.getPoker().isPlay()) {
	    			casino.getPoker().setMise(1);
	    		}
	    		if(key == Input.KEY_DOWN && !casino.getPoker().isPlay()) {
	    			casino.getPoker().setMise(-1);
	    		}
	    	}
	    	/***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /**************************************BLACKJACK************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	    	if(act.getLieu() == Action.blackjack) {
	    		if(key == Input.KEY_ESCAPE && !casino.getBlackJack().isPlay()) {
	    			act.setLieu(Action.casino);
	    			if(son) ambianceCarte.stop();
	    			if(son) ambiance.loop();
	    			sauvegarde();
	        	}
	    		if(key == Input.KEY_SPACE && !casino.getBlackJack().isPlay()) {
	    			if(casino.getBlackJack().getMise() <= casino.getBlackJack().getJoueur().getJeton()
	    					|| casino.getJcourant().getJeton() == -1) {
	    				casino.getJeu2carte().setBJ();
	    				aJouer = true;
	    			}else {
	    				messErr("Pas assez de jetons\npour jouer.");
	    			}
	    		}
	    		if(casino.getBlackJack().isPlay() && !casino.getBlackJack().isTimeFinished() 
	    				&& !casino.getBlackJack().isMiseAct()) {
	    			if(key == Input.KEY_ESCAPE) {
	    				act.setKey((char) 27);
	    				aJouer = false;
	    				act.setLieu(Action.casino);
		    			if(son) ambianceCarte.stop();
		    			if(son) ambiance.loop();
		    			sauvegarde();
	    			}else {
	    				act.setKey(c);
	    			}
	    		}
	    		if(casino.getBlackJack().isPlay() && !casino.getBlackJack().isTimeFinished() 
	    				&& casino.getBlackJack().isMiseAct()) {
	    			if(key == Input.KEY_DOWN) {
	    				act.setKey('d');
	    			}else if(key == Input.KEY_UP) {
	    				act.setKey('u');
	    			}else if(key == Input.KEY_ENTER) {
	    				act.setKey('a');
	    			}else if(key == Input.KEY_ESCAPE) {
	    				act.setKey((char) 27);
	    				aJouer = false;
	    				act.setLieu(Action.casino);
		    			if(son) ambianceCarte.stop();
		    			if(son) ambiance.loop();
		    			sauvegarde();
	    			}else{
	    				act.setKey(c);
	    			}
	    		}
	    		if(key == Input.KEY_UP && !casino.getBlackJack().isPlay()) {
	    			casino.getBlackJack().suivMise();
	    		}
	    		if(key == Input.KEY_DOWN && !casino.getBlackJack().isPlay()) {
	    			casino.getBlackJack().precMise();
	    		}
	    	}
	    	/***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /**************************************ROULETTE*************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	    	if(act.getLieu() == Action.roulette) {
	    		if(key == Input.KEY_ESCAPE && casino.getRoulette().isTimeFinished()) {
	    			if(casino.getRoulette().isPretPari()) {
	    				casino.getRoulette().setPretPari(false);
	    			}else {
	    				act.setLieu(Action.casino);
	    				if(son) ambianceChance.stop();
	    				if(son) ambiance.loop();
	    				sauvegarde();
	    			}
	        	}
	    		if(key == Input.KEY_SPACE && casino.getRoulette().isTimeFinished() && casino.getRoulette().isPretPari()) {
	    			if(casino.getRoulette().getMise() <= casino.getRoulette().getJcourant().getJeton()
	    					|| casino.getJcourant().getJeton() == -1) {
	    				casino.getRoulette().resetTime();
		    			tmpJ = casino.getRoulette().play();
		    			setPosBille(casino.getRoulette().getNumero());
	    			}else {
	    				messErr("Pas assez de jetons\npour jouer.");
	    			}
	    		}
	    		if(key == Input.KEY_ENTER && casino.getRoulette().isTimeFinished() && !casino.getRoulette().isPretPari()) {
	    			casino.getRoulette().setPretPari(true);
	    		}
	    		if(key == Input.KEY_UP && casino.getRoulette().isTimeFinished() && casino.getRoulette().isPretPari()) {
	    			casino.getRoulette().suivPari();
	    		}
	    		if(key == Input.KEY_DOWN && casino.getRoulette().isTimeFinished() && casino.getRoulette().isPretPari()) {
	    			casino.getRoulette().precPari();
	    		}
	    		if(key == Input.KEY_UP && casino.getRoulette().isTimeFinished() && !casino.getRoulette().isPretPari()) {
	    			casino.getRoulette().setTypePari(1);
	    		}
	    		if(key == Input.KEY_DOWN && casino.getRoulette().isTimeFinished() && !casino.getRoulette().isPretPari()) {
	    			casino.getRoulette().setTypePari(-1);
	    		}
	    		if(key == Input.KEY_RIGHT && casino.getRoulette().isTimeFinished()) {
	    			casino.getRoulette().setMise(1);
	    		}
	    		if(key == Input.KEY_LEFT && casino.getRoulette().isTimeFinished()) {
	    			casino.getRoulette().setMise(-1);
	    		}
	    	}
	    	/***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /**************************************MACHINE**************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	    	if(act.getLieu() == Action.machine) {
	    		if(key == Input.KEY_ESCAPE && casino.getMachineSou().isTimeFinished()) {
	    			act.setLieu(Action.casino);
	    			if(son) ambianceChance.stop();
	    			if(son) ambiance.loop();
	    			sauvegarde();
	        	}
	    		if(key == Input.KEY_SPACE && casino.getMachineSou().isTimeFinished()) {
	    			if(casino.getMachineSou().getMise() <= casino.getMachineSou().getJcourant().getJeton()
	    					|| casino.getJcourant().getJeton() == -1) {
	    				casino.getMachineSou().resetTime();
	    				tmpJ = casino.getMachineSou().play();
	    			}else {
	    				messErr("Pas assez de jetons\npour jouer.");
	    			}
	    		}
	    		if(key == Input.KEY_UP && casino.getMachineSou().isTimeFinished()) {
	    			casino.getMachineSou().addChoix();
	    		}
	    		if(key == Input.KEY_DOWN && casino.getMachineSou().isTimeFinished()) {
	    			casino.getMachineSou().suppChoix();
	    		}
	    	}
	    	/***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /*****************************************PARI**************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	    	if(act.getLieu() == Action.pari) {
	    		if(key == Input.KEY_ESCAPE && casino.getPari().isTimeFinished()) {
	    			if(casino.getPari().isPretPari()) {
	    				casino.getPari().setPretPari(false);
	    			}else {
		    			act.setLieu(Action.casino);
		    			if(son) ambianceChance.stop();
		    			if(son) ambiance.loop();
		    			sauvegarde();
	    			}
	        	}
	    		if(key == Input.KEY_SPACE && casino.getPari().isTimeFinished() && casino.getPari().isPretPari()) {
	    			if(casino.getPari().pariComplet()) {
		    			if(casino.getPari().getMise() <= casino.getPari().getJcourant().getJeton()
		    					|| casino.getJcourant().getJeton() == -1) {
		    				casino.getPari().resetTime();
		    				tmpJ = casino.getPari().play();
		    				setVitesseCheval(casino.getPari().getResultat());
		    			}else {
		    				messErr("Pas assez de jetons\npour jouer.");
		    			}
	    			}else {
	    				messErr("pari incomplet");
	    			}
	    		}
	    		if(key == Input.KEY_ENTER && casino.getPari().isTimeFinished() && casino.getPari().isPretPari()) {
	    			casino.getPari().setPari(cheval);
	    		}
	    		if(key == Input.KEY_ENTER && casino.getPari().isTimeFinished() && !casino.getPari().isPretPari()) {
	    			casino.getPari().setPretPari(true);
	    		}
	    		if(key == Input.KEY_UP && casino.getPari().isTimeFinished() && casino.getPari().isPretPari()) {
	    			cheval--;
	    			if(cheval == -1) cheval = casino.getPari().nbrChevaux()-1;
	    		}
	    		if(key == Input.KEY_DOWN && casino.getPari().isTimeFinished() && casino.getPari().isPretPari()) {
	    			cheval = (cheval+1) % casino.getPari().nbrChevaux();
	    		}
	    		if(key == Input.KEY_UP && casino.getPari().isTimeFinished() && !casino.getPari().isPretPari()) {
	    			casino.getPari().suivPari();
	    		}
	    		if(key == Input.KEY_DOWN && casino.getPari().isTimeFinished() && !casino.getPari().isPretPari()) {
	    			casino.getPari().precPari();
	    		}
	    		if(key == Input.KEY_RIGHT && casino.getPari().isTimeFinished()) {
	    			casino.getPari().setMise(1);
	    		}
	    		if(key == Input.KEY_LEFT && casino.getPari().isTimeFinished()) {
	    			casino.getPari().setMise(-1);
	    		}
	    	}
	    	/***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /****************************************MENU***************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	    	if(act.getLieu() == Action.menu) {
	    		if(key == Input.KEY_ESCAPE) {
					if(casino.getMenu().getLoc() == Menu.principal) {
	    				act.setLieu(Action.casino);
	    			}else if(casino.getMenu().getInTuto() != -1 ){
	    				casino.getMenu().setInTuto(-1);
	    			}else{
						casino.getMenu().setLoc(Menu.principal);
	    			}
	        	}
	    		if(key == Input.KEY_UP) {
	    			casino.getMenu().setSelect(-1);
	    		}
	    		if(key == Input.KEY_DOWN) {
	    			casino.getMenu().setSelect(1);
	    		}
	    		if(key == Input.KEY_ENTER) {
	    			if(casino.getMenu().getLoc() == Menu.principal) {
	    				casino.getMenu().setLoc(casino.getMenu().getSelect());
	    			}else if(casino.getMenu().getLoc() == Menu.joueur) {
	    				switch(casino.getMenu().getSelect()) {
	    					case 1: casino.setJcourant(casino.getJfacile()); break;
	    					case 2: casino.setJcourant(casino.getJmoyen()); break;
	    					case 3: casino.setJcourant(casino.getJdifficile()); break;
	    					case 4: casino.setJcourant(casino.getJlibre()); break;
	    				}
	    			}else if(casino.getMenu().getLoc() == Menu.convertir) {
	    				switch(casino.getMenu().getSelect()) {
	    					case 1: if(casino.getJcourant().echangeJeton(10) == -1 ) {
	    								messErr("pas assez de jeton");
	    							}
	    					 		break;
	    					case 2: if(casino.getJcourant().echangeJeton(1000) == -1 ) {
										messErr("pas assez de jeton");
									}
							 		break;
	    					case 3: if(casino.getJcourant().echangeArgent(100) == -1){
			    						messErr("pas assez d'argent");
			    					}
			    					break;
	    					case 4: if(casino.getJcourant().echangeArgent(10000) == -1){
			    						messErr("pas assez d'argent");
			    					}
			    					break;
	    				}
	    			}else if(casino.getMenu().getLoc() == Menu.objet){
	    				switch(casino.getMenu().getSelect()) {
	    					case 1: if(casino.getJcourant().setArgent(-10) == -1) {
	    								messErr("pas assez d'argent \n(10$)");
	    							}else {
	    								casino.getJcourant().addObjet(JoueurCourant.verre);
	    								messJeu("acheter");
	    								sauvegarde();
	    							}
	    							break;
	    					case 2: if(casino.getJcourant().isObjet(JoueurCourant.patte_de_lapin)) {
	    								messErr("objet d�j� acquis");
	    							}else {
				    					if(casino.getJcourant().setArgent(-1000) == -1) {
											messErr("pas assez d'argent \n(1000$)");
										}else {
											casino.getJcourant().addObjet(JoueurCourant.patte_de_lapin);
											messJeu("acheter");
											sauvegarde();
										}
	    							}
	    							break;
	    					case 3: if(casino.getJcourant().isObjet(JoueurCourant.abonnement)) {
			    						messErr("objet d�j� acquis");
			    					}else {
				    					if(casino.getJcourant().setArgent(-10000) == -1) {
											messErr("pas assez d'argent \n(10000$)");
										}else {
											casino.getJcourant().addObjet(JoueurCourant.abonnement);
											messJeu("acheter");
											sauvegarde();
										}
			    					}
	    							break;
	    					case 4: if(casino.getJcourant().isObjet(JoueurCourant.garde_du_corps)) {
			    						messErr("objet d�j� acquis");
			    					}else {
				    					if(casino.getJcourant().setArgent(-100000) == -1) {
											messErr("pas assez d'argent \n(100000$)");
										}else {
											casino.getJcourant().addObjet(JoueurCourant.garde_du_corps);
											messJeu("acheter");
											sauvegarde();
										}
			    					}
	    							break;
	    				}
	    			}else if(casino.getMenu().getLoc() == Menu.option) {
	    				switch(casino.getMenu().getSelect()) {
	    					case 1: son = !son;
	    							if(!son) {
			    						ambiance.stop();
			    					}else {
			    						ambiance.loop();
			    					}
	    							sauvegarde();
	    							break;
	    					case 2: bruit.setBruit();
									sauvegarde();
									break;
	    					case 3: fulls = !fulls; sauvegarde(); break;
	    				}
	    			}else if(casino.getMenu().getLoc() == Menu.tuto && casino.getMenu().getInTuto() == -1) {
	    				switch(casino.getMenu().getSelect()) {
	    					case 1: casino.getMenu().setInTuto(0);
	    						break;
	    					case 2: casino.getMenu().setInTuto(1);
	    						break;
	    					case 3: casino.getMenu().setInTuto(2);
	    						break;
	    					case 4: casino.getMenu().setInTuto(3);
	    						break;
	    					case 5: casino.getMenu().setInTuto(4);
	    						break;
	    				}
	    			}
	    		}
	    	}
	    	
	    	
	    	super.keyPressed(key, c);
	    }
    }
    
    /***********************************************************************************/
    /***********************************************************************************/
    /***********************************************************************************/
    /***********************************GESTION SOURIS**********************************/
    /***********************************************************************************/
    /***********************************************************************************/
    /***********************************************************************************/
    
    @Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
    	/***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /****************************************MENU***************************************/
        /***********************************************************************************/
        /***********************************************************************************/
        /***********************************************************************************/
    	if(act.getLieu() == Action.menu && newx > 550 && newx < 1350) {
    		if(casino.getMenu().getLoc() == Menu.principal) {
    			if(newy > 200 && newy < 300) {
    				casino.getMenu().setSelectP(1);
    			}
    			if(newy > 350 && newy < 450) {
    				casino.getMenu().setSelectP(2);
    			}
    			if(newy > 500 && newy < 600) {
    				casino.getMenu().setSelectP(3);
    			}
    			if(newy > 650 && newy < 750) {
    				casino.getMenu().setSelectP(4);
    			}
    			if(newy > 800 && newy < 900) {
    				casino.getMenu().setSelectP(5);
    			}
    			if(newy > 950 && newy < 1050) {
    				casino.getMenu().setSelectP(6);
    			}
    		}else if(casino.getMenu().getLoc() == Menu.joueur || casino.getMenu().getLoc() == Menu.convertir 
    				|| casino.getMenu().getLoc() == Menu.objet || casino.getMenu().getLoc() == Menu.option) {
    			if(newy > 250 && newy < 400) {
    				casino.getMenu().setSelectP(1);
    			}
    			if(newy > 450 && newy < 600) {
    				casino.getMenu().setSelectP(2);
    			}
    			if(newy > 650 && newy < 800) {
    				casino.getMenu().setSelectP(3);
    			}
    			if(newy > 850 && newy < 1000) {
    				casino.getMenu().setSelectP(4);
    			}
    		}else if(casino.getMenu().getLoc() == Menu.tuto && casino.getMenu().getInTuto() == -1) {
    			if(newy > 200 && newy < 300) {
    				casino.getMenu().setSelectP(1);
    			}
    			if(newy > 350 && newy < 450) {
    				casino.getMenu().setSelectP(2);
    			}
    			if(newy > 500 && newy < 600) {
    				casino.getMenu().setSelectP(3);
    			}
    			if(newy > 650 && newy < 750) {
    				casino.getMenu().setSelectP(4);
    			}
    			if(newy > 800 && newy < 900) {
    				casino.getMenu().setSelectP(5);
    			}
    		}
    	}
    	
    	super.mouseMoved(oldx, oldy, newx, newy);
    }
    
    @Override
    public void mouseClicked(int button, int x, int y, int clickCount) {
    	if(button == Input.MOUSE_LEFT_BUTTON) {
    		/***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /*************************************CASINO****************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	    	if(act.getLieu() == Action.casino) {
	    		if(x > 800 && x < 1125 && y > 400 && y < 750) {
	    			act.setLieu(Action.poker);
	    			Poker poker = new Poker(25000, act, bruit);
	    			casino.setPoker(poker);
	    			if(son) ambiance.stop();
	    			if(son) ambianceCarte.loop();
	    		}
	    		if(x > 125 && x < 475 && y > 650 && y < 1050) {
	    			act.setLieu(Action.blackjack);
	    			BlackJack bj = new BlackJack(25000, act, bruit);
	    			casino.setBlackJack(bj);
	    			if(son) ambiance.stop();
	    			if(son) ambianceCarte.loop();
	    		}
	    		if(x > 1450 && x < 1800 && y > 710 && y < 1000) {
	    			act.setLieu(Action.roulette);
	    			Roulette roulette = new Roulette(5000, casino.getJcourant());
	    			casino.setRoulette(roulette);
	    			if(son) ambiance.stop();
	    			if(son) ambianceChance.loop();
	    		}
	    		if(x > 1425 && x < 1700 && y > 300 && y < 600) {
	    			act.setLieu(Action.machine);
	    			MachineSou machine = new MachineSou(5000, casino.getJcourant());
	    			casino.setMachineSou(machine);
	    			if(son) ambiance.stop();
	    			if(son) ambianceChance.loop();
				}
				if(x > 740 && x < 1080 && y > 25 && y < 225) {
					act.setLieu(Action.pari);
					Pari pari = new Pari(18000, casino.getJcourant());
					casino.setPari(pari);
					if(son) ambiance.stop();
					if(son) ambianceChance.loop();
				}
				if(x > 150 && x < 775 && y > 100 && y < 600) {
					act.setLieu(Action.menu);
					Menu menu = new Menu();
					casino.setMenu(menu);
				}
	    	}else
	    	
	    	/***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /****************************************MENU***************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	        /***********************************************************************************/
	    	if(act.getLieu() == Action.menu) {
	    		if(x > 1850 && x < 1900 && y > 1000 && y < 1050) {
	    			if(casino.getMenu().getLoc() == Menu.principal) {
	    				act.setLieu(Action.casino);
	    			}else if(casino.getMenu().getLoc() == Menu.tuto && casino.getMenu().getInTuto() != -1){
	    				casino.getMenu().setInTuto(-1);
	    			}else{
	    				casino.getMenu().setLoc(Menu.principal);
	    			}
	    		}else if(casino.getMenu().getLoc() == Menu.principal) {
	    			casino.getMenu().setLoc(casino.getMenu().getSelect());
	    		}else if(casino.getMenu().getLoc() == Menu.joueur) {
	    			switch(casino.getMenu().getSelect()) {
	    			case 1: casino.setJcourant(casino.getJfacile()); break;
	    			case 2: casino.setJcourant(casino.getJmoyen()); break;
	    			case 3: casino.setJcourant(casino.getJdifficile()); break;
	    			case 4: casino.setJcourant(casino.getJlibre()); break;
	    			}
	    		}else if(casino.getMenu().getLoc() == Menu.convertir) {
	    			switch(casino.getMenu().getSelect()) {
	    			case 1: if(casino.getJcourant().echangeJeton(10) == -1 ) {
	    				messErr("pas assez de jeton");
	    			}
	    			break;
	    			case 2: if(casino.getJcourant().echangeJeton(1000) == -1 ) {
	    				messErr("pas assez de jeton");
	    			}
	    			break;
	    			case 3: if(casino.getJcourant().echangeArgent(100) == -1){
	    				messErr("pas assez d'argent");
	    			}
	    			break;
	    			case 4: if(casino.getJcourant().echangeArgent(10000) == -1){
	    				messErr("pas assez d'argent");
	    			}
	    			break;
	    			}
	    		}else if(casino.getMenu().getLoc() == Menu.objet){
	    			switch(casino.getMenu().getSelect()) {
	    			case 1: if(casino.getJcourant().setArgent(-10) == -1) {
	    				messErr("pas assez d'argent \n(10$)");
	    			}else {
	    				casino.getJcourant().addObjet(JoueurCourant.verre);
	    				messJeu("acheter");
	    				sauvegarde();
	    			}
	    			break;
	    			case 2: if(casino.getJcourant().isObjet(JoueurCourant.patte_de_lapin)) {
	    				messErr("objet d�j� acquis");
	    			}else {
	    				if(casino.getJcourant().setArgent(-1000) == -1) {
	    					messErr("pas assez d'argent \n(1000$)");
	    				}else {
	    					casino.getJcourant().addObjet(JoueurCourant.patte_de_lapin);
	    					messJeu("acheter");
	    					sauvegarde();
	    				}
	    			}
	    			break;
	    			case 3: if(casino.getJcourant().isObjet(JoueurCourant.abonnement)) {
	    				messErr("objet d�j� acquis");
	    			}else {
	    				if(casino.getJcourant().setArgent(-10000) == -1) {
	    					messErr("pas assez d'argent \n(10000$)");
	    				}else {
	    					casino.getJcourant().addObjet(JoueurCourant.abonnement);
	    					messJeu("acheter");
	    					sauvegarde();
	    				}
	    			}
	    			break;
	    			case 4: if(casino.getJcourant().isObjet(JoueurCourant.garde_du_corps)) {
	    				messErr("objet d�j� acquis");
	    			}else {
	    				if(casino.getJcourant().setArgent(-100000) == -1) {
	    					messErr("pas assez d'argent \n(100000$)");
	    				}else {
	    					casino.getJcourant().addObjet(JoueurCourant.garde_du_corps);
	    					messJeu("acheter");
	    					sauvegarde();
	    				}
	    			}
	    			break;
	    			}
	    		}else if(casino.getMenu().getLoc() == Menu.option) {
	    			switch(casino.getMenu().getSelect()) {
	    			case 1: son = !son;
	    			if(!son) {
	    				ambiance.stop();
	    			}else {
	    				ambiance.loop();
	    			}
	    			sauvegarde();
	    			break;
	    			case 2: bruit.setBruit();
	    			sauvegarde();
	    			break;
	    			case 3: fulls = !fulls; sauvegarde(); break;
	    			}
	    		}else if(casino.getMenu().getLoc() == Menu.tuto && casino.getMenu().getInTuto() == -1) {
	    			switch(casino.getMenu().getSelect()) {
	    			case 1: casino.getMenu().setInTuto(0);
	    			break;
	    			case 2: casino.getMenu().setInTuto(1);
	    			break;
	    			case 3: casino.getMenu().setInTuto(2);
	    			break;
	    			case 4: casino.getMenu().setInTuto(3);
	    			break;
	    			case 5: casino.getMenu().setInTuto(4);
	    			break;
	    			}
	    		}
	    	}
    	}
    	
    	super.mouseClicked(button, x, y, clickCount);
    }
    
    private void setPosBille(double num) {
    	if(num == 0.5) {
    		posBille = 31;
    	}else {
    		switch((int) num) {
    			case 0: posBille = 12; break;
    			case 1: posBille = 30; break;
    			case 2: posBille = 11; break;
    			case 3: posBille = 26; break;
    			case 4: posBille = 7; break;
    			case 5: posBille = 22; break;
    			case 6: posBille = 3; break;
    			case 7: posBille = 18; break;
    			case 8: posBille = 37; break;
    			case 9: posBille = 14; break;
    			case 10: posBille = 33; break;
    			case 11: posBille = 17; break;
    			case 12: posBille = 36; break;
    			case 13: posBille = 29; break;
    			case 14: posBille = 10; break;
    			case 15: posBille = 25; break;
    			case 16: posBille = 6; break;
    			case 17: posBille = 21; break;
    			case 18: posBille = 2; break;
    			case 19: posBille = 0; break;
    			case 20: posBille = 19; break;
    			case 21: posBille = 4; break;
    			case 22: posBille = 23; break;
    			case 23: posBille = 8; break;
    			case 24: posBille = 27; break;
    			case 25: posBille = 34; break;
    			case 26: posBille = 15; break;
    			case 27: posBille = 32; break;
    			case 28: posBille = 13; break;
    			case 29: posBille = 35; break;
    			case 30: posBille = 16; break;
    			case 31: posBille = 1; break;
    			case 32: posBille = 20; break;
    			case 33: posBille = 5; break;
    			case 34: posBille = 24; break;
    			case 35: posBille = 9; break;
    			case 36: posBille = 28; break;
    		}
    	}
    }
    
    private void setVitesseCheval(int[] res) {
    	vitesseCheval.clear();
    	posCheval.clear();
    	posCheval.put("P0", 0);
    	posCheval.put("P1", 0);
    	posCheval.put("P2", 0);
    	posCheval.put("P3", 0);
    	posCheval.put("P4", 0);
    	posCheval.put("P5", 0);
    	posCheval.put("P6", 0);
    	posCheval.put("P7", 0);
    	posCheval.put("P8", 0);
    	posCheval.put("P9", 0);
    	for(int i=0; i<res.length; i++) {
    		vitesseCheval.put("P"+res[i], (res.length-i)+1);
    	}
    }
    
    private void messJeu(String str) {
    	message = str;
    	timerMess.restart();
    }
    
    private void messErr(String err) {
    	erreur = err;
    	timerErr.restart();
    }
    
    private void fermer() {
    	sauvegarde();
    	close = true;
    }
    
    private void sauvegarde() {
    	File sauvegarde = new File("donnee/infoPerso.txt");
    	try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(sauvegarde));
			String str = casino.getJfacile().getArgent()+"\n"+casino.getJfacile().getJeton()+"\n"+
			casino.getJfacile().isObjet(1)+"\n"+casino.getJfacile().isObjet(2)+"\n"+
			casino.getJfacile().isObjet(3)+"\n"+casino.getJmoyen().getArgent()+"\n"+
			casino.getJmoyen().getJeton()+"\n"+casino.getJmoyen().isObjet(1)+"\n"+
			casino.getJmoyen().isObjet(2)+"\n"+casino.getJmoyen().isObjet(3)+"\n"+
			casino.getJdifficile().getArgent()+"\n"+casino.getJdifficile().getJeton()+"\n"+
			casino.getJdifficile().isObjet(1)+"\n"+casino.getJdifficile().isObjet(2)+"\n"+
			casino.getJdifficile().isObjet(3)+"\n"+son+"\n"+bruit.isBruit()+"\n"+fulls;
			switch(casino.getJcourant().getNom()) {
				case "patrick" : str = str.concat("\n1"); break;
				case "xavier" : str = str.concat("\n2"); break;
				case "hubert" : str = str.concat("\n3"); break;
				case "frederic" : str = str.concat("\n4"); break;
			}
			str = str.concat("\n"+casino.hasTrophee(0)+"\n"+casino.hasTrophee(1)+"\n"+casino.hasTrophee(2));
			bw.write(crypte(str));
			bw.close();
			
		} catch (IOException e) {
			messErr("impossible de sauvegarder");
			e.printStackTrace();
		}
    }
    
    private void charger() {
    	File sauvegarde = new File("donnee/infoPerso.txt");
    	try {
			BufferedReader br = new BufferedReader(new FileReader(sauvegarde));
			String str = decrypter(br.readLine());
			if(!str.equals("")) {
				String[] tab = str.split("\n");
				casino.getJfacile().setArgent(Integer.valueOf(tab[0]) - casino.getJfacile().getArgent());
				casino.getJfacile().setJeton(Integer.valueOf(tab[1]) - casino.getJfacile().getJeton());
				
				if(tab[2].equals("true")) {
					casino.getJfacile().addObjet(1);
				}
				if(tab[3].equals("true")) {
					casino.getJfacile().addObjet(2);
				}
				if(tab[4].equals("true")) {
					casino.getJfacile().addObjet(3);
				}
				
				casino.getJmoyen().setArgent(Integer.valueOf(tab[5]) - casino.getJmoyen().getArgent());
				casino.getJmoyen().setJeton(Integer.valueOf(tab[6]) - casino.getJmoyen().getJeton());
				if(tab[7].equals("true")) {
					casino.getJmoyen().addObjet(1);
				}
				if(tab[8].equals("true")) {
					casino.getJmoyen().addObjet(2);
				}
				if(tab[9].equals("true")) {
					casino.getJmoyen().addObjet(3);
				}
				
				casino.getJdifficile().setArgent(Integer.valueOf(tab[10]) - casino.getJdifficile().getArgent());
				casino.getJdifficile().setJeton(Integer.valueOf(tab[11]) - casino.getJdifficile().getJeton());
				if(tab[12].equals("true")) {
					casino.getJdifficile().addObjet(1);
				}
				if(tab[13].equals("true")) {
					casino.getJdifficile().addObjet(2);
				}
				if(tab[14].equals("true")) {
					casino.getJdifficile().addObjet(3);
				}
				
				if(tab[15].equals("false")) {
					son = false;
				}
				
				if(tab[16].equals("false")) {
					bruit.setBruit();
				}
				
				if(tab[17].equals("false")) {
					fulls = false;
				}
				
				switch(Integer.valueOf(tab[18])) {
					case 1: casino.setJcourant(casino.getJfacile()); break;
					case 2: casino.setJcourant(casino.getJmoyen()); break;
					case 3: casino.setJcourant(casino.getJdifficile()); break;
					case 4: casino.setJcourant(casino.getJlibre()); break;
				}
				if(tab[19].equals("true")) {
					casino.addTrophee(0);
				}
				if(tab[20].equals("true")) {
					casino.addTrophee(1);
				}
				if(tab[21].equals("true")) {
					casino.addTrophee(2);
				}
				
				br.close();
			}else {
				messErr("pas de sauvegarde");
			}
		} catch (NumberFormatException | IOException e) {
			messErr("pas de sauvegarde");
			e.printStackTrace();
		}
    }
    
    private String crypte(String str) {
    	String res = str;
    	res = res.replace('0', 'z');
    	res = res.replace('1', 'y');
    	res = res.replace('2', 'w');
    	res = res.replace('3', 'j');
    	res = res.replace('4', 'k');
    	res = res.replace('5', 'p');
    	res = res.replace('6', 'm');
    	res = res.replace('7', 'n');
    	res = res.replace('8', 'g');
    	res = res.replace('9', 'v');
    	res = res.replace('r', 'c');
    	byte[] b = res.getBytes();
    	for(int i = 0; i< b.length; i++) {
    		b[i] = (byte) (~b[i]);
    	}
    	Charset defaultCharset = Charset.defaultCharset();
    	res = new String(b, defaultCharset);
    	res = res.substring((res.length()/2), res.length()).concat(res.substring(0, (res.length()/2)));
    	return res;
    }
    
    private String decrypter(String str) {
    	if(str!=null && !str.equals("")) {
    		str = str.substring((str.length()/2), str.length()).concat(str.substring(0, (str.length()/2)));
	    	byte[] b = str.getBytes();
	    	for(int i = 0; i< b.length; i++) {
	    		b[i] = (byte) (~b[i]);
	    	}
	    	Charset defaultCharset = Charset.defaultCharset();
	    	String res = new String(b, defaultCharset);
	    	res = res.replace('z', '0');
	    	res = res.replace('y', '1');
	    	res = res.replace('w', '2');
	    	res = res.replace('j', '3');
	    	res = res.replace('k', '4');
	    	res = res.replace('p', '5');
	    	res = res.replace('m', '6');
	    	res = res.replace('n', '7');
	    	res = res.replace('g', '8');
	    	res = res.replace('v', '9');
	    	res = res.replace('c', 'r');
	    	return res;
    	}
    	return "";
    }
}
