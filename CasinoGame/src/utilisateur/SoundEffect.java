package utilisateur;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

public class SoundEffect {
	
	private boolean bruit;//activer ou non le son
	private Sound sonCarte1;
	private Sound sonCarte2;
	private Sound sonMelangeCarte;
	private Sound sonChip1;
	private Sound sonChip2;
	private Sound sonRoulette;
	private Sound sonMachine;
	private Sound sonMachineWin;
	private Sound sonPari;
	
	SoundEffect(){
		bruit = true;
		try {
			sonCarte1 = new Sound("son/sonCarte1.wav");
			sonCarte2 = new Sound("son/sonCarte2.wav");
	    	sonMelangeCarte = new Sound("son/sonMelangeCarte.wav");
	    	sonChip1 = new Sound("son/sonChip1.wav");
	    	sonChip2 = new Sound("son/sonChip2.wav");
	    	sonRoulette = new Sound("son/sonRoulette.wav");
	    	sonMachine = new Sound("son/sonMachine.wav");
	    	sonMachineWin = new Sound("son/sonMachineWin.wav");
	    	sonPari = new Sound("son/sonPari.wav");
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	public boolean isBruit() {
		return bruit;
	}
	
	public boolean setBruit() {
		return bruit = !bruit;
	}

	public Sound getSonCarte1() {
		return sonCarte1;
	}

	public Sound getSonCarte2() {
		return sonCarte2;
	}

	public Sound getSonMelangeCarte() {
		return sonMelangeCarte;
	}

	public Sound getSonChip1() {
		return sonChip1;
	}

	public Sound getSonChip2() {
		return sonChip2;
	}

	public Sound getSonRoulette() {
		return sonRoulette;
	}

	public Sound getSonMachine() {
		return sonMachine;
	}

	public Sound getSonMachineWin() {
		return sonMachineWin;
	}

	public Sound getSonPari() {
		return sonPari;
	}
	
	

}
