package utilisateur;

import jeu2carte.BlackJack;
import jeu2carte.Poker;

public class ThreadJeu implements Runnable{
	
	private Poker jeuPoker;
	private BlackJack jeuBJ;
	private boolean poker;
	private boolean BJ;
	
	private int resultPoker;
	private int resultBJ;
	
	public ThreadJeu() {
		jeuPoker = null;
		jeuBJ = null;
		poker = false;
		BJ = false;
	}
	
	@Override
	public void run() {
		while(true) {
			if(this.poker) {
				jeuPoker.resetTime();
				jeuPoker.setPlay(true);
				resultPoker = jeuPoker.play();
				poker = false;
			}
			if(this.BJ) {
				jeuBJ.resetTime();
				jeuBJ.setPlay(true);
				resultBJ = jeuBJ.play();
				BJ = false;
			}
			System.out.print("");
		}
	}

	public Poker getJeuPoker() {
		return jeuPoker;
	}

	public void setJeuPoker(Poker jeuPoker) {
		this.jeuPoker = jeuPoker;
	}

	public BlackJack getJeuBJ() {
		return jeuBJ;
	}

	public void setJeuBJ(BlackJack jeuBJ) {
		this.jeuBJ = jeuBJ;
	}

	public void setPoker() {
		this.poker = true;
	}

	public void setBJ() {
		BJ = true;
	}

	public int getResultPoker() {
		return resultPoker;
	}

	public int getResultBJ() {
		return resultBJ;
	}
	
	
}
