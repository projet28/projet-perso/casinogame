import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

import casino.Casino;
import utilisateur.Action;
import utilisateur.UI;

public class Main {
	
    public static void main(String[] args) throws SlickException
    {
    	//cr�er le casino
    	Casino casino = new Casino();
    	
    	//cr�er les capture des �v�nement clavier
    	Action act = new Action();
    	
    	UI ui = new UI(casino, act);
    	
    	//cr�er l'affichage graphique
        AppGameContainer app = new AppGameContainer(ui);
        //taille et mode fen�tre
        app.setDisplayMode(1920, 1080, true);
        app.setShowFPS(true);
        //lancer le jeu
        app.start();
        
        while(!ui.isClose());
        
        app.exit();
        
    }
	
     
}
