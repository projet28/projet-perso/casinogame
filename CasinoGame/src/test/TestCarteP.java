package test;

import org.junit.Test;
import jeu2carte.Carte;
import jeu2carte.MainPoker;
import jeu2carte.Paquet;

import static org.junit.Assert.assertTrue;

public class TestCarteP {
	
	Paquet p = new Paquet();

	@Test
	public void testPaquet() {
		p.completer();
		assertTrue("taille paquet", p.getSize() == 52);
		p.piocher();
		p.piocher();
		assertTrue("pioche", p.getSize() == 50);
		p.completer();
		assertTrue("taille revenu", p.getSize() == 52);
		Paquet p2 = p;
		p2.melanger();
		boolean notsame = false;
		int i = 0;
		while(!notsame && i<p.getSize()) {
			if(p.piocher() != p2.piocher()) notsame = true;
			i++;
		}
		assertTrue("melanger", notsame);
	}
	
	@Test
	public void testMain() {
		MainPoker m1 = new MainPoker();
		m1.ajouter(new Carte(0,1), false);
		assertTrue("prendre une carte", m1.getSize() == 1);
		m1.reprendreMain();
		assertTrue("enelever carte", m1.getSize() == 0);
	}
	
	@Test
	public void testQFR() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(1,2), false);
		m1.ajouter(new Carte(0,1), false);
		m1.ajouter(new Carte(0,13), false);
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(3,5), false);
		m1.ajouter(new Carte(0,10), false);
		m1.ajouter(new Carte(0,11), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		
		assertTrue("compare main QFR", m1.value(m1.getMain()) == 9 && m1.compareTo(m2) > 0);
	}
	
	@Test
	public void testQF() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(1,2), false);
		m1.ajouter(new Carte(0,9), false);
		m1.ajouter(new Carte(0,13), false);
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(3,5), false);
		m1.ajouter(new Carte(0,10), false);
		m1.ajouter(new Carte(0,11), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		
		assertTrue("compare main QF", m1.value(m1.getMain()) == 8 && m1.compareTo(m2) > 0);
	}
	
	@Test
	public void testCarre() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(1,2), false);
		m1.ajouter(new Carte(2,9), false);
		m1.ajouter(new Carte(0,13), false);
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(3,5), false);
		m1.ajouter(new Carte(0,10), false);
		m1.ajouter(new Carte(0,11), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(1,4), false);
		m2.ajouter(new Carte(0,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(2,2), false);
		m2.ajouter(new Carte(3,2), false);
		m2.ajouter(new Carte(1,3), false);
		m2.ajouter(new Carte(1,9), false);
		
		assertTrue("compare main carre", m2.value(m2.getMain()) == 7 && m1.compareTo(m2) < 0);
	}
	
	@Test
	public void testFull() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(1,2), false);
		m1.ajouter(new Carte(2,9), false);
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(3,9), false);
		m1.ajouter(new Carte(0,10), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		
		assertTrue("compare main full", m1.value(m1.getMain()) == 6 && m1.compareTo(m2) < 0);
	}
	
	@Test
	public void testFlush() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(2,9), false);
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(3,10), false);
		m1.ajouter(new Carte(0,10), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(1,5), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(0,4), false);
		m2.ajouter(new Carte(1,8), false);
		m2.ajouter(new Carte(2,6), false);
		m2.ajouter(new Carte(1,3), false);
		m2.ajouter(new Carte(0,2), false);
		
		assertTrue("compare main flush", m1.value(m1.getMain()) == 5 && m1.compareTo(m2) > 0);
	}
	
	@Test
	public void testQuinte() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(2,9), false);
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(3,10), false);
		m1.ajouter(new Carte(0,10), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(1,5), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(0,4), false);
		m2.ajouter(new Carte(1,8), false);
		m2.ajouter(new Carte(2,6), false);
		m2.ajouter(new Carte(1,3), false);
		m2.ajouter(new Carte(0,2), false);
		
		
		assertTrue("compare main quinte", m2.value(m2.getMain()) == 4 && m1.compareTo(m2) > 0);
	}
	
	@Test
	public void testBrelan() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(2,10), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,12), false);
		m1.ajouter(new Carte(3,10), false);
		m1.ajouter(new Carte(0,10), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(1,5), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(0,4), false);
		m2.ajouter(new Carte(1,8), false);
		m2.ajouter(new Carte(2,6), false);
		m2.ajouter(new Carte(1,3), false);
		m2.ajouter(new Carte(0,2), false);
		
		assertTrue("compare main Brelan", m1.value(m1.getMain()) == 3 && m1.compareTo(m2) < 0);
	}
	
	@Test
	public void testDblPaire() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,9), false);
		m1.ajouter(new Carte(0,6), false);
		m1.ajouter(new Carte(1,11), false);
		m1.ajouter(new Carte(0,10), false);
		m1.ajouter(new Carte(1,6), false);
		m1.ajouter(new Carte(3,11), false);
		m1.ajouter(new Carte(2,12), false);
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(1,5), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(0,4), false);
		m2.ajouter(new Carte(1,8), false);
		m2.ajouter(new Carte(2,6), false);
		m2.ajouter(new Carte(1,3), false);
		m2.ajouter(new Carte(0,2), false);
		
		assertTrue("compare main dbl paire", m1.value(m1.getMain()) == 2 && m1.compareTo(m2) < 0);
	}
	
	@Test
	public void testPaire() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(2,8), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,12), false);
		m1.ajouter(new Carte(3,10), false);
		m1.ajouter(new Carte(0,9), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(1,5), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(0,4), false);
		m2.ajouter(new Carte(1,8), false);
		m2.ajouter(new Carte(2,6), false);
		m2.ajouter(new Carte(1,3), false);
		m2.ajouter(new Carte(0,2), false);
		
		assertTrue("compare main paire", m1.value(m1.getMain()) == 1 && m1.compareTo(m2) < 0);
	}
	
	@Test
	public void testHauteEg() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(2,8), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,12), false);
		m1.ajouter(new Carte(3,10), false);
		m1.ajouter(new Carte(0,13), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,2), false);
		m2.ajouter(new Carte(2,8), false);
		m2.ajouter(new Carte(0,3), false);
		m2.ajouter(new Carte(1,12), false);
		m2.ajouter(new Carte(3,10), false);
		m2.ajouter(new Carte(0,13), false);
		m2.ajouter(new Carte(0,9), false);
		
		assertTrue("compare main haute egal", m1.value(m1.getMain()) == 0 && m1.compareTo(m2) == 0);
	}
	
	@Test
	public void testHaute() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(2,8), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,12), false);
		m1.ajouter(new Carte(3,10), false);
		m1.ajouter(new Carte(0,13), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,2), false);
		m2.ajouter(new Carte(2,8), false);
		m2.ajouter(new Carte(0,3), false);
		m2.ajouter(new Carte(1,12), false);
		m2.ajouter(new Carte(3,10), false);
		m2.ajouter(new Carte(0,11), false);
		m2.ajouter(new Carte(0,4), false);
		
		assertTrue("compare main haute", m1.value(m1.getMain()) == 0 && m1.compareTo(m2) > 0);
	}
	
	@Test
	public void testPairHaute() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(2,8), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,12), false);
		m1.ajouter(new Carte(3,10), false);
		m1.ajouter(new Carte(0,9), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,2), false);
		m2.ajouter(new Carte(2,7), false);
		m2.ajouter(new Carte(0,3), false);
		m2.ajouter(new Carte(1,13), false);
		m2.ajouter(new Carte(3,10), false);
		m2.ajouter(new Carte(0,8), false);
		m2.ajouter(new Carte(0,8), false);
		
		assertTrue("compare pair haute", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) > 0);
	}
	
	@Test
	public void testPairHauteEg() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(2,8), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,12), false);
		m1.ajouter(new Carte(3,10), false);
		m1.ajouter(new Carte(0,13), false);
		m1.ajouter(new Carte(0,13), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,2), false);
		m2.ajouter(new Carte(2,7), false);
		m2.ajouter(new Carte(0,3), false);
		m2.ajouter(new Carte(1,12), false);
		m2.ajouter(new Carte(3,10), false);
		m2.ajouter(new Carte(0,13), false);
		m2.ajouter(new Carte(0,13), false);
		
		assertTrue("compare pair haute egalit�", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) > 0);
	}
	
	@Test
	public void testDoublePairHaute() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(2,8), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,10), false);
		m1.ajouter(new Carte(3,10), false);
		m1.ajouter(new Carte(0,9), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,2), false);
		m2.ajouter(new Carte(2,7), false);
		m2.ajouter(new Carte(0,3), false);
		m2.ajouter(new Carte(1,10), false);
		m2.ajouter(new Carte(3,10), false);
		m2.ajouter(new Carte(0,8), false);
		m2.ajouter(new Carte(0,8), false);
		
		assertTrue("compare double pair haute", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) > 0);
	}
	
	@Test
	public void testDoublePairHaute2() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(2,8), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,10), false);
		m1.ajouter(new Carte(3,10), false);
		m1.ajouter(new Carte(0,9), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,2), false);
		m2.ajouter(new Carte(2,7), false);
		m2.ajouter(new Carte(0,3), false);
		m2.ajouter(new Carte(1,13), false);
		m2.ajouter(new Carte(3,13), false);
		m2.ajouter(new Carte(0,8), false);
		m2.ajouter(new Carte(0,8), false);
		
		assertTrue("compare double pair haute", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) < 0);
	}
	
	@Test
	public void testDoublePairHauteEg() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(2,8), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,10), false);
		m1.ajouter(new Carte(3,10), false);
		m1.ajouter(new Carte(0,9), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,2), false);
		m2.ajouter(new Carte(2,7), false);
		m2.ajouter(new Carte(0,3), false);
		m2.ajouter(new Carte(1,10), false);
		m2.ajouter(new Carte(3,10), false);
		m2.ajouter(new Carte(0,9), false);
		m2.ajouter(new Carte(0,9), false);
		
		assertTrue("compare double pair haute egalit�", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) > 0);
	}
	
	@Test
	public void testBrelanHaute() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(2,8), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,10), false);
		m1.ajouter(new Carte(3,9), false);
		m1.ajouter(new Carte(0,9), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,2), false);
		m2.ajouter(new Carte(2,7), false);
		m2.ajouter(new Carte(0,3), false);
		m2.ajouter(new Carte(1,13), false);
		m2.ajouter(new Carte(3,8), false);
		m2.ajouter(new Carte(0,8), false);
		m2.ajouter(new Carte(0,8), false);
		
		assertTrue("compare brelan haute", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) > 0);
	}
	
	@Test
	public void testBrelanHauteEg() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,2), false);
		m1.ajouter(new Carte(2,8), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,10), false);
		m1.ajouter(new Carte(3,9), false);
		m1.ajouter(new Carte(0,9), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,2), false);
		m2.ajouter(new Carte(2,7), false);
		m2.ajouter(new Carte(0,3), false);
		m2.ajouter(new Carte(1,10), false);
		m2.ajouter(new Carte(3,9), false);
		m2.ajouter(new Carte(0,9), false);
		m2.ajouter(new Carte(0,9), false);
		
		assertTrue("compare brelan haute egalit�", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) > 0);
	}
	
	@Test
	public void testQuinteHaute() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(2,8), false);
		m1.ajouter(new Carte(0,11), false);
		m1.ajouter(new Carte(1,10), false);
		m1.ajouter(new Carte(3,7), false);
		m1.ajouter(new Carte(0,8), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,4), false);
		m2.ajouter(new Carte(2,7), false);
		m2.ajouter(new Carte(0,3), false);
		m2.ajouter(new Carte(1,13), false);
		m2.ajouter(new Carte(3,5), false);
		m2.ajouter(new Carte(0,6), false);
		m2.ajouter(new Carte(0,8), false);
		
		assertTrue("compare Quinte haute", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) > 0);
	}
	
	@Test
	public void testQuinteHauteEg() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(2,7), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,10), false);
		m1.ajouter(new Carte(3,8), false);
		m1.ajouter(new Carte(0,6), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,2), false);
		m2.ajouter(new Carte(2,7), false);
		m2.ajouter(new Carte(0,13), false);
		m2.ajouter(new Carte(1,10), false);
		m2.ajouter(new Carte(3,8), false);
		m2.ajouter(new Carte(0,6), false);
		m2.ajouter(new Carte(0,9), false);
		
		assertTrue("compare Quinte haute egalit�", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) == 0);
	}
	
	@Test
	public void testFlushHaute() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(2,8), false);
		m1.ajouter(new Carte(0,11), false);
		m1.ajouter(new Carte(0,10), false);
		m1.ajouter(new Carte(3,7), false);
		m1.ajouter(new Carte(0,8), false);
		m1.ajouter(new Carte(0,2), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,4), false);
		m2.ajouter(new Carte(2,7), false);
		m2.ajouter(new Carte(0,3), false);
		m2.ajouter(new Carte(0,13), false);
		m2.ajouter(new Carte(3,5), false);
		m2.ajouter(new Carte(0,6), false);
		m2.ajouter(new Carte(0,8), false);
		
		assertTrue("compare Flush haute", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) < 0);
	}
	
	@Test
	public void testFlushHauteEg() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(2,7), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,10), false);
		m1.ajouter(new Carte(0,8), false);
		m1.ajouter(new Carte(0,6), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,3), false);
		m2.ajouter(new Carte(2,7), false);
		m2.ajouter(new Carte(0,12), false);
		m2.ajouter(new Carte(1,10), false);
		m2.ajouter(new Carte(0,8), false);
		m2.ajouter(new Carte(0,6), false);
		m2.ajouter(new Carte(0,9), false);
		
		assertTrue("compare Flush haute egalit�", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) == 0);
	}
	
	@Test
	public void testFullHaute() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,11), false);
		m1.ajouter(new Carte(2,8), false);
		m1.ajouter(new Carte(0,11), false);
		m1.ajouter(new Carte(0,11), false);
		m1.ajouter(new Carte(3,7), false);
		m1.ajouter(new Carte(0,7), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,11), false);
		m2.ajouter(new Carte(2,11), false);
		m2.ajouter(new Carte(0,8), false);
		m2.ajouter(new Carte(0,11), false);
		m2.ajouter(new Carte(3,5), false);
		m2.ajouter(new Carte(0,6), false);
		m2.ajouter(new Carte(0,8), false);
		
		assertTrue("compare Full haute", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) < 0);
	}
	
	@Test
	public void testFullHaute2() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(2,7), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,12), false);
		m1.ajouter(new Carte(0,7), false);
		m1.ajouter(new Carte(0,7), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,8), false);
		m2.ajouter(new Carte(2,8), false);
		m2.ajouter(new Carte(0,10), false);
		m2.ajouter(new Carte(1,10), false);
		m2.ajouter(new Carte(0,8), false);
		m2.ajouter(new Carte(0,6), false);
		m2.ajouter(new Carte(0,9), false);
		
		
		assertTrue("compare Full haute", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) < 0);
	}
	
	@Test
	public void testFullHauteEg() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(2,7), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(1,12), false);
		m1.ajouter(new Carte(0,7), false);
		m1.ajouter(new Carte(0,7), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,7), false);
		m2.ajouter(new Carte(2,7), false);
		m2.ajouter(new Carte(0,12), false);
		m2.ajouter(new Carte(1,12), false);
		m2.ajouter(new Carte(0,7), false);
		m2.ajouter(new Carte(0,6), false);
		m2.ajouter(new Carte(0,9), false);
		
		assertTrue("compare Full haute egalit�", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) == 0);
	}
	
	@Test
	public void testQuinteFlushHaute() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(0,8), false);
		m1.ajouter(new Carte(0,11), false);
		m1.ajouter(new Carte(0,10), false);
		m1.ajouter(new Carte(0,7), false);
		m1.ajouter(new Carte(0,13), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,12), false);
		m2.ajouter(new Carte(0,11), false);
		m2.ajouter(new Carte(0,10), false);
		m2.ajouter(new Carte(1,13), false);
		m2.ajouter(new Carte(0,9), false);
		m2.ajouter(new Carte(0,6), false);
		m2.ajouter(new Carte(0,8), false);
		
		assertTrue("compare QuinteFlush haute", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) > 0);
	}
	
	@Test
	public void testQuinteFlushHauteEg() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(0,7), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(0,10), false);
		m1.ajouter(new Carte(0,8), false);
		m1.ajouter(new Carte(0,6), false);
		m1.ajouter(new Carte(0,9), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(0,2), false);
		m2.ajouter(new Carte(0,7), false);
		m2.ajouter(new Carte(0,13), false);
		m2.ajouter(new Carte(0,10), false);
		m2.ajouter(new Carte(0,8), false);
		m2.ajouter(new Carte(0,6), false);
		m2.ajouter(new Carte(0,9), false);
		
		assertTrue("compare QuinteFlush haute egalit�", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) == 0);
	}
	
	@Test
	public void testQFREg() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(1,2), false);
		m1.ajouter(new Carte(0,1), false);
		m1.ajouter(new Carte(0,13), false);
		m1.ajouter(new Carte(0,12), false);
		m1.ajouter(new Carte(3,5), false);
		m1.ajouter(new Carte(0,10), false);
		m1.ajouter(new Carte(0,11), false);
		
		MainPoker m2 = new MainPoker();
		
		m2.ajouter(new Carte(1,10), false);
		m2.ajouter(new Carte(1,11), false);
		m2.ajouter(new Carte(1,12), false);
		m2.ajouter(new Carte(1,13), false);
		m2.ajouter(new Carte(1,1), false);
		m2.ajouter(new Carte(1,2), false);
		m2.ajouter(new Carte(1,2), false);
		
		assertTrue("compare main QFR egalit�", m1.value(m1.getMain()) == m2.value(m2.getMain()) && m1.compareTo(m2) == 0);
	}
	
	@Test
	public void testMain4() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(1,2), false);
		m1.ajouter(new Carte(0,1), false);
		m1.ajouter(new Carte(0,13), false);
		m1.ajouter(new Carte(0,2), false);
		
		assertTrue("petite main de 4 cartes", m1.getMain().size() == 4 && m1.value(m1.getMain()) == 1);
	}
	
	@Test
	public void testMain6() {
		MainPoker m1 = new MainPoker();
		
		m1.ajouter(new Carte(1,2), false);
		m1.ajouter(new Carte(0,1), false);
		m1.ajouter(new Carte(0,13), false);
		m1.ajouter(new Carte(0,4), false);
		m1.ajouter(new Carte(0,3), false);
		m1.ajouter(new Carte(0,2), false);
		
		assertTrue("main � partir de 6 main", m1.getMain().size() == 5 && m1.value(m1.getMain()) == 5);
	}
}
