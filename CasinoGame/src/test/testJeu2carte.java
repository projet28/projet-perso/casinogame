package test;

import org.junit.Test;

import jeu2carte.BlackJack;
import jeu2carte.Poker;
import joueur.JoueurCourant;

import static org.junit.Assert.assertTrue;

public class testJeu2carte {
	
	JoueurCourant j = new JoueurCourant("test", 100, 100, 100, "test", 1);
	
	//pas ouf les test
	
	Poker poker = new Poker(5000, null, null);
	
	@Test
	public void testRepartJeton() {
		int[] jeton = Poker.getRepartJeton(0);
		assertTrue("test 0 jeton", jeton[0] == 0 && jeton[1] == 0 && jeton[2] == 0 && jeton[3] == 0 &&
				jeton[4] == 0 && jeton[5] == 0 && jeton[6] == 0 && jeton[7] == 0);
		
		jeton = Poker.getRepartJeton(8);
		assertTrue("test 8 jeton", jeton[0] == 8);
		
		jeton = Poker.getRepartJeton(12);
		assertTrue("test 12 jeton", jeton[0] == 12);
		
		jeton = Poker.getRepartJeton(15);
		assertTrue("test 15 jeton", jeton[0] == 10 && jeton[1] == 1);
		
		jeton = Poker.getRepartJeton(22);
		assertTrue("test 22 jeton", jeton[0] == 12 && jeton[1] == 2);
		
		jeton = Poker.getRepartJeton(36);
		assertTrue("test 36 jeton", jeton[0] == 11 && jeton[1] == 5);
		
		jeton = Poker.getRepartJeton(46);
		assertTrue("test 46 jeton", jeton[0] == 11 && jeton[1] == 5 && jeton[2] == 1);
		
		jeton = Poker.getRepartJeton(62);
		assertTrue("test 62 jeton", jeton[0] == 12 && jeton[1] == 6 && jeton[2] == 2);
		
		jeton = Poker.getRepartJeton(85);
		assertTrue("test 85 jeton", jeton[0] == 10 && jeton[1] == 5 && jeton[2] == 5);
		
		jeton = Poker.getRepartJeton(152);
		assertTrue("test 152 jeton", jeton[0] == 12 && jeton[1] == 6 && jeton[2] == 6 && jeton[3] == 2);
		
		jeton = Poker.getRepartJeton(33209);
		assertTrue("test 33210 jeton", jeton[0] == 14 && jeton[1] == 5 && jeton[2] == 7 && jeton[3] == 8 &&
				jeton[4] == 9 && jeton[5] == 6 && jeton[6] == 9 && jeton[7] == 4);
		
		jeton = Poker.getRepartJeton(33210);
		assertTrue("test 33210 jeton", jeton[0] == 10 && jeton[1] == 5 && jeton[2] == 5 && jeton[3] == 5 &&
				jeton[4] == 5 && jeton[5] == 5 && jeton[6] == 5 && jeton[7] == 5);
		
		jeton = Poker.getRepartJeton(44321);
		assertTrue("test 33210 jeton", jeton[0] == 11 && jeton[1] == 5 && jeton[2] == 6 && jeton[3] == 5 &&
				jeton[4] == 6 && jeton[5] == 5 && jeton[6] == 6 && jeton[7] == 7);
	}
	
	@Test
	public void testPoker() {
		poker.addJoueur(j);
		int jeton = poker.getJoueur().getJeton();
		int res = poker.play();
		int nouvJeton = poker.getJoueur().getJeton();
		assertTrue("test poker", (res == 1 && nouvJeton > jeton) ||
				(res == 0 && nouvJeton < jeton));
	}
	
	BlackJack BJ = new BlackJack(1000, null, null);
	
	@Test
	public void testBJ() {
		BJ.addJoueur(j);
		int jeton = BJ.getJoueur().getJeton();
		int res = BJ.play();
		int nouvJeton = BJ.getJoueur().getJeton();
		assertTrue("test BJ", (res == 1 && nouvJeton > jeton) ||
				(res == 0 && nouvJeton < jeton));
	}

}
