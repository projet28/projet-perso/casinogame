package test;

import org.junit.Test;

import jeu2chance.MachineSou;
import jeu2chance.Pari;
import jeu2chance.Roulette;
import joueur.JoueurCourant;

import static org.junit.Assert.assertTrue;

public class testJeu2chance {
	
	JoueurCourant j = new JoueurCourant("test", 100, 100, 100, "test", 1);
	
	MachineSou ma = new MachineSou(5000, j);
	
	
	@Test
	public void testMachine() {
		int jeton = ma.getJcourant().getJeton();
		ma.addChoix();
		int res = ma.play();
		int nouvJeton = ma.getJcourant().getJeton();
		assertTrue("test machine", res >= 0 && res - 1 == nouvJeton - jeton);
	}
	
	Pari pari = new Pari(5000, j);
	
	@Test
	public void testPari() {
		int jeton = pari.getJcourant().getJeton();
		pari.setMise(100);
		int res = pari.play();
		int nouvJeton = pari.getJcourant().getJeton();
		assertTrue("test pari", res >= 0 && res - pari.getMise() == nouvJeton - jeton);
	}
	
	Roulette roulette = new Roulette(5000, j);

	@Test
	public void testRoulette() {
		int jeton = roulette.getJcourant().getJeton();
		roulette.setMise(100);
		int res = roulette.play();
		int nouvJeton = roulette.getJcourant().getJeton();
		assertTrue("test roulette", res >= 0 && res - roulette.getMise() == nouvJeton - jeton);
	}
}
