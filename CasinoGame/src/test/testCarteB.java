package test;

import org.junit.Test;
import jeu2carte.Carte;
import jeu2carte.MainBJ;
import jeu2carte.Paquet;

import static org.junit.Assert.assertTrue;

public class testCarteB {

	
	Paquet p = new Paquet();

	@Test
	public void testPaquet() {
		p.completer();
		assertTrue("taille paquet", p.getSize() == 52);
		p.piocher();
		p.piocher();
		assertTrue("pioche", p.getSize() == 50);
		p.completer();
		assertTrue("taille revenu", p.getSize() == 52);
		Paquet p2 = p;
		p2.melanger();
		boolean notsame = false;
		int i = 0;
		while(!notsame && i<p.getSize()) {
			if(p.piocher() != p2.piocher()) notsame = true;
			i++;
		}
		assertTrue("melanger", notsame);
	}
	
	@Test
	public void testMain() {
		MainBJ m1 = new MainBJ();
		m1.ajouter(new Carte(0,1));
		assertTrue("prendre une carte", m1.getSize() == 1);
		m1.reprendreMain();
		assertTrue("enelever carte", m1.getSize() == 0);
	}
	
	@Test
	public void testBJ() {
		MainBJ m1 = new MainBJ();
		
		m1.ajouter(new Carte(1,1));
		m1.ajouter(new Carte(1,12));
		
		MainBJ m2 = new MainBJ();
		
		m2.ajouter(new Carte(1,10));
		m2.ajouter(new Carte(1,10));
		m2.ajouter(new Carte(1,1));
		
		
		assertTrue("compare main BJ", m1.value(m1.getMain()) == 0 && m1.compareTo(m2) > 0 
				&& m2.value(m2.getMain()) == 21);
	}
	
	@Test
	public void testplusde21() {
		MainBJ m1 = new MainBJ();
		
		m1.ajouter(new Carte(1,11));
		m1.ajouter(new Carte(1,12));
		m1.ajouter(new Carte(1,1));
		m1.ajouter(new Carte(1,1));
		
		MainBJ m2 = new MainBJ();
		
		m2.ajouter(new Carte(1,10));
		m2.ajouter(new Carte(1,13));
		
		
		assertTrue("compare main + de 21", m1.value(m1.getMain()) == 22 && m1.compareTo(m2) < 0 
				&& m2.value(m2.getMain()) == 20);
	}
	
	@Test
	public void testEg1() {
		MainBJ m1 = new MainBJ();
		
		m1.ajouter(new Carte(1,11));
		m1.ajouter(new Carte(1,12));
		
		MainBJ m2 = new MainBJ();
		
		m2.ajouter(new Carte(1,10));
		m2.ajouter(new Carte(1,13));
		
		
		assertTrue("compare main egalit�", m1.value(m1.getMain()) == 20 && m1.compareTo(m2) == 0 
				&& m2.value(m2.getMain()) == 20);
	}
	
	@Test
	public void testEg2() {
		MainBJ m1 = new MainBJ();
		
		m1.ajouter(new Carte(1,11));
		m1.ajouter(new Carte(1,12));
		m1.ajouter(new Carte(1,12));
		
		MainBJ m2 = new MainBJ();
		
		m2.ajouter(new Carte(1,10));
		m2.ajouter(new Carte(1,13));
		m2.ajouter(new Carte(1,12));
		
		assertTrue("compare main egalit� + de 21", m1.value(m1.getMain()) == 30 && m1.compareTo(m2) == 0 
				&& m2.value(m2.getMain()) == 30);
	}
	
	@Test
	public void testEg3() {
		MainBJ m1 = new MainBJ();
		
		m1.ajouter(new Carte(1,11));
		m1.ajouter(new Carte(1,1));
		
		MainBJ m2 = new MainBJ();
		
		m2.ajouter(new Carte(1,10));
		m2.ajouter(new Carte(1,1));
		
		
		assertTrue("compare main egalit� 21 mais BJ", m1.value(m1.getMain()) == 0 && m1.compareTo(m2) > 0 
				&& m2.value(m2.getMain()) == 21);
	}
	
	@Test
	public void testsplit() {
		MainBJ m1 = new MainBJ();
		
		m1.ajouter(new Carte(1,11));
		m1.ajouter(new Carte(1,1));
		int i = m1.split();
		
		MainBJ m2 = new MainBJ();
		
		m2.ajouter(new Carte(1,10));
		m2.ajouter(new Carte(1,11));
		int j = m2.split();
		
		assertTrue("compare main split", i == -1 && m1.getSplit() == null &&
				m2.getSplit() != null && j == 0);
	}
}
