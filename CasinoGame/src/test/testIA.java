package test;

import org.junit.Test;

import jeu2carte.Carte;
import jeu2carte.MainBJ;
import jeu2carte.MainPoker;
import joueur.IA;

import static org.junit.Assert.assertTrue;

public class testIA {
	
	//ia niveau 0
	IA ia = new IA("test", 100, "test", 0, null);
	
	@Test
	public void testPoker0() {
		MainPoker main = new MainPoker();
		ia.setMainP(main);
		main.ajouter(new Carte(0,2), false);
		main.ajouter(new Carte(0,3), false);
		
		int checkres1 = ia.actionPoker(0, 10, 1);
		int blindres1 = ia.actionPoker(10, 10, 1);
		int blindres4 = ia.actionPoker(10, 10, 4);
		int relres1 = ia.actionPoker(20, 10, 1);
		int relres4 = ia.actionPoker(30, 10, 1);
		
		main.ajouter(new Carte(0,4), false);
		main.ajouter(new Carte(0,7), false);
		main.ajouter(new Carte(0,7), false);
		main.ajouter(new Carte(1,2), false);
		int checkres2 = ia.actionPoker(0, 10, 1);
		int blindres2 = ia.actionPoker(10, 10, 1);
		int relres2 = ia.actionPoker(20, 10, 1);
		
		main.ajouter(new Carte(1,2), false);
		int checkres3 = ia.actionPoker(0, 10, 1);
		int blindres3 = ia.actionPoker(10, 10, 1);
		int relres3 = ia.actionPoker(20, 10, 1);
		
		assertTrue("check", checkres1 == 0 && checkres2 == 20 && checkres3 == 100);
		assertTrue("suivre blind", blindres1 == 10 && blindres2 == 20 && blindres3 == 100 && blindres4 == -1);
		assertTrue("suivre relance", relres1 == 20 && relres2 == 40 && relres3 == 100 && relres4 == -1);
	}
	
	

	@Test
	public void testBJ0() {
		int mise = ia.miser();
		MainBJ main = new MainBJ();
		main.ajouter(new Carte(0,4));
		main.ajouter(new Carte(1,4));
		ia.setMainB(main);
		int split = ia.actionBJ(10);
		
		main.reprendreMain();
		main.ajouter(new Carte(0,4));
		main.ajouter(new Carte(1,10));
		int dble = ia.actionBJ(10);
		
		main.reprendreMain();
		main.ajouter(new Carte(0,4));
		main.ajouter(new Carte(1,1));
		
		int carte = ia.actionBJ(10);
		main.ajouter(new Carte(0,4));
		int reste = ia.actionBJ(10);
		
		
		assertTrue("BJ", mise == 10 && split == 2 && dble == 3 && carte == 1 && reste == -1);
	}
	
	//TODO les ia de chaque niveau
}
