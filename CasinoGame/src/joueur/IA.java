package joueur;

import utilisateur.SoundEffect;

public class IA extends Joueur{
	
	private SoundEffect son;
	
	private int niveau; //0 abruti, 1 faible, 2 moyen, 3 fort
	
	private int miseBJ;//la mise du joueur pour le bj

	public IA(String nom, int jeton, String icon, int niveau, SoundEffect son) {
		super(nom, jeton, icon);
		this.niveau = niveau;
		miseBJ = 0;
		
		this.son = son;
	}
	
	public int getMiseBJ() {
		return miseBJ;
	}

	public void setMiseBJ(int miseBJ) {
		this.miseBJ = miseBJ;
	}
	
	
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/**						 			POKER												 **/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	
	/**
	 * 
	 * @param mise la mise courante si 0 alors encore aucune mise
	 * @param blind la blind mise de d�part (mise minimum)
	 * @parma tour le tour courant
	 * @return -1 si se couche 
	 * 			la mise si il suit
	 * 			>mise si il relance
	 * 			0 si la mise �tait de zero aussi alors il check
	 * 			-11 si erreur
	 */
	public int actionPoker(int mise, int blind, int tour) {
		if(niveau == 0) {
			if(mise == 0) {
				return isCheck0(blind, tour);
			}else if(mise == blind) {
				return isSuivreBlind0(mise, tour);
			}else {
				return isSuivreRelance0(mise, blind, tour);
			}
		}
		if(niveau == 1) {
			if(mise == 0) {
				return isCheck1(blind, tour);
			}else if(mise == blind) {
				return isSuivreBlind1(mise, tour);
			}else {
				return isSuivreRelance1(mise, blind, tour);
			}
		}
		if(niveau == 2) {
			if(mise == 0) {
				return isCheck2(blind, tour);
			}else if(mise == blind) {
				return isSuivreBlind2(mise, tour);
			}else {
				return isSuivreRelance2(mise, blind, tour);
			}
		}
		if(niveau == 3) {
			if(mise == 0) {
				return isCheck3(blind, tour);
			}else if(mise == blind) {
				return isSuivreBlind3(mise, tour);
			}else {
				return isSuivreRelance3(mise, blind, tour);
			}
		}
		return -11;
	}
	
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/**							NIVEAU ABRUTI												 **/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	
	/**
	 * que fais le joueur si personne n'a mis�?
	 * 
	 * @param blind la blind mise de d�part (mise minimum)
	 * @parma tour le tour courant
	 * @return -1 si se couche 
	 * 			>mise si il relance
	 * 			0 si check
	 */
	private int isCheck0(int blind, int tour) {
		//si pas de mise il check toujours sauf
		//si grande main il mise gros
		if(this.mainP.value(this.mainP.getMain()) >= 6) {
			if(son.isBruit()) son.getSonChip1().play(); //TODO le faire partout il y a besoin
			return this.jeton; //all-in
		}
		if(this.mainP.value(this.mainP.getMain()) >= 4) {
			if(blind*2 <= this.jeton) return blind*2; //grosse mise
			return this.jeton; //all-in
		}
		return 0; //check
	}
	
	
	/**
	 * que fais le joueur si la mise est �quivalente � la blind?
	 * 
	 * @param mise la mise courante
	 * @parma tour le tour courant
	 * @return -1 si se couche 
	 * 			la mise si il suit
	 * 			>mise si il relance
	 */
	private int isSuivreBlind0(int mise, int tour) {
		//si main normal il suit sauf
		//si grande main il mise gros
		if(this.mainP.value(this.mainP.getMain()) >= 6) {
			return this.jeton; //all-in
		}
		if(this.mainP.value(this.mainP.getMain()) >= 4) {
			if(mise*2 <= this.jeton) return mise*2; //grosse mise
			return this.jeton; //all-in
		}
		if(this.mainP.value(this.mainP.getMain()) < 1 && tour > 3) {
			return -1; //se couche si rien et bientot la fin
		}
		return mise; //suit
	}
	
	
	/**
	 * que fais le joueur si la mise est sup�rieur � la blind?
	 * 
	 * @param mise la mise courante
	 * @param blind la blind mise de d�part (mise minimum)
	 * @parma tour le tour courant
	 * @return -1 si se couche 
	 * 			la mise si il suit
	 * 			>mise si il relance
	 */
	private int isSuivreRelance0(int mise, int blind, int tour) {
		//si main normal il suit sauf
		//si grande main il mise gros
		if(this.mainP.value(this.mainP.getMain()) >= 6) {
			return this.jeton; //all-in
		}
		if(this.mainP.value(this.mainP.getMain()) >= 4) {
			if(mise*2 <= this.jeton) return mise*2; //grosse mise
				return this.jeton; //all-in
		}
		if(this.mainP.value(this.mainP.getMain()) < 2 && blind*2 < mise) {
			return -1; //se couche si rien et trop grosse mise
		}
		return mise; //suit
	}
	
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/**							NIVEAU FAIBLE	//TODO										 **/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	
	/**
	 * que fais le joueur si personne n'a mis�?
	 * 
	 * @param blind la blind mise de d�part (mise minimum)
	 * @parma tour le tour courant
	 * @return -1 si se couche 
	 * 			>mise si il relance
	 * 			0 si check
	 */
	private int isCheck1(int blind, int tour) {
		return 1;
	}
	
	
	/**
	 * que fais le joueur si la mise est �quivalente � la blind?
	 * 
	 * @param mise la mise courante
	 * @parma tour le tour courant
	 * @return -1 si se couche 
	 * 			la mise si il suit
	 * 			>mise si il relance
	 */
	private int isSuivreBlind1(int mise, int tour) {
		return 1;
	}
	
	
	/**
	 * que fais le joueur si la mise est sup�rieur � la blind?
	 * 
	 * @param mise la mise courante
	 * @param blind la blind mise de d�part (mise minimum)
	 * @parma tour le tour courant
	 * @return -1 si se couche 
	 * 			la mise si il suit
	 * 			>mise si il relance
	 */
	private int isSuivreRelance1(int mise, int blind, int tour) {
		return 1;
	}
	
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/**							NIVEAU MOYEN		//TODO									 **/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	
	/**
	 * que fais le joueur si personne n'a mis�?
	 * 
	 * @param blind la blind mise de d�part (mise minimum)
	 * @parma tour le tour courant
	 * @return -1 si se couche 
	 * 			>mise si il relance
	 * 			0 si check
	 */
	private int isCheck2(int blind, int tour) {
		return 2;
	}
	
	
	/**
	 * que fais le joueur si la mise est �quivalente � la blind?
	 * 
	 * @param mise la mise courante
	 * @parma tour le tour courant
	 * @return -1 si se couche 
	 * 			la mise si il suit
	 * 			>mise si il relance
	 */
	private int isSuivreBlind2(int mise, int tour) {
		return 2;
	}
	
	
	/**
	 * que fais le joueur si la mise est sup�rieur � la blind?
	 * 
	 * @param mise la mise courante
	 * @param blind la blind mise de d�part (mise minimum)
	 * @parma tour le tour courant
	 * @return -1 si se couche 
	 * 			la mise si il suit
	 * 			>mise si il relance
	 */
	private int isSuivreRelance2(int mise, int blind, int tour) {
		return 2;
	}
	
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/**							NIVEAU FORT				//TODO								 **/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	
	/**
	 * que fais le joueur si personne n'a mis�?
	 * 
	 * @param blind la blind mise de d�part (mise minimum)
	 * @parma tour le tour courant
	 * @return -1 si se couche 
	 * 			>mise si il relance
	 * 			2 si check
	 */
	private int isCheck3(int blind, int tour) {
		return 3;
	}
	
	
	/**
	 * que fais le joueur si la mise est �quivalente � la blind?
	 * 
	 * @param mise la mise courante
	 * @parma tour le tour courant
	 * @return -1 si se couche 
	 * 			la mise si il suit
	 * 			>mise si il relance
	 */
	private int isSuivreBlind3(int mise, int tour) {
		return 3;
	}
	
	
	/**
	 * que fais le joueur si la mise est sup�rieur � la blind?
	 * 
	 * @param mise la mise courante
	 * @param blind la blind mise de d�part (mise minimum)
	 * @parma tour le tour courant
	 * @return -1 si se couche 
	 * 			la mise si il suit
	 * 			>mise si il relance
	 */
	private int isSuivreRelance3(int mise, int blind, int tour) {
		return 3;
	}
	
	
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/**						 			BLACK-JACK											 **/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	
	/**
	 * pour savoir quelle mise le joueur va mettre
	 * 
	 * @return la mise du joueur ou -1 si erreur
	 */
	public int miser() {
		if(niveau == 0) {
			return miser0();
		}
		if(niveau == 1) {
			return miser1();		
		}
		if(niveau == 2) {
			return miser2();
		}
		if(niveau == 3) {
			return miser3();
		}
		return -1;
	}
	
	/**
	 * 
	 * @param mise la mise courante
	 * @return -1 si il reste
	 * 			1 si il veut une autre carte
	 * 			2 si il split
	 * 			3 si il double
	 * 			-11 si erreur
	 */
	public int actionBJ(int mise) {
		if(niveau == 0) {
			if(mainB.canSplit()) {
				boolean split = isSplit0(mise);
				if(split) return 2;
			}else {
				return isReste0(mise);
			}
		} //TODO rajouter le cas doubler pour les autres niveau
		if(niveau == 1) {
			if(mainB.canSplit()) {
				boolean split = isSplit1(mise);
				if(split) return 2;
			}else {
				return isReste1(mise);
			}
		}
		if(niveau == 2) {
			if(mainB.canSplit()) {
				boolean split = isSplit2(mise);
				if(split) return 2;
			}else {
				return isReste2(mise);
			}
		}
		if(niveau == 3) {
			if(mainB.canSplit()) {
				boolean split = isSplit3(mise);
				if(split) return 2;
			}else {
				return isReste3(mise);
			}
		}
		return -11;
	}
	
	
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/**							NIVEAU ABRUTI												 **/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	
	/**
	 * pour savoir quelle mise le joueur va mettre
	 * 
	 * @return la mise du joueur ou -1 si erreur
	 */
	private int miser0() {
		if(son.isBruit()) son.getSonChip1().play(); //TODO le faire partout ou il faut
		if(this.jeton<10) return this.jeton; //il retourne tous ses jetons
		return 10; //ou 10 si il en a assez
	}
	
	/**
	 * savoir si le joueur va split
	 * 
	 * @param mise la mise du joueur
	 * @return true si il split false sinon
	 */
	private boolean isSplit0(int mise) {
		if(mise*2 <= jeton) {//il split tout le temps si il a assez de jetons
			return true;
		}
		return false;
	}
	
	/**
	 * savoir si le joueur veut une carte, reste ou double
	 * 
	 * @param mise la mise du joueur
	 * @return -1 si il reste
	 * 			1 si il veut une autre carte 
	 * 			3 si il double
	 */
	private int isReste0(int mise) {
		if(mainB.value(mainB.getMain()) == 14) {//si il a 14 il double
			return 3;
		}
		if(mainB.value(mainB.getMain()) < 17) {//si il � moins que 17 il continue (comme croupier)
			return 1;
		}
		return -1;
	}
	
	
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/**							NIVEAU FAIBLE				//TODO							 **/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	
	/**
	 * pour savoir quelle mise le joueur va mettre
	 * 
	 * @return la mise du joueur ou -1 si erreur
	 */
	private int miser1() {
		return 0;
	}
	
	/**
	 * savoir si le joueur va split
	 * 
	 * @param mise la mise du joueur
	 * @return true si il split false sinon
	 */
	private boolean isSplit1(int mise) {
		return false;
	}
	
	/**
	 * savoir si le joueur veut une carte, reste ou double
	 * 
	 * @param mise la mise du joueur
	 * @return -1 si il reste
	 * 			1 si il veut une autre carte 
	 * 			3 si il double
	 */
	private int isReste1(int mise) {
		return 0;
	}
	
	
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/**							NIVEAU MOYEN						//TODO					 **/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	
	/**
	 * pour savoir quelle mise le joueur va mettre
	 * 
	 * @return la mise du joueur ou -1 si erreur
	 */
	private int miser2() {
		return 0;
	}
	
	/**
	 * savoir si le joueur va split
	 * 
	 * @param mise la mise du joueur
	 * @return true si il split false sinon
	 */
	private boolean isSplit2(int mise) {
		return false;
	}
	
	/**
	 * savoir si le joueur veut une carte, reste ou double
	 * 
	 * @param mise la mise du joueur
	 * @return -1 si il reste
	 * 			1 si il veut une autre carte 
	 * 			3 si il double
	 */
	private int isReste2(int mise) {
		return 0;
	}
	
	
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/**							NIVEAU FORT							//TODO					 **/
	/*******************************************************************************************/
	/*******************************************************************************************/
	/*******************************************************************************************/
	
	/**
	 * pour savoir quelle mise le joueur va mettre
	 * 
	 * @return la mise du joueur ou -1 si erreur
	 */
	private int miser3() {
		return 0;
	}
	
	/**
	 * savoir si le joueur va split
	 * 
	 * @param mise la mise du joueur
	 * @return true si il split false sinon
	 */
	private boolean isSplit3(int mise) {
		return false;
	}
	
	/**
	 * savoir si le joueur veut une carte, reste ou double
	 * 
	 * @param mise la mise du joueur
	 * @return -1 si il reste
	 * 			1 si il veut une autre carte 
	 * 			3 si il double
	 */
	private int isReste3(int mise) {
		return 0;
	}
	
}
