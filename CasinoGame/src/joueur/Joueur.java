package joueur;

import jeu2carte.MainBJ;
import jeu2carte.MainPoker;

public class Joueur {
	protected String nom; //nom du joueur
	protected int jeton; //son nombre de jeton (si ia alors son nombre de jetons durant le jeu)
	
	protected String icon; //chemin d'acces a son image
	
	
	protected MainPoker mainP; //la main du joueur pour le poker
	protected MainBJ mainB; //la main du joueur pour le BJ
	
	public Joueur(String nom, int jeton, String icon) {
		this.nom = nom;
		this.jeton = jeton;
		this.icon = icon;
		mainP = new MainPoker();
		mainB = new MainBJ();
	}

	public String getNom() {
		return nom;
	}

	public int getJeton() {
		return jeton;
	}
	
	public String getIcon() {
		return icon;
	}
	
	/**
	 * modifie le nombre de jeton du joueur
	 * 
	 * @param nombres nombres de jeton � ajouter ou enlever
	 * 
	 */
	public void setJeton(int nombres) {
		if(this.jeton != -1) this.jeton += nombres;
	}

	public MainPoker getMainP() {
		return mainP;
	}

	public void setMainP(MainPoker main) {
		this.mainP = main;
	}
	
	public MainBJ getMainB() {
		return mainB;
	}

	public void setMainB(MainBJ main) {
		this.mainB = main;
	}
	
}
