package joueur;

public class JoueurCourant extends Joueur{
	private int argent; //l'argent du joueur
	private int objectif; //son objectif d'argent pour gagner
	private int chance; //la chance du joueur 1 chanceux, 2 normal, 3 mal-chanceux
	
	//donn�e initial du joueur pour revenir en arri�re
	private int Sjeton;
	private int Sargent;
	
	//la liste des objets sp�ciaux
	public static int verre = 0, patte_de_lapin = 1, abonnement = 2, garde_du_corps = 3;  
	private boolean[] objet;
	
	public JoueurCourant(String nom, int jeton, int argent, int objectif, String icon, int chance) {
		super(nom,jeton,icon);
		this.argent = argent;
		this.objectif = objectif;
		this.chance = chance;
		
		this.Sjeton = jeton;
		this.Sargent = argent;
		
		this.objet = new boolean[] {false,false,false,false};
	}

	public int getArgent() {
		return argent;
	}
	
	public int getObjectif() {
		return objectif;
	}
	
	public int getChance() {
		if(objet[patte_de_lapin]) {
			return chance-1;
		}
		return chance;
	}
	
	public void addObjet(int obj) {
		objet[obj] = true;
	}
	
	public boolean isObjet(int obj) {
		return objet[obj];
	}

	/**
	 * remet les informations initial du joueur 
	 * 
	 */
	public void restart() {
		this.jeton = this.Sjeton;
		this.argent = this.Sargent;
		this.objet = new boolean[] {false,false,false,false};
	}
	
	/**
	 * modifie le nombre d'argent du joueur
	 * 
	 * @param nombres nombres d'argent � ajouter ou enlever
	 * @return -1 si erreur
	 */
	public int setArgent(int nombres) {
		if(argent != -1) {
			if(argent + nombres >= 0) {
				return this.argent += nombres;
			}else {
				return -1;
			}
		}
		return 0;
	}
	
	/**
	 * �change des jetons en argent 1 jeton deviens 10 d'argent
	 * 
	 * precondition : on peut �changer qu'� partir de 10 jetons 
	 * @param jeton nombres de jeton � changer en argent
	 * @return le nouveau nombre d'argent sinon retourne -1
	 * 
	 */
	public int echangeJeton(int jeton) {
		if(argent != -1) {
			if(jeton<=this.jeton) {
				setJeton(-jeton);
				setArgent(jeton*10);
				return argent;
			}else {
				return -1;
			}
		}
		return 0;
	}
	
	/**
	 * �change de l'argent en jeton 10 d'argent deviens 1 jeton
	 * 
	 * precondition : on peut �changer qu'� partir de 10 d'argent et argent%10
	 * @param argent nombres d'argent � changer en jeton
	 * @return le nouveau nombre de jeton sinon retourne -1
	 * 
	 */
	public int echangeArgent(int argent) {
		if(this.argent != -1) {
			if(this.argent>=argent) {
				setArgent(-argent);
				setJeton(argent/10);
				return jeton;
			}else {
				return -1;
			}
		}
		return 0;
	}
}
