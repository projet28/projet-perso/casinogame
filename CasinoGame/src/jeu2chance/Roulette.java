package jeu2chance;

import joueur.JoueurCourant;

public class Roulette extends JeuDeChance{

	//attribus
	private int[] nombres; //les nombres possible de 0 � 36 avec 00
	private int[] pari;	//les nombres parier par le joueur
	private double numero; //le numero o� la bille va tomber 0.5 si 00
	
	//pour g�rer les choix ext�rieurs ( les grands nombres de chiffre)
	public static final int to18 = 1, even = 2, red = 3, black = 4, odd = 5, to36 = 6;
	private int choixExterieur; //le choix courant
	
	//pour l'ui
	private int typePari; //pari 1 : 1 jeton, 2 : 2 jeton ...
	private boolean pretPari; //a selectionn� sont types de paris et pr�t a selectionner le(s) num�ro
	
	
	/*
	 * https://www.casinonewsdaily.com/roulette-guide/american-roulette/
	 * voir les r�gles des diff�rentes mise
	 * 
	 * le but est de miser des jetons > 0 sur un ou plusieurs nombres
	 * payout de 1 to 1 signifie gagner sa mise plus 1 jeton
	 */
	
	public Roulette(long time, JoueurCourant j) {
		super(time, j);
		this.finirTimer();
		mise = 1;
		nombres = new int[] {0,00,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,
				28,29,30,31,32,33,34,35,36};
		pari = new int[] {0,1,2,3,4,5,6,7,8,9,10,11};
		numero = -1;
		
		choixExterieur = 1;
		
		typePari = 1;
		pretPari = false;
	}
	
	/********************************************************************************/
	/********************************************************************************/
	/********************************************************************************/
	/******************GERER LES INTERACTIONS CLAVIER********************************/
	/********************************************************************************/
	/********************************************************************************/
	/********************************************************************************/
	
	/********************************************************************************/
	/**************************PASSE AU JETON SUIVANT********************************/
	/********************************************************************************/
	private void suiv1() {
		if(pari[0] == 37){
			pari[0] = 0;
		}else {
			pari[0]++;
		}
	}
	
	private void suiv2() {
		if(pari[0] == 0 && pari[1] == 1) {
			pari[1] = 2;
		}else if(pari[0] == 0 && pari[1] == 2) {
			pari[1] = 3;
		}else if(pari[0] == 0 && pari[1] == 3) {
			pari[0] = 1;
		}else if(pari[0] == 1 && pari[1] == 3) {
			pari[1] = 4;
		}else if(pari[0] == 1 && pari[1] == 4) {
			pari[0] = 2;
			pari[1] = 5;
		}else if(pari[0] == 34 && pari[1] == 37){
			pari[0] = 2;
			pari[1] = 3;
		}else if(pari[0] == 36 && pari[1] == 37){
			pari[0] = 0;
			pari[1] = 1;
		}else if(pari[0] == 35 && pari[1] == 36){
			pari[0] = 3;
			pari[1] = 4;
		}else if(pari[0]==pari[1]-3){
			pari[0]++;
			pari[1]++;
		}else {
			pari[0] = pari[0] + 3;
			pari[1] = pari[1] + 3;
		}
	}

	private void suiv3() {
		if(pari[0] == 0 && pari[1] == 2 && pari[2] == 3) {
			pari[1] = 1;
		}else if(pari[0] == 0 && pari[1] == 1 && pari[2] == 3) {
			pari[0] = 1;
			pari[1] = 3;
			pari[2] = 4;
		}else if(pari[0] == 1 && pari[1] == 3 && pari[2] == 4) {
			pari[0] = 2;
			pari[1] = 3;
			pari[2] = 4;
		}else if(pari[2] == 37) {
			pari[0] = 0;
			pari[1] = 2;
			pari[2] = 3;
		}else {
			pari[0] = pari[0]+3;
			pari[1] = pari[1]+3;
			pari[2] = pari[2]+3;
		}
	}
	
	private void suiv4() {
		if(pari[3] == 37) {
			pari[0] = 2;
			pari[1] = 3;
			pari[2] = 5;
			pari[3] = 6;
		}else if(pari[1]%3!=0){
			pari[0] = pari[0]+2;
			pari[1] = pari[1]+2;
			pari[2] = pari[2]+2;
			pari[3] = pari[3]+2;
		}else {
			pari[0]++;
			pari[1]++;
			pari[2]++;
			pari[3]++;
		}
	}
	
	private void suiv6() {
		if(pari[5] == 37) {
			pari[0] = 2;
			pari[1] = 3;
			pari[2] = 4;
			pari[3] = 5;
			pari[4] = 6;
			pari[5] = 7;
		}else {
			pari[0] = pari[0]+3;
			pari[1] = pari[1]+3;
			pari[2] = pari[2]+3;
			pari[3] = pari[3]+3;
			pari[4] = pari[4]+3;
			pari[5] = pari[5]+3;
		}
	}
	
	private void suiv7() {
		if(pari[0] == 4 && pari[11] == 37) {
			pari[0] = 2;
			pari[1] = 3;
			pari[2] = 4;
			pari[3] = 5;
			pari[4] = 6;
			pari[5] = 7;
			pari[6] = 8;
			pari[7] = 9;
			pari[8] = 10;
			pari[9] = 11;
			pari[10] = 12;
			pari[11] = 13;
		}else if(pari[0] == 26 && pari[11] == 37){
			pari[0] = 2;
			pari[1] = 5;
			pari[2] = 8;
			pari[3] = 11;
			pari[4] = 14;
			pari[5] = 17;
			pari[6] = 20;
			pari[7] = 23;
			pari[8] = 26;
			pari[9] = 29;
			pari[10] = 32;
			pari[11] = 35;
		}else if(pari[0] == pari[1] - 1) {
			pari[0] = pari[0]+12;
			pari[1] = pari[1]+12;
			pari[2] = pari[2]+12;
			pari[3] = pari[3]+12;
			pari[4] = pari[4]+12;
			pari[5] = pari[5]+12;
			pari[6] = pari[6]+12;
			pari[7] = pari[7]+12;
			pari[8] = pari[8]+12;
			pari[9] = pari[9]+12;
			pari[10] = pari[10]+12;
			pari[11] = pari[11]+12;
		}else {
			pari[0]++;
			pari[1]++;
			pari[2]++;
			pari[3]++;
			pari[4]++;
			pari[5]++;
			pari[6]++;
			pari[7]++;
			pari[8]++;
			pari[9]++;
			pari[10]++;
			pari[11]++;
		}
	}
	
	private void suiv8() {
		if(choixExterieur < 6) {
			choixExterieur++;
		}else {
			choixExterieur = 1;
		}
	}
	
	/********************************************************************************/
	/**************************PASSE AU JETON PRECEDENT******************************/
	/********************************************************************************/
	
	private void prec1() {
		if(pari[0] == 0) {
			pari[0] = 37;
		}else {
			pari[0]--;
		}
	}
	
	private void prec2() {
		if(pari[0] == 0 && pari[1] == 1) {
			pari[0] = 36;
			pari[1] = 37;
		}else if(pari[0] == 2 && pari[1] == 5) {
			pari[0] = 1;
			pari[1] = 4;
		}else if(pari[0] == 1 && pari[1] == 4) {
			pari[1] = 3;
		}else if(pari[0] == 1 && pari[1] == 3) {
			pari[0] = 0;
		}else if(pari[0] == 0 && pari[1] == 3) {
			pari[1] = 2;
		}else if(pari[0] == 0 && pari[1] == 2) {
			pari[1] = 1;
		}else if(pari[0] == 2 && pari[1] == 3) {
			pari[0] = 34;
			pari[1] = 37;
		}else if(pari[0] == 3 && pari[1] == 4) {
			pari[0] = 35;
			pari[1] = 36;
		}else if(pari[0]==pari[1]-3){
			pari[0]--;
			pari[1]--;
		}else {
			pari[0] = pari[0] - 3;
			pari[1] = pari[1] - 3;
		}
	}

	private void prec3() {
		if(pari[0] == 1 && pari[1] == 3 && pari[2] == 4) {
			pari[0] = 0;
			pari[1] = 1;
			pari[2] = 3;
		}else if(pari[0] == 0 && pari[1] == 1 && pari[2] == 3) {
			pari[0] = 0;
			pari[1] = 2;
			pari[2] = 3;
		}else if(pari[0] == 2 && pari[1] == 3 && pari[2] == 4) {
			pari[0] = 1;
			pari[1] = 3;
			pari[2] = 4;
		}else if(pari[0] == 0 && pari[1] == 2 && pari[2] == 3) {
			pari[0] = 35;
			pari[1] = 36;
			pari[2] = 37;
		}else {
			pari[0] = pari[0]-3;
			pari[1] = pari[1]-3;
			pari[2] = pari[2]-3;
		}
	}
	
	private void prec4() {
		if(pari[0] == 2) {
			pari[0] = 33;
			pari[1] = 34;
			pari[2] = 36;
			pari[3] = 37;
		}else if(pari[1]%3==0){
			pari[0] = pari[0]-2;
			pari[1] = pari[1]-2;
			pari[2] = pari[2]-2;
			pari[3] = pari[3]-2;
		}else {
			pari[0]--;
			pari[1]--;
			pari[2]--;
			pari[3]--;
		}
	}
	
	private void prec6() {
		if(pari[0] == 2) {
			pari[0] = 32;
			pari[1] = 33;
			pari[2] = 34;
			pari[3] = 35;
			pari[4] = 36;
			pari[5] = 37;
		}else {
			pari[0] = pari[0]-3;
			pari[1] = pari[1]-3;
			pari[2] = pari[2]-3;
			pari[3] = pari[3]-3;
			pari[4] = pari[4]-3;
			pari[5] = pari[5]-3;
		}
	}
	
	private void prec7() {
		if(pari[0] == 2 && pari[11] == 35) {
			pari[0] = 26;
			pari[1] = 27;
			pari[2] = 28;
			pari[3] = 29;
			pari[4] = 30;
			pari[5] = 31;
			pari[6] = 32;
			pari[7] = 33;
			pari[8] = 34;
			pari[9] = 35;
			pari[10] = 36;
			pari[11] = 37;
		}else if(pari[0] == 2 && pari[11] == 13){
			pari[0] = 4;
			pari[1] = 7;
			pari[2] = 10;
			pari[3] = 13;
			pari[4] = 16;
			pari[5] = 19;
			pari[6] = 22;
			pari[7] = 25;
			pari[8] = 28;
			pari[9] = 31;
			pari[10] = 34;
			pari[11] = 37;
		}else if(pari[0] == pari[1] - 1) {
			pari[0] = pari[0]-12;
			pari[1] = pari[1]-12;
			pari[2] = pari[2]-12;
			pari[3] = pari[3]-12;
			pari[4] = pari[4]-12;
			pari[5] = pari[5]-12;
			pari[6] = pari[6]-12;
			pari[7] = pari[7]-12;
			pari[8] = pari[8]-12;
			pari[9] = pari[9]-12;
			pari[10] = pari[10]-12;
			pari[11] = pari[11]-12;
		}else {
			pari[0]--;
			pari[1]--;
			pari[2]--;
			pari[3]--;
			pari[4]--;
			pari[5]--;
			pari[6]--;
			pari[7]--;
			pari[8]--;
			pari[9]--;
			pari[10]--;
			pari[11]--;
		}
	}
	
	private void prec8() {
		if(choixExterieur > 1) {
			choixExterieur--;
		}else {
			choixExterieur = 6;
		}
	}
	
	/**
	 * permet de passer au jeton suivant en fonction du type de mise(nbr de jeton)
	 */
	public void suivPari() {
		switch(typePari) {
			case 1 : suiv1();
					 break;
			case 2 : suiv2();
					 break;
			case 3 : suiv3();
					 break;
			case 4 : suiv4();
					 break;
			case 5 : break;
			case 6 : suiv6();
					 break;
			case 7 : suiv7();
					 break;
			case 8 : suiv8();
					 break;
		}
	}
	
	/**
	 * permet de passer au jeton precedent en fonction du type de mise(nbr de jeton)
	 */
	public void precPari() {
		switch(typePari) {
			case 1 : prec1();
					 break;
			case 2 : prec2();
					 break;
			case 3 : prec3();
					 break;
			case 4 : prec4();
					 break;
			case 5 : break;
			case 6 : prec6();
					 break;
			case 7 : prec7();
					 break;
			case 8 : prec8();
					 break;
		}
	}
	
	public int getChoixExt() {
		return choixExterieur;
	}
	
	/**
	 * 
	 * @return les jetons selectionn� avec un tableau
	 */
	public double[] getPari() {
		double[] jeton = new double[getTypePari()];
		for(int i=0; i<jeton.length; i++) {
			if(pari[i]==1) {
				jeton[i] = 0.5;
			}else {
				jeton[i] = nombres[pari[i]];
			}
		}
		return jeton;
	}
	
	/**
	 * 
	 * @return les jetons selectionn� avec un string
	 */
	public String getStringPari() {
		String res = "";
		if(typePari==8) {
			switch(choixExterieur) {
				case 1: return "1to18";
				case 2: return "pair";
				case 3: return "rouge";
				case 4: return "noir";
				case 5: return "impaire";
				case 6: return "19to36";
			}
		}
		int tmp = typePari;
		if(tmp == 7) tmp = 12;
		for(int i = 0; i<tmp; i++) {
			if(i==6)res+="\n";
			if(pari[i]==1) {
				res += " 00";
			}else {
				res += " "+nombres[pari[i]];
			}
		}
		return res;
	}
	
	/**
	 * 
	 * @return le nombre de jeton selectionn�
	 */
	public int getTypePari() {
		if(typePari == 7) return 12;
		if(typePari == 8) return 24;
		return typePari;
	}


	public void setTypePari(int typePari) {
		if(this.typePari + typePari < 1) {
			this.typePari = 8;
		}else if(this.typePari + typePari > 8) {
			this.typePari = 1;
		}else {
			this.typePari += typePari;
		}
		pari = new int[] {0,1,2,3,4,5,6,7,8,9,10,11};
		//g�rer les jetons de base pour les diff�rents types de pari
		if(this.typePari == 3) {
			pari[1] = 2;
			pari[2] = 3;
		}
		if(this.typePari == 4) {
			pari[0] = 2;
			pari[1] = 3;
			pari[2] = 5;
			pari[3] = 6;
		}
		if(this.typePari == 6) {
			pari[0] = 2;
			pari[1] = 3;
			pari[2] = 4;
			pari[3] = 5;
			pari[4] = 6;
			pari[5] = 7;
		}
		if(this.typePari == 7) {
			pari[0] = 2;
			pari[1] = 3;
			pari[2] = 4;
			pari[3] = 5;
			pari[4] = 6;
			pari[5] = 7;
			pari[6] = 8;
			pari[7] = 9;
			pari[8] = 10;
			pari[9] = 11;
			pari[10] = 12;
			pari[11] = 13;
		}
	}


	public boolean isPretPari() {
		return pretPari;
	}


	public void setPretPari(boolean pretPari) {
		this.pretPari = pretPari;
	}
	
	public double getNumero() {
		return numero;
	}


	@Override
	/**
	 * 
	 * retourne le nombre de jeton gagn�
	 */
	public int play() {
		// TODO modifier les jetons du joueur
		this.Jcourant.setJeton(-this.mise); //pour enlever la mise de jeton
		hasard();
		this.setPlay(true);
		if(this.Jcourant.isObjet(JoueurCourant.abonnement)) {
			return (int) (gagne()*1.1);//donne 10% de plus si abonnement
		}else {
			return gagne();//donne les jetons du joueur
		}
	}
	
	/**
	 * donne le resultat en fonction des possibilite et de la chance de les avoirs
	 * et de la chance du joueur
	 * 
	 * si 00 alors numero = 0.5
	 * 
	 * @param joueur le joueur qui joue
	 */
	private void hasard() {
		//TODO
		numero = 0;
	}
	
	/**
	 * v�rifie si le r�sultat est gagnant
	 * 
	 * @return les jetons gagn� 0 sinon
	 * 
	 */
	private int gagne() {
		//TODO
		return 0;
	}
	
	public JoueurCourant getJcourant() {
		return Jcourant;
	}

	public void setJcourant(JoueurCourant jcourant) {
		Jcourant = jcourant;
	}

}
