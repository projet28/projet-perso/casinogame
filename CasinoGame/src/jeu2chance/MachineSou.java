package jeu2chance;

import joueur.JoueurCourant;

public class MachineSou extends JeuDeChance{
	
	//attributs
	private String[] possibilite;	//la liste des images (en chemin)
	private int[][] machine;	//l'etat de la machine
	
	//pour g�rer les diff�rents choix de pari possible
	public static final int simple = 0, haut = 1, bas = 2, diagonal1 = 3, diagonal2 = 4;
	private boolean[] choix;
	private int choixCourant;

	/*
	 * le but du jeu est juste de miser 1 jeton
	 * si 3 images sont align� sur la rang� du milieu alors le joueur gagne
	 * de 1 jeton � 1000 jetons
	 * 
	 * on peut ameliorer le jeu en faisant des mises sur plusieurs lignes et diagonalle
	 *  et la mise est de 1 jeton par ligne
	 */
	
	
	
	
	public MachineSou(long time, JoueurCourant j) {
		super(time, j);
		this.finirTimer();
		//initialisation des attributs
		possibilite = new String[] {"M1","M2","M3","M4","M5","M6","M7","M8","M9"};
		machine = new int[][]  {{0, 1, 2},
								{3, 4, 5},
								{6, 7, 8},
								};//TODO debut al�atoire
		choix = new boolean[]{true, false, false, false, false};
		choixCourant = 0;
		this.mise = 1;
	}
	
	/**
	 * 
	 * @return le choix choisi
	 */
	public String getChoix() {
		String res = "";
		for(int i = 0; i<choix.length; i++) {
			if(!choix[i]) {
				return res;
			}
			switch(i) {
				case simple : res = "simple"; 
							  break;
				case haut : res = "double";
							break;
				case bas : res = "triple";
						   break;
				case diagonal1 : res = "1 diagonale";
								 break;
				case diagonal2 : res = "2 diagonale";
								 break;
			}
		}
		return res;
	}
	

	private void setChoix(int choisi) {
		switch(choisi) {
			case simple : choix[simple] = !choix[simple];
						  break;
			case haut : choix[haut] = !choix[haut];
						break;
			case bas : choix[bas] = !choix[bas];
					   break;
			case diagonal1 : choix[diagonal1] = !choix[diagonal1];
							 break;
			case diagonal2 : choix[diagonal2] = !choix[diagonal2];
							 break;
		}
	}
	
	/**
	 * augmente le nombre de choix
	 */
	public void addChoix() {
		if(choixCourant<4) {
			choixCourant++;
			setChoix(choixCourant);
		}else {
			choixCourant = 0;
			choix = new boolean[]{true, false, false, false, false};
		}
		switch(choixCourant) {
			case simple : this.mise = 1; 
						  break;
			case haut : this.mise = 2;
						break;
			case bas : this.mise = 3;
					   break;
			case diagonal1 : this.mise = 5;
							 break;
			case diagonal2 : this.mise = 10;
							 break;
		}
	}
	
	/**
	 * diminue le nombre de choix
	 */
	public void suppChoix() {
		if(choixCourant>0) {
			setChoix(choixCourant);
			choixCourant--;
		}else {
			choixCourant = 4;
			choix = new boolean[]{true, true, true, true, true};
		}
		switch(choixCourant) {
			case simple : this.mise = 1; 
						  break;
			case haut : this.mise = 2;
						break;
			case bas : this.mise = 3;
					   break;
			case diagonal1 : this.mise = 5;
							 break;
			case diagonal2 : this.mise = 10;
							 break;
		}
	}
	
	public String[][] getImage() {
		return new String[][] {{possibilite[machine[0][0]], possibilite[machine[0][1]], possibilite[machine[0][2]]},
							   {possibilite[machine[1][0]], possibilite[machine[1][1]], possibilite[machine[1][2]]},
							   {possibilite[machine[2][0]], possibilite[machine[2][1]], possibilite[machine[2][2]]},
							  };
	}

	@Override
	/**
	 * 
	 * retourne le nombre de jeton gagn�
	 */
	public int play() {
		// TODO modifier les jetons du joueur
		this.Jcourant.setJeton(-this.mise); //pour enlever 1 jeton
		hasard();
		this.setPlay(true);
		if(this.Jcourant.isObjet(JoueurCourant.abonnement)) {
			return (int) (gagne()*1.1);//donne 10% de plus si abonnement
		}else {
			return gagne();//donne les jetons du joueur
		}
	}
	
	
	/**
	 * donne le resultat en fonction des possibilite et de la chance de les avoirs
	 * et de la chance du joueur
	 * 
	 * @param joueur le joueur qui joue
	 */
	private void hasard() {
		this.Jcourant.getChance();// v�rifier la chance du joueur 0 ultra chanceux 1 chanceux 2 normal 3 malchance
		machine = new int[][] {{0, 0, 0},
							   {0, 0, 0},
							   {0, 0, 0},
							   };
	}
	
	/**
	 * v�rifie si le r�sultat est gagnant
	 * et retourne l'argent obtenu
	 * 
	 * gagnant que si au moins 1 triple
	 * 0: 10 jetons
	 * 1: 20 jetons
	 * 2: 50
	 * 3: 100
	 * 4: 500
	 * 6: 1000
	 * 7: 5000
	 * 8: 50 000
	 * 
	 * @return 0 si perdu
	 * 
	 */
	private int gagne() {
		//TODO v�rifier la ligne du milieu machine[1][i];
		
		return 0;
	}

	public JoueurCourant getJcourant() {
		return Jcourant;
	}

	public void setJcourant(JoueurCourant jcourant) {
		Jcourant = jcourant;
	}

}
