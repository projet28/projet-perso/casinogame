package jeu2chance;

import casino.Jeu;
import joueur.JoueurCourant;

abstract class JeuDeChance extends Jeu{
	
	JoueurCourant Jcourant; //le joueur qui joue

	public JeuDeChance(long time, JoueurCourant j) {
		super(time);
		Jcourant = j;
	}

	@Override
	abstract public int play();
}
