package jeu2chance;

import joueur.JoueurCourant;

public class Pari extends JeuDeChance{

	//attributs
	static public final int simple = 0, doubl = 1, triple = 2, quarte = 3, quinte = 4;

	private int[] chevaux;	//le numero de chaque chevaux
	private double[] cote; //la cote de chaque cheval
	private int[] pari;	//le cheval sur qui le joueur pari
	private int typePari; //le type du pari
	private boolean pretPari; //savoir si le type de pari est fini
	private int[] gagnant; //le resultat de la course

	/*
	 * le but du jeu est juste de miser un nombre de jeton > 0
	 * si le pari est gagnant il double sa mise
	 * 
	 * on peut ameliorer le jeu en faisant des mises en fonction de la c�te des 
	 * diff�rent chevaux
	 */
		
		
		
		
	public Pari(long time, JoueurCourant j) {
		super(time, j);
		this.finirTimer();
		mise = 1;
		//initialisation des attributs
		setChevaux();
		typePari = simple;
		pretPari = false;
	}
	
	private void setChevaux() {
		chevaux = new int[]  {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}; //TODO g�n�rer le nombre al�atoirement entre 5 et 10
		cote = new double[] {1.1, 2, 5, 10, 7, 2.5, 4, 9.1, 1.4, 4}; //TODO g�n�rer la cote de chaque cheval al�atoirement (1.1 min et 10 max)
	}
	
	public int[] getChevaux() {
		return chevaux;
	}
	
	public double getCote(int cheval) {
		return cote[cheval];
	}
	
	/**
	 * style de pari suiv
	 */
	public void suivPari() {
		if(chevaux.length > 8) {
			this.typePari = (typePari+1) % 5;
		}else if(chevaux.length > 6) {
			this.typePari = (typePari+1) % 4;
		}else {
			this.typePari = (typePari+1) % 3;
		}
	}
		
	/**
	 * style de pari pr�cedent
	 */
	public void precPari() {
		this.typePari = (typePari-1);
		if(this.typePari < 0) {
			if(chevaux.length > 8) {
				this.typePari = 4;
			}else if(chevaux.length > 6) {
				this.typePari = 3;
			}else {
				this.typePari = 2;
			}
		}
	}
	
	public int nbrChevaux() {
		return chevaux.length;
	}
	
	public int getTypePari() {
		return typePari;
	}
	
	public boolean isPretPari() {
		return pretPari;
	}


	public void setPretPari(boolean pretPari) {
		this.pretPari = pretPari;
		pari = new int[this.typePari+1];
		for(int i=0; i<pari.length; i++) {
			pari[i] = -1;
		}
	}
		
	/**
	 * pari sur le cheval
	 */
	public void setPari(int cheval) {
		int i = 0;
		while(i<pari.length && pari[i] != -1) i++;
		if(pari.length > i) {
			boolean unique = true;
			for(int j=0; j<=i; j++) {
				if(pari[j] == cheval) unique = false;
			}
			if(unique) {
				pari[i] = cheval;
			}
		}
	}
	
	public String getPari() {
		String res = "";
		for(int i=0; i<pari.length; i++) {
			if(pari[i] != -1) res += pari[i];
		}
		return res;
	}
	
	public boolean pariComplet() {
		for(int i=0; i<pari.length; i++) {
			if(pari[i] == -1) return false;
		}
		return true;
	}


	public int[] getResultat() {
		return gagnant;
	}

	@Override
	/**
	 * 
	 * retourne le nombre de jeton gagn�
	 */
	public int play() {
		// TODO modifier les jetons du joueur
		this.Jcourant.setJeton(-this.mise); //pour enlever la mise de jeton
		hasard();
		this.setPlay(true);
		int jeton;
		if(this.Jcourant.isObjet(JoueurCourant.abonnement)) {
			jeton = (int) ((gagne())*1.1);//donne 10% de plus si abonnement
		}else {
			jeton = gagne();//donne les jetons du joueur
		}
		setChevaux();
		typePari = simple;
		setPretPari(false);
		return jeton;
	}


	/**
	 * donne le resultat en fonction des chevaux et de la chance de les avoirs
	 * et de la chance du joueur
	 * 
	 * un cheval avec une meilleur cote a plus de chance de gagner (1.1 quasi sur de finir 1er ou 2e et 10 quasi sur dernier) 
	 * 
	 * @param joueur le joueur qui joue
	 */
	private void hasard() {
		this.Jcourant.getChance();// v�rifier 0 ultra chanceux la chance du joueur 1 chanceux 2 normal 3 malchance
		//gagnant = new int[chevaux.length];
		gagnant = new int[] {2,5,1,7,0,3,4,6,9,8};
	}

	/**
	 * v�rifie si le r�sultat est gagnant et return les jetons gagn�
	 * 
	 * gagnant que si le pari est 100% bon
	 * 
	 * pari sur 1 cheval rend le nombre de jeton * la cote du cheval
	 * pari sur 2 chevaux rend le nombre de jeton * (la cote du cheval 1 + la cote du cheval 2)
	 * pari sur 3 chevaux rend le nombre de jeton * (la cote 1 + la cote 2 + la cote 3) * 1.5
	 * pari sur 4 chevaux rend le nombre de jeton * (la cote 1 * la cote 2 + la cote 3 + la cote 4) * 2
	 * pari sur 5 chevaux rend le nombre de jeton * (la cote 1 * la cote 2 * la cote 3 + la cote 4 + la cote 5) * 3
	 * 
	 * @return 0 si perdu sinon les jetons gagn�
	 * 
	 */
	private int gagne() {
		//TODO
		return 0;
	}

	public JoueurCourant getJcourant() {
		return Jcourant;
	}

	public void setJcourant(JoueurCourant jcourant) {
		Jcourant = jcourant;
	}

}
