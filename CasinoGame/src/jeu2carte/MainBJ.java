package jeu2carte;

import java.util.ArrayList;
import java.util.List;

public class MainBJ implements Comparable<MainBJ>{

	private List<Carte> main; //la main de BJ
	private MainBJ split; //permet de g�rer un jeu split
	private boolean spliter; //savoir si on est dans le split
	
	public MainBJ() {
		main = new ArrayList<Carte>();
		split = null;
	}

	public List<Carte> getMain() {
		return main;
	}

	public void setMain(List<Carte> main) {
		this.main = main;
	}
	
	public int getSize() {
		return main.size();
	}
	
	public MainBJ getSplit() {
		return split;
	}
	
	public boolean canSplit() {
		if(!spliter && split == null && getSize()==2) {
			if(main.get(0).compareTo(main.get(1)) == 0 ||
					(main.get(0).getNumero() >= 10 && main.get(1).getNumero() >= 10)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * s�pare la main si possible et permet au joueur de jouer de main en m�me temps (jusq'� 3)
	 * 
	 * @return -1 si impossible de split 0 sinon
	 */
	public int split() {
		if(canSplit()) {
			split = new MainBJ();
			split.spliter=true;
			split.ajouter(main.remove(1));
			return 0;
		}
		return -1;
	}
	
	/**
	 * ajoute une carte pour le joueur 
	 * @param c la carte � ajouter
	 */
	public void ajouter(Carte c) {
		main.add(c);
	}
	
	/**
	 * retourne la main du joueur et vide toutes ses cartes
	 * @return retourne la main du joueur
	 */
	public List<Carte> reprendreMain() {
		List<Carte> l = main;
		main.clear();
		return l;
	}
	
	/**
	 * 
	 * @param m la list de carte a test�
	 * @return la valeur de la main (la somme des cartes, 0 si BJ)
	 */
	public int value(List<Carte> m) {
		int val=0;
		int cptAs = 0;
		
		for(int i=0; i<m.size(); i++) {
			if(m.get(i).getNumero()>10) {
				val += 10;
			}else {
				if(m.get(i).getNumero()==1) cptAs++;
				val += m.get(i).getNumero();
			}
		}
		if(val<21 && cptAs>0) {
			for(int i=0;i<=cptAs;i++) {
				if(val+10<=21) {
					val+=10;
				}
			}
		}
		
		if(m.size()==2 && val==21 && m.get(0).getNumero() != 10 && m.get(1).getNumero() != 10 && split == null && !spliter) {
			return 0;
		}
		return val;
	}
	
	@Override
	public int compareTo(MainBJ m2) {
		return compareTo(m2.getMain());
	}
	
	private int compareTo(List<Carte> m2) {
		int val1 = value(this.main);
		int val2 = value(m2);
		
		if(val1 > 21 || val2 > 21) {
			if(val1 > 21 && val2 < 22) return -1;
			if(val1 < 22 && val2 > 21) return 1;
			return 0;
		}
		if(val1 == 0 || val2 == 0) {
			if(val1 != 0 && val2 == 0) return -1;
			if(val1 == 0 && val2 != 0) return 1;
			return 0;
		}
		return val1-val2;
	}
	
	private String s ="";
	@Override
	public String toString() {
		
		main.forEach(c ->{ s += c.toString() + "; ";});
		return s;
	}
}
