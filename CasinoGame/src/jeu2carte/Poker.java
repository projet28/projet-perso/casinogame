package jeu2carte;

import java.util.ArrayList;
import java.util.List;

import joueur.IA;
import joueur.Joueur;
import joueur.JoueurCourant;
import utilisateur.Action;
import utilisateur.SoundEffect;

public class Poker extends JeuDeCarte{
	
	/*
	 * https://www.pokerstars.fr/poker/jeux/texas-holdem/
	 * 
	 */
	
	private Action act; // r�cupere les actions clavier
	private SoundEffect son; //pour gerer les sons
	
	private List<Carte> jeu; //les cartes du milieu
	private int jetonsJoueurCourant; //g�rer les jetons du joueur courant pour la partie
	private int blind; //somme de la blind
	private int miseCourant; //mise du moment
	private int miseTotal; //la mise final � la fin du tour et � partager
	private int dealer; //commence au joueur 0 (par exemple) puis 1, 2 ... puis encore 0;
	private int tourJoueur; //a quelle joueur c'est le tour de miser
	private int cptTour; //compter les tours (4)
	private int nbRelance; //pour savoir le nb de jeton si on relance
	
	private boolean relance; //pour savoir si il relance
	private boolean quitte; //si le joueur veut quitter la table

	public Poker(long time, Action act, SoundEffect son) {
		super(time);
		this.act = act;
		this.son = son;
		
		Joueur j1 = new IA("flavient", 100, "images/1", 0, son);
		Joueur j2 = new IA("flavient", 100, "images/1", 0, son);
		Joueur j3 = new IA("flavient", 100, "images/1", 0, son);
		Joueur j4 = new IA("flavient", 100, "images/1", 0, son);
		joueurs.add(j1);
		joueurs.add(j2);
		joueurs.add(j3);
		joueurs.add(j4);//TODO d�j� le joueur courant � 0, cr�� un nombre de joueur al�atoirement entre 3 et 5
		dealer = 0; //TODO g�n�rer al�atoirement
		cptTour = 1;
		jeu = new ArrayList<Carte>(5); 		
		
		relance = false;
		quitte = false;
		this.mise = 100;
	}
	
	public List<Carte> getJeu(){
		return jeu;
	}
	
	public int getMiseTotal() {
		return miseTotal;
	}
	
	public int getMiseCourante() {
		return miseCourant;
	}
	
	private void suivMise() {
		if(mise == 10000) {
			mise = 100;
		}else {
			mise = mise * 10;
		}
	}
	
	private void precMise() {
		if(mise == 100) {
			mise = 10000;
		}else {
			mise = mise / 10;
		}
	}
	
	@Override
	/**
	 * si 1 mise suivant -1 precedent
	 */
	public void setMise(int mise) {
		if(mise==1) {
			suivMise();
		}
		if(mise==-1) {
			precMise();
		}
	}
	
	public int getMise() {
		return mise;
	}
	
	public int getJetonJoueurCourant() {
		return jetonsJoueurCourant;
	}
	
	public boolean isRelance() {
		return relance;
	}
	
	public int getNbRelance() {
		return nbRelance;
	}

	/**
	 * on commence a relancer � partir de la mise 
	 * 
	 * @param nbRelance si 1 on augmente -1 on diminue
	 */
	public void setNbRelance(int nbRelance) {
		if(nbRelance == 1) {
			if(this.nbRelance == this.jetonsJoueurCourant) {
				this.nbRelance = miseCourant+1;
			}else {
				this.nbRelance++;
			}
		}
		if(nbRelance == -1) {
			if(this.nbRelance == miseCourant+1) {
				this.nbRelance = this.jetonsJoueurCourant;
			}else {
				this.nbRelance--;
			}
		}
	}

	public int getDealer() {
		return dealer;
	}

	public boolean isAbandonne() {
		return quitte;
	}


	public void setAbandonne() {
		this.quitte = true;
	}

	@Override
	public int play() {
		this.blind = this.mise/10;//la blind est 10 fois moins la mise (petit blind 2 fois moins que la grosse)
		getJoueur().setJeton(-this.mise);//enlever les jetons miser
		jetonsJoueurCourant=this.mise;//jetons miser
		this.finirTimer();//met fin au timer pour g�rer les actions du joueur
		while(!fin() && !quitte) {
			if(jeu(cptTour) == -1) return -1;
			cptTour++;
		}
		this.setPlay(false);
		getJoueur().getMainP().reprendreMain();
		if(this.getJoueur().isObjet(JoueurCourant.abonnement) && jetonsJoueurCourant != this.mise) {
			getJoueur().setJeton((int) (jetonsJoueurCourant*1.1));//donne 10% de plus si abonnement
		}else {
			getJoueur().setJeton(jetonsJoueurCourant);//donne les jetons du joueur
		}
		if(gagne()) return 1;
		if(!gagne()) return 0;
		return -1;
	}
	
	/**
	 * utilise le clavier: r: relance
	 * 					   s: suivre
	 * 					   c: coucher
	 * 					   t: tapis
	 * 
	 * @param mise la mise courante si 0 alors encore aucune mise
	 * @param blind la blind mise de d�part (mise minimum)
	 * @return -1 si se couche 
	 * 			la mise si il suit
	 * 			>mise si il relance ou il tapis
	 * 			0 si la mise �tait de zero aussi alors il check
	 * 			-11 si erreur
	 */
	private int actionJoueur(int mise, int blind) {
		char action = ' ';
		act.setKey(' ');
		this.resetTime();
		while(!this.isTimeFinished() && (action == 'r' || action == ' ')) {
			while(!this.isTimeFinished() && (action = act.actionPoker()) == ' ');
			switch(action) {
				case 'r' : this.nbRelance = miseCourant; relance = true; break;//TODO mettre relance a true puis utiliser this.nbRelance quand il rappuie sur r alors il relance
				case 's' : break;//TODO utiliser son.getSonChip1().play(); pour mettre du son en plus
				case 'c' : break;//TODO
				case 't' : break;//TODO
				case 'e' : setAbandonne(); break;
				//chaque case doit retourner ce qu'il faut
			}
			
		}
		this.finirTimer();
		relance = false;
		return -11;
	}
	
	/**
	 * la fonction du jeu principal
	 * 
	 * mise du jeu: grosseBlind puis petiteBlind le 1er tour puis les joueurs suivent, relance ou se couche
	 * 
	 * @return -1 si erreur
	 */
	private int jeu(int tour) {
		switch (tour) {
			case 1: distribuerJeu(1);//pre-flop
					tourJoueur = (dealer+1)%joueurs.size();//le joueur � gauche commence
					mise(true);
					break;
			case 2: distribuerJeu(2);//flop
					tourJoueur = (dealer+1)%joueurs.size();//le joueur � gauche commence
					mise(false);
					break;
			case 3: distribuerJeu(3);//turn
					tourJoueur = (dealer+1)%joueurs.size();//le joueur � gauche commence
					mise(false);
					break;
			case 4: distribuerJeu(4);//rivi�re
					tourJoueur = (dealer+1)%joueurs.size();//le joueur � gauche commence
					mise(false);
					break;
			case 5: tourJoueur = (dealer+1)%joueurs.size();//le joueur � gauche commence
					mise(false);
					recommence();//devoilement
					break;
			default: System.out.println("impossible");
					return -1;
		}
		miseTotal+=miseCourant;
		miseCourant=0;
		return 0;
	}
	
	private void mise(boolean debut) {
		boolean relance=true;
		if(debut) {
			tourJoueur = (tourJoueur+1)%joueurs.size();
			tourJoueur = (tourJoueur+1)%joueurs.size();//les 2 blinds ne joue pas en 1er
			//TODO si debut les blinds doivent etre mis� sinon on commence � 0
		}
		int min = joueurs.size();//le nombre de joueur qui doit jouer
		while(relance && !quitte) { //il faut miser jusqu'� faire un tour complet avec tous le monde qui suit ou se couche
			if(tourJoueur==0) {
				actionJoueur(miseCourant, blind); //si -11 alors il a pas jou� donc action par d�fault (suivre ou se coucher)
			}else {
				((IA) joueurs.get(tourJoueur)).actionPoker(miseCourant,blind,cptTour);
			}
			tourJoueur = (tourJoueur+1)%joueurs.size(); //TODO dans se style
			min--;
			if(min==0) relance = false;
		}
	}
	
	/**
	 * la partie est fini si il reste que un joueur donc tous les autres ont plus de jetons
	 * 
	 * @return true si la partie est fini false sinon
	 */
	private boolean fin() {
		return false; //TODO
	}
	
	/**
	 * la partie est gagne si il reste que le joueur courant
	 * 
	 * @return true si la partie est gagne false sinon
	 */
	private boolean gagne() {
		return false;	//TODO
	}
	
	/**
	 * 
	 * donne 2 cartes � chaque joueur
	 * utiliser 
	 * 
	 */
	private void distribuerMain() {
		joueurs.forEach(j -> {j.getMainP().ajouter(paquet.piocher(), true); if(son.isBruit()) son.getSonCarte1().play();});
		joueurs.forEach(j -> {j.getMainP().ajouter(paquet.piocher(), true); if(son.isBruit()) son.getSonCarte2().play();});
		//TODO j'ai d�j� fais en fait
	}
	
	/**
	 * @param tour le tour actuelle pour savoir combien de cartes d�voiler
	 * 
	 * distribu les cartes du jeu: distribuerMain() le 1er tour
	 * 							   3 le 2e tour
	 * 							   1 le 3e tour
	 * 							   1 le 4e tour
	 * 
	 */
	private void distribuerJeu(int tour) {
		//attends(2); //TODO pour faire attendre le jeu et le rendre plus r�aliste
		switch(tour) {
			case 1:
				distribuerMain();
				break;
			case 2: jeu.add(paquet.piocher()); //ajoute la carte aux jeu TODO
					if(son.isBruit())son.getSonCarte1().play(); //TODO pour mettre du son a chaque fois qu'il y a une carte
					joueurs.get(0).getMainP().ajouter(jeu.get(0), false); //puis ajouter aux joueurs
					jeu.add(paquet.piocher()); //ajoute la carte aux jeu TODO
					if(son.isBruit()) son.getSonCarte1().play();
					joueurs.get(0).getMainP().ajouter(jeu.get(1), false); //puis ajouter aux joueurs
					jeu.add(paquet.piocher()); //ajoute la carte aux jeu TODO
					if(son.isBruit()) son.getSonCarte2().play();
					joueurs.get(0).getMainP().ajouter(jeu.get(2), false); //puis ajouter aux joueurs
				break;
			case 3: //TODO
				jeu.add(paquet.piocher()); //ajoute la carte aux jeu TODO
				if(son.isBruit()) son.getSonCarte2().play();
				joueurs.get(0).getMainP().ajouter(jeu.get(3), false); //puis ajouter aux joueurs
				break;
			case 4: //TODO
				jeu.add(paquet.piocher()); //ajoute la carte aux jeu TODO
				if(son.isBruit()) son.getSonCarte2().play();
				joueurs.get(0).getMainP().ajouter(jeu.get(4), false); //puis ajouter aux joueurs
				break;
			default:
				System.out.println("impossible");
		}
	}
	
	
	/**
	 * 
	 * reprends les cartes distribue les jetons mis� et pr�pare la nouvelle partie
	 * 
	 */
	private void recommence() {
		gagnant(); //utiliser ca pour savoir qui � gagner et qui prend l'argent
		paquet.completer();
		joueurs.forEach(j -> {j.getMainP().reprendreMain(); if(son.isBruit()) son.getSonCarte1().play();});
		jeu.clear();
		paquet.melanger();
		if(son.isBruit()) son.getSonMelangeCarte().play();
		this.attends(3);
		dealer = (dealer + 1) % joueurs.size();
		miseTotal = 0; // le faire apr�s avoir donn� les jetons
		miseCourant = 0;
		cptTour = 0;
		//TODO jetons et verifier si quelqu'un n'a plus de jetons et part
	}
	
	/**
	 * indique le joueur gagnant en fonction de sa main
	 * 
	 * @return l'id du joueur dans la liste joueurs
	 */
	private int gagnant() {
		//TODO utiliser compareTo de MainPoker
		return 0;
	}

}
