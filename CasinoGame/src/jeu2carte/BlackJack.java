package jeu2carte;

import joueur.IA;
import joueur.Joueur;
import joueur.JoueurCourant;
import utilisateur.Action;
import utilisateur.SoundEffect;

public class BlackJack extends JeuDeCarte{
	
	/*
	 * https://www.guide-blackjack.com/Regles-du-black-jack.html
	 * 
	 * l'assurance a rajouter � la fin TODO
	 */
	
	private Action act; // r�cupere les actions clavier
	private SoundEffect son; //gerer les son
	
	private int jetonsJoueurCourant; //g�rer les jetons du joueur courant pour la partie
	
	private boolean miseAct;//pour savoir si l'action de miser ou jouer
	private boolean croupier;//pour savoir si c'est au tour du croupier
	
	private boolean quitte; //si le joueur veut quitter la table

	public BlackJack(long time, Action act, SoundEffect son) {
		super(time);
		this.act = act;
		this.son = son;
		Joueur croupier = new IA("croupier", -1, "images/1", 0, son);
		Joueur j1 = new IA("igor", 100, "images/1", 0, son);
		Joueur j2 = new IA("igor", 100, "images/1", 0, son);
		Joueur j3 = new IA("igor", 100, "images/1", 0, son);
		Joueur j4 = new IA("igor", 100, "images/1", 0, son);
		joueurs.add(j1);
		joueurs.add(j2);
		joueurs.add(j3);
		joueurs.add(j4);
		joueurs.add(croupier);//TODO d�j� le joueur courant � 0, et croupier en dernier, cr�� un nombre de joueur al�atoirement entre 1 et 5
		
		miseAct = false;
		quitte = false;
		
		this.mise = 100;
	}
	
	public void suivMise() {
		if(mise == 10000) {
			mise = 100;
		}else {
			mise = mise * 10;
		}
	}
	
	public void precMise() {
		if(mise == 100) {
			mise = 10000;
		}else {
			mise = mise / 10;
		}
	}
	
	
	public boolean isMiseAct() {
		return miseAct;
	}
	
	public boolean isCroupier() {
		return croupier;
	}
	
	public int getJetonJoueurCourant() {
		return jetonsJoueurCourant;
	}


	public boolean isAbandonne() {
		return quitte;
	}


	public void setAbandonne() {
		this.quitte = true;
	}
	

	@Override
	public int play() {
		getJoueur().setJeton(-this.mise);//enlever les jetons miser
		jetonsJoueurCourant= this.mise;//jetons du joueur
		this.mise = 1;
		while(!fin() && !quitte) {
			if(jeu()==-1) return -1;
		}
		this.setPlay(false);
		getJoueur().getMainB().reprendreMain();
		if(this.getJoueur().isObjet(JoueurCourant.abonnement) && jetonsJoueurCourant != getJoueur().getJeton()) {
			getJoueur().setJeton((int) (jetonsJoueurCourant*1.1));//donne 10% de plus si abonnement
		}else {
			getJoueur().setJeton(jetonsJoueurCourant);//donne les jetons du joueur
		}
		if(gagne()) return 1;
		if(!gagne()) return 0;
		return -1;
	}
	
	/**
	 * pour savoir quelle mise le joueur va mettre
	 * 
	 * u: augmenter la mise
	 * d: diminiuer la mise
	 * a: accepter
	 * @return la mise du joueur ou -1 si erreur
	 */
	private int actionMiser() {
		this.miseAct = true;
		char action = ' ';
		act.setKey(' ');
		this.resetTime();
		while(!this.isTimeFinished()) {
			while(!this.isTimeFinished() && (action = act.actionMiserBJ()) == ' ');
			switch(action) {
				case 'u' : this.setMise(1); break;
				case 'd' : this.setMise(-1); break;
				case 'a' : this.finirTimer(); return this.getMise(); //TODO rajouter le son des jetons
				case 'e' : this.finirTimer(); setAbandonne(); return -11;
			}
			action = ' ';
			act.setKey(' ');
		}
		this.finirTimer();
		return -1;
	}
	
	/**
	 * 
	 * @param mise la mise courante
	 * @return -1 si il reste :r
	 * 			1 si il veut une autre carte :c
	 * 			2 si il split :s
	 * 			3 si il double :d
	 * 			-11 si erreur
	 * 
	 */
	private int actionJoueur(int mise) {
		this.miseAct = false;
		this.resetTime();
		char action = ' ';
		while(!this.isTimeFinished() && (action = act.actionBJ()) == ' ');
		switch(action) {
			case 'r' : break;//TODO rajouter le son des cartes
			case 's' : break;//TODO
			case 'c' : break;//TODO
			case 'd' : break;//TODO 
			case 'e' : setAbandonne(); break;//TODO
		}
		this.finirTimer();
		return -11;
	}
	
	/**
	 * la fonction du jeu principal
	 * 
	 * @return -1 si erreur
	 */
	private int jeu() {
		//TODO le joueur courant est au milieu de la table mais position 0 alors que si il a un joueur � gauche il doit jouer avant
		miser();
		if(!quitte) { 
			distribuerJeu();
			for(int i=0; i<joueurs.size();i++) {
				if(!quitte) {
					if(jouer(i) == -1) return -1;
					if(!quitte) attends(1);
				}
			}
		}
		recommence();
		return 0;
	}
	
	/**
	 * la fonction o� chaque joueur mise
	 * 
	 * @return -1 si erreur
	 */
	private int miser() {
		joueurs.forEach(j -> {
			if(j instanceof JoueurCourant) {
				actionMiser();
			}
			if(j instanceof IA && joueurs.indexOf(j) != joueurs.size()-1) {
				((IA) j).setMiseBJ(((IA) j).miser());
			}
		});
		return 0;
	}
	
	/**
	 * la  fonction des actions des joueurs
	 * 
	 * @param joueur le joueur qui va jouer
	 * @return -1 si erreur
	 */
	private int jouer(int joueur) {
		if(!quitte) {
			if(joueur == joueurs.size()-1) { //le croupier
				croupier = true;
				//int action = ((IA) joueurs.get(joueur)).actionBJ(((IA) joueurs.get(joueur)).getMiseBJ());
				//TODO g�rer les action et si main > 21
			}else if(joueur == 0) { //tour joueur courant
				int action = actionJoueur(this.getMise()); //si -11 alors il a pas jou� donc action par d�fault (rester)
				//TODO g�rer les action et si main > 21
				switch(action) {
				
				}
			}else { //aux ia
				//int action = ((IA) joueurs.get(joueur)).actionBJ(((IA) joueurs.get(joueur)).getMiseBJ());
				//TODO g�rer les action et si main > 21
			}
		}
		return 0;
	}
	
	/**
	 * la partie est fini le joueur n'a plus de jetons on pourra dire que le joueur se fais virer si il gagne
	 * trop d'argent (et peut pas se faire virer si il a acheter un garde du corps)?
	 * 
	 * @return true si la partie est fini false sinon
	 */
	private boolean fin() {
		return false; //TODO
	}
	
	/**
	 * la partie est gagne si le joueur se fais virer?
	 * 
	 * @return true si la partie est gagne false sinon
	 */
	private boolean gagne() {
		return false;	//TODO
	}
	
	
	/**
	 * distribue les 1er cartes pour tous le monde 
	 * 1 carte pour tous le monde 2*
	 * 
	 */
	private void distribuerJeu() {
		croupier = false;
		joueurs.forEach( j -> {j.getMainB().ajouter(paquet.piocher()); if(son.isBruit()) son.getSonCarte1().play();}); 
		joueurs.forEach( j -> {j.getMainB().ajouter(paquet.piocher()); if(son.isBruit()) son.getSonCarte2().play();});
		//TODO j'ai d�j� fais aussi en fait
	}
	
	/**
	 * 
	 * reprends les cartes distribue les jetons mis� et pr�pare la nouvelle partie
	 * 
	 */
	private void recommence() {
		gagnant(); //utiliser ca pour savoir qui � gagner contre le croupier
		paquet.completer();
		joueurs.forEach(j ->{
			j.getMainB().reprendreMain();
			if(son.isBruit()) son.getSonCarte2().play();
			if(j instanceof IA && joueurs.indexOf(j) != joueurs.size()-1) ((IA) j).setMiseBJ(0);
		});
		paquet.melanger();
		if(son.isBruit()) son.getSonMelangeCarte().play();
		this.attends(3);
		this.setMise(0);
		//TODO jetons et verifier si quelqu'un n'a plus de jetons et part
	}
	
	/**
	 * indique les joueur qui ont gagner le tour
	 * 
	 * @return un tableau d'entier qui contient les id des gagnants (si aucun gagnant alors null)
	 */
	private int[] gagnant() {
		//TODO utiliser le compareTo
		return new int[joueurs.size()];
	}

}
