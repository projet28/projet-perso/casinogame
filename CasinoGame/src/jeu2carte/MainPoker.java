package jeu2carte;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainPoker implements Comparable<MainPoker>{
	
	private class ListComparable implements Comparator<List<Carte>>{
		
		@Override
		public int compare(List<Carte> l1, List<Carte> l2) {
			return compareTo(l1,l2);
		}
		
	}
	
	private List<Carte> cartes; //les cartes dispo sur le terrain et par le joueur
	private List<Carte> main; //la main du joueur (les meilleurs 5 cartes dispo)
	private List<Carte> possCarte; //les 2 1er cartes du joueurs
	
	public MainPoker() {
		main = new ArrayList<Carte>(5);
		possCarte = new ArrayList<Carte>(2);
		cartes = new ArrayList<Carte>(7);
	}
	
	/**
	 * 
	 * @return la main du joueur (5 cartes)
	 */
	public List<Carte> getMain() {
		return main;
	}
	
	/**
	 * 
	 * @return la main du joueur (2 cartes)
	 */
	public List<Carte> getMainPoss() {
		return possCarte;
	}
	
	private List<List<Carte>> possibilite6(List<Carte> l, Carte c){
		List<List<Carte>> poss = new ArrayList<List<Carte>>(6);
		poss.add(l);
		for(int i = 0; i<5; i++) {
			List<Carte> tmp = new ArrayList<Carte>(l);
			tmp.set(i, c);
			Collections.sort(tmp);
			poss.add(tmp);
		}
		return poss;
	}
	
	private List<List<Carte>> possibilite(){
		List<List<Carte>> poss = new ArrayList<List<Carte>>();
		poss.addAll(possibilite6(cartes.subList(0, 5), cartes.get(5)));
		if(cartes.size() == 7) {
			for(int i = 0; i<6; i++) {
				poss.addAll(possibilite6(poss.get(i),cartes.get(6)));
			}
		}
		return poss;
	}
	
	private List<Carte> meilleurMain(){
		List<List<Carte>> poss = possibilite();
		Collections.sort(poss, new ListComparable());
		List<Carte> res = poss.get(poss.size()-1);
		return res;
	}
	
	/**
	 * 
	 * cr�e la main du joueur � partir des 7 cartes disponible en choisissant la meilleur combinaison
	 * pour 5 cartes
	 * marche aussi si il y a moins de carte
	 */
	private void setMain() {
		Collections.sort(cartes);
		if(cartes.size() < 6) {
			main = cartes;
		}else {
			main = meilleurMain();
		}
	}
	
	/**
	 * 
	 * @return taille des cartes dispo
	 */
	public int getSize() {
		return cartes.size();
	}
	
	/**
	 * ajoute une carte pour le joueur (il faut ajouter les 7 cartes)
	 * @param c la carte � ajouter
	 * @param prem si c'est les 2 premi�re cartes
	 */
	public void ajouter(Carte c, boolean prem) {
		cartes.add(c);
		if(prem) possCarte.add(c);
		setMain();
	}
	
	/**
	 * retourne la main du joueur et vide toutes ses cartes
	 * @return retourne la main du joueur (5 cartes)
	 */
	public List<Carte> reprendreMain() {
		List<Carte> l = main;
		main.clear();
		cartes.clear();
		possCarte.clear();
		return l;
	}
	
	private Carte isQuinteFlushRoyale(List<Carte> m) {
		if(m.get(m.size()-1).getNumero() == 1) {
			return isQuinteFlush(m);
		}
		return null;
	}
	
	private Carte isQuinteFlush(List<Carte> m) {
		if(isFlush(m) != null && isQuinte(m) != null) {
			if(isFlush(m).compareTo(isQuinte(m)) < 0){
				return isQuinte(m);
			}else {
				return isFlush(m);
			}
		}else {
			return null;
		}
	}
	
	private Carte isCarre(List<Carte> m) {
		for(int i=0;i<m.size();i++) {
			for(int j=i+1;j<m.size();j++) {
				for(int k=j+1;k<m.size();k++) {
					for(int l=k+1;l<m.size();l++) {
						if(m.get(i).compareTo(m.get(j)) == 0 &&
							m.get(i).compareTo(m.get(k)) == 0 &&
							m.get(i).compareTo(m.get(l)) == 0) {
							return m.get(i);
						}
					}
				}
			}
		}
		return null;
	}
	
	/*private List<Carte> isFull7(List<Carte> m) {
		Carte[] c = new Carte[2];
		for(int i=0;i<m.size();i++) {
			for(int j=i+1;j<m.size();j++) {
				for(int k=j+1;k<m.size();k++) {
					if(m.get(i).compareTo(m.get(j)) == 0 &&
						m.get(i).compareTo(m.get(k)) == 0) {
						c[0] = m.get(i);
					}
				}
			}
		}
		if(c[0] != null) {
			for(int i=0;i<m.size();i++) {
				for(int j=i+1;j<m.size();j++) {
					if(c[0].getNumero() != m.get(i).getNumero() &&
							m.get(i).compareTo(m.get(j)) == 0) {
						c[1] = m.get(i);
					}
				}
			}
		}
		if(c[1] != null) {
			List<Carte> res = new ArrayList<Carte>(5);
			for(int i=0;i<m.size();i++){
				if(m.get(i).compareTo(c[0]) == 0) {
					res.add(m.get(i)); 
				}else if(m.get(i).compareTo(c[1]) == 0) {
					res.add(m.get(i)); 
				}
			}
			while(res.size() > 5 && m.get(0).compareTo(c[0]) != 0) {
				res.remove(0);
			}
			return res;
		}
		return null;

	}*/
	
	private Carte[] isFull(List<Carte> m) {
		Carte[] c = new Carte[2];
		for(int i=0;i<m.size();i++) {
			for(int j=i+1;j<m.size();j++) {
				for(int k=j+1;k<m.size();k++) {
					if(m.get(i).compareTo(m.get(j)) == 0 &&
						m.get(i).compareTo(m.get(k)) == 0) {
						c[0] = m.get(i);
					}
				}
			}
		}
		if(c[0] != null) {
			for(int i=0;i<m.size();i++) {
				for(int j=i+1;j<m.size();j++) {
					if(c[0].getNumero() != m.get(i).getNumero() &&
							m.get(i).compareTo(m.get(j)) == 0) {
						c[1] = m.get(i);
						return c;
					}
				}
			}
		}
		return null;

	}
	
	/*private List<Carte> isFlush7(List<Carte> m) {
		int cptT = 0;
		int cptP = 0;
		int cptCa = 0;
		int cptCo = 0;
		int tmp = 0;
		for(int i=0;i<m.size();i++) {
			if(m.get(i).getCouleur() == 1) {
				cptT++;
				if(cptT > 4) tmp = i;
			}
			if(m.get(i).getCouleur() == 0) {
				cptP++;
				if(cptP > 4) tmp = i;
			}
			if(m.get(i).getCouleur() == 2) {
				cptCa++;
				if(cptCa > 4) tmp = i;
			}
			if(m.get(i).getCouleur() == 3) {
				cptCo++;
				if(cptCo > 4) tmp = i;
			}
		}
		if(cptT > 4 || cptP > 4 || cptCa > 4 || cptCo > 4) {
			List<Carte> res = new ArrayList<Carte>(5);
			for(int i=0;i<m.size();i++){
				if(m.get(i).memeCouleur(m.get(tmp))) res.add(m.get(i)); 
			}
			while(res.size() > 5) {
				res.remove(0);
			}
			return res;
		}
		return null;
	}*/
	
	private Carte isFlush(List<Carte> m) {
		if(m.size()>=5) {
			int cpt = 0;
			for(int i=0;i<m.size()-1;i++) {
				if(m.get(i).memeCouleur(m.get(i+1))) {
					cpt++;
				}
			}
			if(cpt == m.size()-1) return m.get(m.size()-1);
		}
		return null;
	}
	
	private Carte isQuinte(List<Carte> m) {
		Carte c = null;
		if(m.size()>=5) {
			int cpt = 0;
			for(int i=0;i<m.size()-1;i++) {
				if(m.get(i).compareTo(m.get(i+1)) == -1) {
					cpt++;
					
				}else if(cpt < m.size()-1) {
					cpt = 0;
				}
				if(cpt>=m.size()-1) {
					c = m.get(i);
					
				}
			}
		}
		return c;
	}
	
	private Carte isBrelan(List<Carte> m) {
		for(int i=0;i<m.size();i++) {
			for(int j=i+1;j<m.size();j++) {
				for(int k=j+1;k<m.size();k++) {
					if(m.get(i).compareTo(m.get(j)) == 0 &&
						m.get(i).compareTo(m.get(k)) == 0) {
						return m.get(i);
					}
				}
			}
		}
		return null;
	}
	
	private Carte[] isDoublePaire(List<Carte> m) {
		Carte[] c = new Carte[2];
		for(int i=0;i<m.size();i++) {
			for(int j=i+1;j<m.size();j++) {
				if(c[0] == null) {
					if(m.get(i).compareTo(m.get(j)) == 0) {
						c[0] = m.get(i);
					}
				}else {
					if(m.get(i).compareTo(m.get(j)) == 0) {
						c[1] = m.get(i);
						return c;
					}
				}
			}
		}
		return null;
	}
	
	private Carte isPaire(List<Carte> m) {
		for(int i=0;i<m.size();i++) {
			for(int j=i+1;j<m.size();j++) {
				if(m.get(i).compareTo(m.get(j)) == 0) {
					return m.get(i);
				}
			}
		}
		return null;
	}
	
	private int plusHaut(List<Carte> m1, List<Carte> m2) {
		int res = 0;
		for(int i=m2.size()-1;i>=0;i--) {
			if((res = m1.get(i).compareTo(m2.get(i))) != 0) {
				return res;
			}
		}
		return 0;
	}
	
	/**
	 * 
	 * @param m la list de carte a test� 5 cartes max
	 * @return la valeur de la main 0 si rien 1 pair ... 9 quinteflushroyale
	 */
	public int value(List<Carte> m) {
		if(isQuinteFlushRoyale(m) != null) return 9;
		if(isQuinteFlush(m) != null) return 8;
		if(isCarre(m) != null) return 7;
		if(isFull(m) != null) return 6;
		if(isFlush(m) != null) return 5;
		if(isQuinte(m) != null) return 4;
		if(isBrelan(m) != null) return 3;
		if(isDoublePaire(m) != null) return 2;
		if(isPaire(m) != null) return 1;
		return 0;
	}
	
	@Override
	public int compareTo(MainPoker m2) {
		return compareTo(this.getMain(), m2.getMain());
	}
	
	private int compareTo(List<Carte> m1, List<Carte> m2) {
		
		int val1 = value(m1);
		int val2 = value(m2);
		
		if(val1 == val2) {
			int tmp;
			int tmp2;
			switch (val1) {
				case 0: return plusHaut(m1, m2);
				case 1: if((tmp = this.isPaire(m1).compareTo(isPaire(m2))) == 0) {
							return plusHaut(m1, m2);
						}else{
							return tmp;
						}
				case 2: if((tmp = this.isDoublePaire(m1)[1].compareTo(isDoublePaire(m2)[1])) == 0) {
							if((tmp2 = this.isDoublePaire(m1)[0].compareTo(isDoublePaire(m2)[0])) == 0) {
								return plusHaut(m1, m2);
							}else {
								return tmp2;
							}
						}else{
							return tmp;
						}
				case 3: if((tmp = this.isBrelan(m1).compareTo(isBrelan(m2))) == 0) {
							return plusHaut(m1, m2);
						}else{
							return tmp;
						}
				case 4: if((tmp = this.isQuinte(m1).compareTo(isQuinte(m2))) == 0) {
							return plusHaut(m1, m2);
						}else{
							return tmp;
						}
				case 5: if((tmp = this.isFlush(m1).compareTo(isFlush(m2))) == 0) {
							return plusHaut(m1, m2);
						}else{
							return tmp;
						}
				case 6: if((tmp = this.isFull(m1)[0].compareTo(isFull(m2)[0])) == 0) {
							if((tmp2 = this.isFull(m1)[1].compareTo(isFull(m2)[1])) == 0) {
								return plusHaut(m1, m2);
							}else {
								return tmp2;
							}
						}else{
							return tmp;
						}
				case 7: if((tmp = this.isCarre(m1).compareTo(isCarre(m2))) == 0) {
							return plusHaut(m1, m2);
						}else{
							return tmp;
						}
				case 8: if((tmp = this.isQuinteFlush(m1).compareTo(isQuinteFlush(m2))) == 0) {
							return plusHaut(m1, m2);
						}else{
							return tmp;
						}
				case 9: if((tmp = this.isQuinteFlushRoyale(m1).compareTo(isQuinteFlushRoyale(m2))) == 0) {
							return plusHaut(m1, m2);
						}else{
							return tmp;
						}
				default: System.out.println("impossible");
			}
			return 0;
		}else {
			return val1 - val2;
		}
	}
	
	private String s ="";
	@Override
	public String toString() {
		
		main.forEach(c ->{ s += c.toString() + "; ";});
		return s;
	}
}
