package jeu2carte;

import java.util.ArrayList;
import java.util.List;

import casino.Jeu;
import joueur.Joueur;
import joueur.JoueurCourant;

abstract class JeuDeCarte extends Jeu{
	
	protected Paquet paquet; //pile de carte
	protected List<Joueur> joueurs; //indice 0 = joueur courant

	public JeuDeCarte(long time) {
		super(time);
		paquet = new Paquet();
		joueurs = new ArrayList<Joueur>();
	}
	
	@Override
	abstract public int play();

	public List<Joueur> getJoueurs() {
		return joueurs;
	}

	public void setJoueurs(List<Joueur> joueurs) {
		this.joueurs = joueurs;
	}
	
	public void addJoueur(JoueurCourant j) {
		joueurs.add(0,j);
	}
	
	public JoueurCourant getJoueur() {
		return (JoueurCourant) joueurs.get(0);
	}
	
	/**
	 * fini la repartition des jetons
	 * 
	 * @param cptJeton le nombre de jetons restant
	 * @param t le tableau de repartition des jetons
	 * @return le tableau des jetons repartis totalement
	 */
	static private int[] repart(int cptJeton, int[] t) {
		t[0]+=((((((cptJeton%5000)%1000)%500)%100)%25)%10)%5;
		t[1]+=((((((cptJeton%5000)%1000)%500)%100)%25)%10)/5;
		t[2]+=(((((cptJeton%5000)%1000)%500)%100)%25)/10;
		t[3]+=((((cptJeton%5000)%1000)%500)%100)/25;
		t[4]+=(((cptJeton%5000)%1000)%500)/100;
		t[5]+=((cptJeton%5000)%1000)/500;
		t[6]+=(cptJeton%5000)/1000;
		t[7]+=cptJeton/5000;
		return t;
	}
	
	/**
	 * r�partis les jetons en fonction de leur nombre et des besoins du joueur
	 * 
	 * @param cptJeton le nombre de jeton � repartir
	 * @return un tableau des jetons 
	 */
	static public int[] getRepartJeton(int cptJeton) {
		int[] t = new int[] {0,0,0,0,0,0,0,0};
		if(cptJeton<10) {
			t[0] = cptJeton;
		}else {
			t[0]=10;
			cptJeton-=10*1;
			if(cptJeton<35) {
				t[1]=cptJeton/5;
				t = repart(cptJeton-t[1]*5, t);
			}else {
				t[1]=5;
				cptJeton-=5*5;
				if(cptJeton<85) {
					t[2]+=cptJeton/10;
					t = repart(cptJeton-t[2]*10, t);
				}else {
					t[2]=5;
					cptJeton-=5*10;
					if(cptJeton<210) {
						t[3]=cptJeton/25;
						t = repart(cptJeton-t[3]*25, t);
					}else {
						t[3]=5;
						cptJeton-=5*25;
						if(cptJeton<710) {
							t[4]=cptJeton/100;
							t = repart(cptJeton-t[4]*100, t);
						}else {
							t[4]=5;
							cptJeton-=5*100;
							if(cptJeton<3210) {
								t[5]=cptJeton/500;
								t = repart(cptJeton-t[5]*500, t);
							}else {
								t[5]=5;
								cptJeton-=5*500;
								if(cptJeton<8210) {
									t[6]=cptJeton/1000;
									t = repart(cptJeton-t[6]*1000, t);
								}else {
									t[6]=5;
									cptJeton-=5*1000;
									if(cptJeton<33210) {
										t[7]=cptJeton/5000;
										t = repart(cptJeton-t[7]*5000, t);
									}else {
										t[7]=5;
										cptJeton-=5*5000;
										t = repart(cptJeton, t);
									}
								}
							}
						}
					}
				}
			}
		}
		return t;
	}

}
