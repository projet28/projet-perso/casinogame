package jeu2carte;

public class Carte implements Comparable<Carte>{

	private String couleur; //carreau, pique ...
	private int c; //en numero
	private int numero; //11 pour valets, 12 pour dame et 13 pour roi
	
	public Carte(int couleur, int numero) {
		this.c = couleur;
		if(couleur==0) this.couleur = "pique";
		if(couleur==1) this.couleur = "tr�fle";
		if(couleur==2) this.couleur = "carreau";
		if(couleur==3) this.couleur = "coeur";
		this.numero = numero;
	}
	
	public int getCouleur() {
		return c;
	}

	public String getCouleurString() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	/**
	 * 
	 * @param c2 l'autre carte
	 * @return true si les cartes sont de m�me couleur false sinon
	 */
	public boolean memeCouleur(Carte c2) {
		return c2.couleur==this.couleur;
	}
	
	@Override
	public int compareTo(Carte c2) {
		if(this.numero == 1 || c2.numero == 1) {
			if(this.numero == 1 && c2.numero == 1) return 0;
			if(this.numero == 1) return (this.numero + 13) - c2.numero;
			if(c2.numero == 1) return this.numero - (c2.numero + 13);
		}
		return this.numero - c2.numero;
	}
	
	@Override
	public String toString() {
		return this.numero + " de " + this.couleur; 
	}
	
}
