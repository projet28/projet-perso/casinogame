package jeu2carte;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Paquet {
	private List<Carte> paquet; // le paquet de carte
	
	public Paquet() {
		paquet = new ArrayList<Carte>(52);
		completer();
		melanger();
	}
	
	/**
	 * 
	 * @return la taille du paquet
	 */
	public int getSize() {
		return paquet.size();
	}
	
	/**
	 * 
	 * remet toutes les cartes dans le paquet
	 */
	public void completer() {
		paquet.clear();
		for(int i=0; i<4; i++) {
			for(int j=1; j<14; j++) {
				paquet.add(new Carte(i, j));
			}
		}
	}
	
	/**
	 * 
	 * @return la carte au dos du paquet
	 */
	public Carte piocher() {
		return paquet.remove(paquet.size()-1);
	}
	
	/**
	 * 
	 * m�lange le paquet
	 */
	public void melanger() {
		Collections.shuffle(paquet);
	}
}
